#ifndef __TENCMACHINELEARNING__
#define __TENCMACHINELEARNING__

#include "opencv2/core.hpp"
#include "opencv2/ml.hpp"

#include "TLibCommon/TComDataCU.h"
#include "TLibCommon/TComRdCost.h"

// ====================================================================================================================
// Class definition
// ====================================================================================================================

/// Machine Learning Tools class
class TEncMachineLearning
{

private: 

	// cv::Ptr<cv::ml::StatModel> model01[4];
	// cv::Ptr<cv::ml::StatModel> model02[4];
	// cv::Ptr<cv::ml::StatModel> model03[4];
	cv::Ptr<cv::ml::RTrees> model01[4];
	cv::Ptr<cv::ml::RTrees> model02[4];
	cv::Ptr<cv::ml::RTrees> model03[4];
	// UInt m_QPOffsets[5] = {1,1,4,5,6};

	template<typename T>
	static cv::Ptr<T> load_classifier(const std::string& filename_to_load);
	// cv::Ptr<cv::ml::RTrees> load_classifier(const std::string& filename_to_load);


public:

	void load_all_classifiers(const std::string& models_dir, const std::string classifier_type = "RTrees");
	
	std::map<std::string,Double> get_features_cu(TComDataCU* pcCu, TComRdCost* pcRdCost);

	cv::Mat  calc_features_noSplitQT(TComDataCU* pCu, TComRdCost* pcRdCost, TComDataCU*** alreadyCreatedCus, std::map<std::string,Double> cuFeaturesMap);
	cv::Mat  calc_features_noSplitHor(TComDataCU* pCu, TComRdCost* pcRdCost, TComDataCU*** alreadyCreatedCus, std::map<std::string,Double> wholeCuFeatures);
	cv::Mat  calc_features_noSplitVer(TComDataCU* pCu, TComRdCost* pcRdCost, TComDataCU*** alreadyCreatedCus, std::map<std::string,Double> wholeCuFeatures);

	void splitsToKeep(bool keptSplits[4], bool authorizedSplits[4], TComDataCU* pCu, TComRdCost* pcRdCost, TComDataCU*** alreadyCreatedCus);

};

#endif // __TENCMACHINELEARNING__