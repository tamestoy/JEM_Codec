#include <sys/types.h>
#include <dirent.h>
#include <vector>
#include <math.h>
#include "TEncMachineLearning.h"


void read_directory(const std::string& name, std::vector<std::string>& v)
{
    DIR* dirp = opendir(name.c_str());
    struct dirent * dp;
    while ((dp = readdir(dirp)) != NULL) {
        v.push_back(dp->d_name);
    }
    closedir(dirp);
}


template<typename T>
cv::Ptr<T> TEncMachineLearning::load_classifier(const std::string& filename_to_load)
{
	// load classifier from the specified file
    cv::Ptr<T> model = cv::ml::StatModel::load<T>( filename_to_load );
    if( model.empty() )
        std::cout << "Could not read the classifier " << filename_to_load << std::endl;
    else
        std::cout << "The classifier " << filename_to_load << " is loaded." << std::endl;

    return model;
}



void TEncMachineLearning::load_all_classifiers(const std::string& models_dir, const std::string classifier_type )
{
    printf("loading all classifiers ...\n");

    std::vector<std::string> vect_files;
    read_directory(models_dir,vect_files);

    for(uint i=0 ; i<vect_files.size() ; i++)
    {
			const std::string& filename_to_load = models_dir + "/" + vect_files[i];
      if (vect_files[i].find(".model")!= std::string::npos)
      {
        for(uint j=0; j<4; j++)
        {
          int size = 128 >> j ;
         
          if (vect_files[i].find("01")!=std::string::npos && vect_files[i].find(std::to_string(size))!=std::string::npos)
          {
              std::cout << "\nloading model 01 from "<< vect_files[i] << std::endl;
              std::cout << "insert model -> "<< j << std::endl;
							//ADAPTER SI USE SVM !!!!!
              // model01[j] = load_classifier(filename_to_load);
              model01[j] = load_classifier<cv::ml::RTrees>(filename_to_load);
          }
         	else if (vect_files[i].find("02")!=std::string::npos && vect_files[i].find(std::to_string(size))!=std::string::npos)
          {
              std::cout << "\nloading model 02 from "<< vect_files[i] << std::endl;
              std::cout << "insert model -> "<< j << std::endl;
							//ADAPTER SI USE SVM !!!!!
              // model02[j] = load_classifier(filename_to_load);
              model02[j] = load_classifier<cv::ml::RTrees>(filename_to_load);
          }
					else if (vect_files[i].find("03")!=std::string::npos && vect_files[i].find(std::to_string(size))!=std::string::npos)
          {
              std::cout << "\nloading model 03 from "<< vect_files[i] << std::endl;
              std::cout << "insert model -> "<< j << std::endl;
							//ADAPTER SI USE SVM !!!!!
              // model03[j] = load_classifier(filename_to_load);
              model03[j] = load_classifier<cv::ml::RTrees>(filename_to_load);
          }
        }
      } 

    }

	return;
}



std::map<std::string,Double> TEncMachineLearning::get_features_cu(TComDataCU* pCu, TComRdCost* pcRdCost)
{ 
  //Get MV block array informations
  UInt sizeblock = pcRdCost->getSizeBlocks();
  assert(sizeblock == 4 || sizeblock == 8);
  // UInt blockIncrement = sizeblock/4;
  UInt uiCTUSize = pCu->getSlice()->getSPS()->getCTUSize();
  UInt widthArrayBlocks = uiCTUSize / sizeblock;

  //Get current CU informations
  UInt cuRSAddrInCtu = g_auiZscanToRaster[pCu->getZorderIdxInCtu()];
  UInt nbBlocksInWidth = pCu->getWidth(0) /sizeblock;
  UInt nbBlocksInHeight = pCu->getHeight(0) /sizeblock;
  RefPicList refPic = REF_PIC_LIST_0;

  //Looping on the Mvs of the 4x4 blocks contained in the CU
  //Compute CU's MEANS 
  Double meanPix = 0;
  Double meanHor = 0;
  Double meanVer = 0;
  Double meanGradx = 0;
  Double meanGrady = 0;
  for(UInt block_x=0; block_x<nbBlocksInWidth; block_x++)
  {
    for(UInt block_y=0; block_y<nbBlocksInHeight; block_y++)
    {
      UInt blockRSAddrInCtu = cuRSAddrInCtu + block_x + widthArrayBlocks * block_y;
      
      meanPix += pcRdCost->getArrayApproxBlocks(blockRSAddrInCtu)->getPixMean(COMPONENT_Y);
      
      meanHor += pcRdCost->getArrayApproxBlocks(blockRSAddrInCtu)->getCUMvField(refPic)->getMv(0).getHor();
      meanVer += pcRdCost->getArrayApproxBlocks(blockRSAddrInCtu)->getCUMvField(refPic)->getMv(0).getVer();

      meanGradx += pcRdCost->getArrayApproxBlocks(blockRSAddrInCtu)->getGradient(COMPONENT_Y,0);
      meanGrady += pcRdCost->getArrayApproxBlocks(blockRSAddrInCtu)->getGradient(COMPONENT_Y,1);
    }
  }
  meanPix /= (nbBlocksInWidth*nbBlocksInHeight);
  meanGradx /= (nbBlocksInWidth*nbBlocksInHeight);
  meanGrady /= (nbBlocksInWidth*nbBlocksInHeight);
  meanHor /= (nbBlocksInWidth*nbBlocksInHeight);
  meanVer /= (nbBlocksInWidth*nbBlocksInHeight);

  //meanPix,meanGradx,meanGrady,meanHor,meanVer,varPix,energy,varHor,varVer,maxEcartHor,maxEcartVer
  std::map<std::string,Double> wholeCuFeatures;
  wholeCuFeatures["meanPix"] = meanPix;
  wholeCuFeatures["meanGradx"] = meanGradx;
  wholeCuFeatures["meanGrady"] = meanGrady;
  wholeCuFeatures["meanHor"] = meanHor;
  wholeCuFeatures["meanVer"] = meanVer;

  //Compute CU's VARIANCES 
  Double varPix = 0;
  Double energy = 0;
  Double varHor = 0;
  Double varVer = 0;
  Double maxEcartHor = 0;
  Double maxEcartVer = 0;  
  for(UInt block_x=0; block_x<nbBlocksInWidth; block_x++)
  {
    for(UInt block_y=0; block_y<nbBlocksInHeight; block_y++)
    {
      UInt blockRSAddrInCtu = cuRSAddrInCtu + block_x + widthArrayBlocks * block_y;

      //Features Based on pixels
      Double meanPixBlock = pcRdCost->getArrayApproxBlocks(blockRSAddrInCtu)->getPixMean(COMPONENT_Y);
      Double varPixBlock = pcRdCost->getArrayApproxBlocks(blockRSAddrInCtu)->getPixVar(COMPONENT_Y);
      Double diffMean = fabs(meanPixBlock + varPixBlock - meanPix)/2. + fabs(meanPixBlock - varPixBlock - meanPix)/2.0;
      varPix       	 += diffMean;

      //Features Based on MVs
      Double horDeltaMv = (pcRdCost->getArrayApproxBlocks(blockRSAddrInCtu)->getCUMvField(refPic)->getMv(0).getHor() - meanHor);
      Double verDeltaMv = (pcRdCost->getArrayApproxBlocks(blockRSAddrInCtu)->getCUMvField(refPic)->getMv(0).getVer() - meanVer);
      //Energy
      energy += fabs(pcRdCost->getArrayApproxBlocks(blockRSAddrInCtu)->getGradient(COMPONENT_Y,0) * horDeltaMv);
      energy += fabs(pcRdCost->getArrayApproxBlocks(blockRSAddrInCtu)->getGradient(COMPONENT_Y,1) * verDeltaMv);
      //Variance MV
      varHor += fabs(horDeltaMv);
      varVer += fabs(verDeltaMv);
      //Maximum difference with mean
      maxEcartHor = max(maxEcartHor,fabs(horDeltaMv));
      maxEcartVer = max(maxEcartVer,fabs(verDeltaMv));
    }
  }
  varPix /= (nbBlocksInWidth*nbBlocksInHeight);
  energy /= (nbBlocksInWidth*nbBlocksInHeight);
  varHor /= (nbBlocksInWidth*nbBlocksInHeight);
  varVer /= (nbBlocksInWidth*nbBlocksInHeight);

	wholeCuFeatures["varPix"] = varPix;
  wholeCuFeatures["energy"] = energy;
  wholeCuFeatures["varHor"] = varHor;
  wholeCuFeatures["varVer"] = varVer;
  wholeCuFeatures["maxEcartHor"] = maxEcartHor;
  wholeCuFeatures["maxEcartVer"] = maxEcartVer;

  return wholeCuFeatures;
}



UInt getTemporality(UInt numFrame)
{
	UInt divider = 16;
	UInt temporality = 0;
	while (divider >= 1)
	{
		if (numFrame % divider == 0)
			break;
		divider /= 2;
	 	temporality = temporality + 1;
	}
	return temporality;
}

	
cv::Mat  TEncMachineLearning::calc_features_noSplitQT(TComDataCU* pCu, TComRdCost* pcRdCost, TComDataCU*** alreadyCreatedCus, std::map<std::string,Double> wholeCuFeatures)
{
	assert(alreadyCreatedCus != NULL);
	std::vector<float> chosen_features;

	UInt qp      = pCu->getQP(0);
	chosen_features.push_back(qp); 

	// UInt numFrame		 = pCu->getSlice()->getPOC();
	// UInt temporality = getTemporality(numFrame);
 //  chosen_features.push_back(temporality);
  
  // ***************************************************************
  //PARTIE CU ENTIERE
  // ***************************************************************



  // ***************************************************************
  //PARTIE SPLIT QT
  // ***************************************************************
 	const TComSPS   &sps =*(pCu->getSlice()->getSPS());
  UInt uiHeight = pCu->getHeight(0);   
  UInt uiWidth = pCu->getWidth(0);
  UInt uiDepth = pCu->getDepth(0);     
  UInt uiQTWidthIdx = g_aucConvertToBit[uiWidth>>1];  
  UInt uiQTHeightIdx = g_aucConvertToBit[uiHeight>>1];
	assert(uiWidth == uiHeight);

  //cout global de la partition QT
 	Double varMeanPix = 0;
 	Double meanVarPix = 0;
 	Double energyQt = 0;
 	Double varMeanHor = 0;
  Double varMeanVer = 0;
  Double meanVarMv = 0;
  for( UInt uiPartUnitIdx = 0; uiPartUnitIdx < 4; uiPartUnitIdx++ )
  {
    TComDataCU* pcSubPartCU    = alreadyCreatedCus[uiQTWidthIdx][uiQTHeightIdx];
    pcSubPartCU->initSubCU( pCu , uiPartUnitIdx, uiDepth+1, pCu->getQP(0) );           // clear sub partition datas or init.

    if( ( pcSubPartCU->getCUPelX() < sps.getPicWidthInLumaSamples() ) && ( pcSubPartCU->getCUPelY() < sps.getPicHeightInLumaSamples() ) )
    {
      std::map<std::string,Double> subCuFeatures = get_features_cu(pcSubPartCU, pcRdCost);

      varMeanPix += 0.25 * fabs(subCuFeatures["meanPix"] - wholeCuFeatures["meanPix"]);
      meanVarPix += 0.25 * subCuFeatures["varPix"] ;
      energyQt 	 += 0.25 * subCuFeatures["energy"]; 

      varMeanHor +=  0.25*fabs(subCuFeatures["meanHor"]-wholeCuFeatures["meanHor"]);
      varMeanVer +=  0.25*fabs(subCuFeatures["meanVer"]-wholeCuFeatures["meanVer"]);
      meanVarMv  +=  0.25*subCuFeatures["varHor"] + 0.25*subCuFeatures["varVer"];

    }
  }
  // # trainingFile.write("@ATTRIBUTE QP REAL\n") 
// # trainingFile.write("@ATTRIBUTE temporality REAL\n")

// # trainingFile.write("@ATTRIBUTE varPix REAL\n")
// # trainingFile.write("@ATTRIBUTE varLowermeanPix REAL\n")
// # trainingFile.write("@ATTRIBUTE meanLowerVarPix REAL\n")

// # trainingFile.write("@ATTRIBUTE ratioEnergyQt REAL\n")         
// # # trainingFile.write("@ATTRIBUTE varMv REAL\n")
// # trainingFile.write("@ATTRIBUTE maxEcartMV REAL\n")
// # # trainingFile.write("@ATTRIBUTE varLowerMeanMV REAL\n")
// # trainingFile.write("@ATTRIBUTE meanLowerVarMV REAL\n")
// # # trainingFile.write("@ATTRIBUTE varLowerVarMV REAL\n")

// # trainingFile.write("@ATTRIBUTE split {0,1}\n")

  chosen_features.push_back(wholeCuFeatures["varPix"]);
  chosen_features.push_back(varMeanPix);
  chosen_features.push_back(meanVarPix);

  chosen_features.push_back(min(10.0,wholeCuFeatures["energy"]/max(0.1,energyQt)));
  chosen_features.push_back(wholeCuFeatures["varHor"]+wholeCuFeatures["varVer"]);
  chosen_features.push_back(wholeCuFeatures["maxEcartHor"]+wholeCuFeatures["maxEcartVer"]);
  chosen_features.push_back(varMeanHor+varMeanVer);
  chosen_features.push_back(meanVarMv);

  cv::Mat mat = cv::Mat(1,chosen_features.size(), CV_32F); 
  for (int i = 0; i < chosen_features.size(); i++)
  {
  	// printf("chosen_features[%i] = %f\n", i, chosen_features[i]);
  	mat.at<float>(0,i) = chosen_features[i];
  }
	return mat;

  // return chosen_features;
}


cv::Mat  TEncMachineLearning::calc_features_noSplitHor(TComDataCU* pCu, TComRdCost* pcRdCost, TComDataCU*** alreadyCreatedCus, std::map<std::string,Double> wholeCuFeatures)
{
 	const TComSPS   &sps =*(pCu->getSlice()->getSPS());
  UInt uiWidth = pCu->getWidth(0);
  UInt uiHeight = pCu->getHeight(0);   
  UInt uiDepth = pCu->getDepth(0);     
  UInt uiBTWidthIdx  = g_aucConvertToBit[uiWidth];   
  UInt uiBTHeightIdx = g_aucConvertToBit[uiHeight>>1];

	assert(alreadyCreatedCus != NULL);
	std::vector<float> chosen_features;

  // ***************************************************************
  //PARTIE CU ENTIERE
  // ***************************************************************
  // # trainingFile.write("@ATTRIBUTE QP REAL\n")
	// # trainingFile.write("@ATTRIBUTE temporality REAL\n")
	// # trainingFile.write("@ATTRIBUTE ratioSize REAL\n")

	// # trainingFile.write("@ATTRIBUTE gradx/grady REAL\n")
	// # trainingFile.write("@ATTRIBUTE varPix REAL\n")
	// # trainingFile.write("@ATTRIBUTE varMv REAL\n")
	// # trainingFile.write("@ATTRIBUTE maxEcartMV REAL\n")
	UInt qp      = pCu->getQP(0);
	chosen_features.push_back(qp); 

	// UInt numFrame		 = pCu->getSlice()->getPOC();
	// UInt temporality = getTemporality(numFrame);
 //  chosen_features.push_back(temporality);
  	
  chosen_features.push_back(log2(uiWidth*1.0/uiHeight));

	if (wholeCuFeatures["meanGradx"]*wholeCuFeatures["meanGrady"]!=0.0)
			chosen_features.push_back(wholeCuFeatures["meanGradx"]/wholeCuFeatures["meanGrady"]);
	else
		chosen_features.push_back(0.0);

	chosen_features.push_back(wholeCuFeatures["varPix"]);
	chosen_features.push_back(wholeCuFeatures["varHor"] + wholeCuFeatures["varVer"]);
	chosen_features.push_back(wholeCuFeatures["maxEcartHor"]+wholeCuFeatures["maxEcartVer"]);


  // ***************************************************************
  //PARTIE SPLIT BT HORIZONTAL
  // ***************************************************************
 	Double BThorVarPix = 0;
 	Double BThorMinVarPix = MAX_DOUBLE;
 	Double BThorEnergy = 0;
 	Double BThorVarMv = 0;
  Double BThorEcartMV = 0;
  Double BThorMinVarMv = MAX_DOUBLE;
  for( UInt uiPartUnitIdx = 0; uiPartUnitIdx < 2; uiPartUnitIdx++ )
  {    
    TComDataCU* pcSubPartCU     = alreadyCreatedCus[uiBTWidthIdx][uiBTHeightIdx];
    pcSubPartCU->initSubBT( pCu, uiPartUnitIdx, uiDepth, uiWidth, uiHeight>>1, 1, pCu->getQP(0));

    if( ( pcSubPartCU->getCUPelX() < sps.getPicWidthInLumaSamples() ) && ( pcSubPartCU->getCUPelY() < sps.getPicHeightInLumaSamples() ) )
    {
      std::map<std::string,Double> subCuFeatures = get_features_cu(pcSubPartCU, pcRdCost);
			
			//BThorVarPix = BThorVarPix + 0.5*featuresSubCU["varPix"]
      BThorVarPix += 0.5 * subCuFeatures["varPix"] ;
      //BThorMinVarPix = min(BThorMinVarPix, featuresSubCU["varPix"])
      BThorMinVarPix = min(BThorMinVarPix,subCuFeatures["varPix"]);
			//BThorEnergy = BThorEnergy + featuresSubCU["energy"] / 2.0 
      BThorEnergy 	 += 0.5 * subCuFeatures["energy"]; 
			//BThorVarMv = BThorVarMv + 0.5*featuresSubCU["varHor"] + 0.5*featuresSubCU["varVer"]
      BThorVarMv +=  0.5*(subCuFeatures["varHor"]+subCuFeatures["varVer"]);
      //BThorEcartMV = BThorEcartMV + 0.5 *featuresSubCU["maxEcartHor"] + 0.5  * featuresSubCU["maxEcartVer"]
      BThorEcartMV +=  0.5*(subCuFeatures["maxEcartHor"]+subCuFeatures["maxEcartVer"]);
			//BThorMinVarMv = min(BThorMinVarMv, featuresSubCU["varHor"] + featuresSubCU["varVer"])
      BThorMinVarMv = min(BThorMinVarMv,subCuFeatures["varHor"]+subCuFeatures["varVer"]);
    }
  }

// # trainingFile.write("@ATTRIBUTE ratio_varPix REAL\n")
// # trainingFile.write("@ATTRIBUTE ratio_minVarPix REAL\n")
// # trainingFile.write("@ATTRIBUTE ratio_varMv REAL\n")
// # trainingFile.write("@ATTRIBUTE ratio_minVarMv REAL\n")
// # trainingFile.write("@ATTRIBUTE ratio_ecartMax REAL\n")
// # trainingFile.write("@ATTRIBUTE ratioEnergyHor REAL\n")	
	// # chosen_features.push_back(BThorVarPix);
	chosen_features.push_back(wholeCuFeatures["varPix"]/max(0.1,BThorVarPix));
	chosen_features.push_back(wholeCuFeatures["varPix"]/max(0.1,BThorMinVarPix));
	// # chosen_features.push_back(BThorVarMv)
	chosen_features.push_back((wholeCuFeatures["varHor"] + wholeCuFeatures["varVer"])/max(0.1,BThorVarMv));
	chosen_features.push_back((wholeCuFeatures["varHor"] + wholeCuFeatures["varVer"])/max(0.1,BThorMinVarMv));
	chosen_features.push_back((wholeCuFeatures["maxEcartHor"]+wholeCuFeatures["maxEcartVer"])/max(0.1,BThorEcartMV));

	Double energyCu = wholeCuFeatures["energy"];
	chosen_features.push_back(min(10.0,energyCu/max(0.1,BThorEnergy)));

  cv::Mat mat = cv::Mat(1,chosen_features.size(), CV_32F); 
  for (int i = 0; i < chosen_features.size(); i++)
  {
  	// printf("chosen_features[%i] = %f\n", i, chosen_features[i]);
  	mat.at<float>(0,i) = chosen_features[i];
  }
	return mat;
}


cv::Mat  TEncMachineLearning::calc_features_noSplitVer(TComDataCU* pCu, TComRdCost* pcRdCost, TComDataCU*** alreadyCreatedCus, std::map<std::string,Double> wholeCuFeatures)
{
 	const TComSPS   &sps =*(pCu->getSlice()->getSPS());
  UInt uiWidth = pCu->getWidth(0);
  UInt uiHeight = pCu->getHeight(0);   
  UInt uiDepth = pCu->getDepth(0);     
   UInt uiBTWidthIdx  = g_aucConvertToBit[uiWidth>>1];   
  UInt uiBTHeightIdx = g_aucConvertToBit[uiHeight];

	assert(alreadyCreatedCus != NULL);
	std::vector<float> chosen_features;

  // ***************************************************************
  //PARTIE CU ENTIERE
  // ***************************************************************
  // # trainingFile.write("@ATTRIBUTE QP REAL\n")
	// # trainingFile.write("@ATTRIBUTE temporality REAL\n")
	// # trainingFile.write("@ATTRIBUTE ratioSize REAL\n")

	// # trainingFile.write("@ATTRIBUTE gradx/grady REAL\n")
	// # trainingFile.write("@ATTRIBUTE varPix REAL\n")
	// # trainingFile.write("@ATTRIBUTE varMv REAL\n")
	// # trainingFile.write("@ATTRIBUTE maxEcartMV REAL\n")
	UInt qp      = pCu->getQP(0);
	chosen_features.push_back(qp); 

	// UInt numFrame		 = pCu->getSlice()->getPOC();
	// UInt temporality = getTemporality(numFrame);
 //  chosen_features.push_back(temporality);
  	
  chosen_features.push_back(log2(uiWidth*1.0/uiHeight));

	if (wholeCuFeatures["meanGradx"]*wholeCuFeatures["meanGrady"]!=0.0)
			chosen_features.push_back(wholeCuFeatures["meanGradx"]/wholeCuFeatures["meanGrady"]);
	else
		chosen_features.push_back(0.0);

	chosen_features.push_back(wholeCuFeatures["varPix"]);
	chosen_features.push_back(wholeCuFeatures["varHor"] + wholeCuFeatures["varVer"]);
	chosen_features.push_back(wholeCuFeatures["maxEcartHor"]+wholeCuFeatures["maxEcartVer"]);


  // ***************************************************************
  //PARTIE SPLIT BT VERTICAL
  // ***************************************************************
 	Double BTVerVarPix = 0;
 	Double BTVerMinVarPix = MAX_DOUBLE;
 	Double BTVerEnergy = 0;
 	Double BTVerVarMv = 0;
  Double BTVerEcartMV = 0;
  Double BTVerMinVarMv = MAX_DOUBLE;
  for( UInt uiPartUnitIdx = 0; uiPartUnitIdx < 2; uiPartUnitIdx++ )
  {    
    TComDataCU* pcSubPartCU     = alreadyCreatedCus[uiBTWidthIdx][uiBTHeightIdx];
    pcSubPartCU->initSubBT( pCu, uiPartUnitIdx, uiDepth, uiWidth>>1, uiHeight, 2, pCu->getQP(0));

    if( ( pcSubPartCU->getCUPelX() < sps.getPicWidthInLumaSamples() ) && ( pcSubPartCU->getCUPelY() < sps.getPicHeightInLumaSamples() ) )
    {
      std::map<std::string,Double> subCuFeatures = get_features_cu(pcSubPartCU, pcRdCost);
			
			//BTVerVarPix = BTVerVarPix + 0.5*featuresSubCU["varPix"]
      BTVerVarPix += 0.5 * subCuFeatures["varPix"] ;
      //BTVerMinVarPix = min(BTVerMinVarPix, featuresSubCU["varPix"])
      BTVerMinVarPix = min(BTVerMinVarPix,subCuFeatures["varPix"]);
			//BTVerEnergy = BTVerEnergy + featuresSubCU["energy"] / 2.0 
      BTVerEnergy 	 += 0.5 * subCuFeatures["energy"]; 
			//BTVerVarMv = BTVerVarMv + 0.5*featuresSubCU["varHor"] + 0.5*featuresSubCU["varVer"]
      BTVerVarMv +=  0.5*(subCuFeatures["varHor"]+subCuFeatures["varVer"]);
      //BTVerEcartMV = BTVerEcartMV + 0.5 *featuresSubCU["maxEcartHor"] + 0.5  * featuresSubCU["maxEcartVer"]
      BTVerEcartMV +=  0.5*(subCuFeatures["maxEcartHor"]+subCuFeatures["maxEcartVer"]);
			//BTVerMinVarMv = min(BTVerMinVarMv, featuresSubCU["varHor"] + featuresSubCU["varVer"])
      BTVerMinVarMv = min(BTVerMinVarMv,subCuFeatures["varHor"]+subCuFeatures["varVer"]);
    }
  }

// # trainingFile.write("@ATTRIBUTE ratio_varPix REAL\n")
// # trainingFile.write("@ATTRIBUTE ratio_minVarPix REAL\n")
// # trainingFile.write("@ATTRIBUTE ratio_varMv REAL\n")
// # trainingFile.write("@ATTRIBUTE ratio_minVarMv REAL\n")
// # trainingFile.write("@ATTRIBUTE ratio_ecartMax REAL\n")
// # trainingFile.write("@ATTRIBUTE ratioEnergyVer REAL\n")	
	// # chosen_features.push_back(BTVerVarPix);
	chosen_features.push_back(wholeCuFeatures["varPix"]/max(0.1,BTVerVarPix));
	chosen_features.push_back(wholeCuFeatures["varPix"]/max(0.1,BTVerMinVarPix));
	// # chosen_features.push_back(BTVerVarMv)
	chosen_features.push_back((wholeCuFeatures["varHor"] + wholeCuFeatures["varVer"])/max(0.1,BTVerVarMv));
	chosen_features.push_back((wholeCuFeatures["varHor"] + wholeCuFeatures["varVer"])/max(0.1,BTVerMinVarMv));
	chosen_features.push_back((wholeCuFeatures["maxEcartHor"]+wholeCuFeatures["maxEcartVer"])/max(0.1,BTVerEcartMV));

	Double energyCu = wholeCuFeatures["energy"];
	chosen_features.push_back(min(10.0,energyCu/max(0.1,BTVerEnergy)));

  cv::Mat mat = cv::Mat(1,chosen_features.size(), CV_32F); 
  for (int i = 0; i < chosen_features.size(); i++)
  {
  	// printf("chosen_features[%i] = %f\n", i, chosen_features[i]);
  	mat.at<float>(0,i) = chosen_features[i];
  }
	return mat;
}



void TEncMachineLearning::splitsToKeep(bool keptSplits[4], bool authorizedSplits[4], TComDataCU* pCu, TComRdCost* pcRdCost, TComDataCU*** alreadyCreatedCus)
{
	UInt categoryCU =  min(3.0, log2(1.0*(128*128)/(pCu->getWidth(0)*pCu->getHeight(0)))/2);
	printf("W H %i %i -> category %i\n", pCu->getWidth(0), pCu->getHeight(0), categoryCU);
	std::map<std::string,Double> wholeCuFeatures = get_features_cu(pCu, pcRdCost);

	Double probaSplit[4] = {0.0, 0.0, 0.0, 0.0};
	UInt ntestsNS = 0;

	//Match Nosplit VS QT
	probaSplit[0] = 1.0;
	probaSplit[1] = 0.0;
	if(!authorizedSplits[0] && authorizedSplits[1])
	{
		probaSplit[0] = 0.0;
		probaSplit[1] = 1.0;
	}
	if (authorizedSplits[0] && authorizedSplits[1])
	{	
		cv::Mat featuresNSQT = calc_features_noSplitQT(pCu, pcRdCost, alreadyCreatedCus, wholeCuFeatures);
		// Double classOut = model01[categoryCU]->predict( featuresNSQT );
		// probaSplit[1] = (fabs(classOut - 1.0) < espislon);
		// printf("classOut probaSplit[1] %f %f\n", classOut, probaSplit[1] );

    cv::Mat output;
    model01[categoryCU]->getVotes( featuresNSQT , output, 0);
 		UInt votesNS = output.at<int>(1,0);
 		UInt votesQT = output.at<int>(1,1);
 		probaSplit[1] = 1.0*votesQT / (1.0*(votesNS+votesQT));
		probaSplit[0] = 1.0 - probaSplit[1];
		printf("votesNS votesQT probaSplit[1] %i %i %f\n", votesNS, votesQT, probaSplit[1] );
		ntestsNS++;
	}

	//Choose splits to keep, score higher than :
	// FOR NS 0.3 
	// FOR QT 0.1 
	// FOR BT 0.3 
	Double thresholds[4] = {0.3, 0.1, 0.3, 0.3};

	if (probaSplit[0] > thresholds[0])
	{
		if(!authorizedSplits[0])
		{
			probaSplit[0] = 0.0;
			probaSplit[2] = 1.0;
			probaSplit[3] = 1.0;
		}
		//Match Nosplit VS BTHOR
		if(authorizedSplits[0] && authorizedSplits[2])
		{
			cv::Mat featuresNSHor = calc_features_noSplitHor(pCu, pcRdCost, alreadyCreatedCus, wholeCuFeatures);
			cv::Mat output;
	    model02[categoryCU]->getVotes( featuresNSHor , output, 0);
	 		UInt votesNS = output.at<int>(1,0);
	 		UInt votesHor = output.at<int>(1,1);
	 		probaSplit[2] = 1.0*votesHor / (1.0*(votesNS+votesHor));
			probaSplit[0] += 1.0 - probaSplit[2];
			printf("votesNS votesHor probaSplit[2] %i %i %f\n", votesNS, votesHor, probaSplit[2] );
			ntestsNS++;
		}
		//Match Nosplit VS BTVER		
		if(authorizedSplits[0] && authorizedSplits[3])
		{
			cv::Mat featuresNSVer = calc_features_noSplitVer(pCu, pcRdCost, alreadyCreatedCus, wholeCuFeatures);
			cv::Mat output;
	    model03[categoryCU]->getVotes( featuresNSVer , output, 0);
	 		UInt votesNS = output.at<int>(1,0);
	 		UInt votesVer = output.at<int>(1,1);
	 		probaSplit[3] = 1.0*votesVer / (1.0*(votesNS+votesVer));
			probaSplit[0] += 1.0 - probaSplit[3];
			printf("votesNS votesVer probaSplit[3] %i %i %f\n", votesNS, votesVer, probaSplit[3] );
			ntestsNS++;
		}
	}
	probaSplit[0] /= ntestsNS;


	//KEEP at maximum 1 BT when NoSplit or QT already chosen by classifiers
	if (probaSplit[0] > thresholds[0] || probaSplit[1] > thresholds[1])
	{
		if (probaSplit[2] > probaSplit[3])
			probaSplit[3] = 0.0;
		else
			probaSplit[2] = 0.0;
	}

	for(int i = 0; i<4; i++)
	{
		//CHOISIR LES PROBAS QU'ON GARDE
		// keptSplits[i] = (probaSplit[i] > m_probaThres);
		keptSplits[i] = (probaSplit[i] > thresholds[i]);
		printf("authorizedSplits keptSplits %i %i\n", authorizedSplits[i], keptSplits[i]);
	}
}
