import sys
import math
import re
from operator import itemgetter
from random import shuffle

#*************************************************************
def load_file(nameInputfile):

	inputfile = open(nameInputfile)

	header = []
	if nameInputfile[-4:] == "arff":
		line = inputfile.readline()
		while line != "@DATA\n":
			header.append(line[:-1])
			line = inputfile.readline()    
		header.append(line[:-1])
	
	instances=[]
	for line in inputfile:
		data = re.split(',',line[:-1])
   		instances.append(data)
    	

	lastString = instances[1][-1][-1]
	if lastString == '}':
		position_class = len(instances[1]) - 2
	else:
		position_class = len(instances[1]) - 1

	inputfile.close()

	return header, instances, position_class
#*************************************************************


# ************************************************************
def randomizeAndChose(instances,position_class,num_by_class):
	
	#shuffle whole list
	for i in range(5):
		shuffle(instances)

	#split in classes
	print " posClass,",position_class
	print " instances(posClass), len(instances)", instances[0][position_class],len(instances[0])
	instances_sorted = sorted(instances,key=itemgetter(position_class))

	#select "num_by_class" of each class
	chosen_instances = []
	
	i = 0
	while i < len(instances_sorted) - 1 :
		cur_class = instances_sorted[i][position_class]
		num_cur_class=0
		while instances_sorted[i][position_class] == cur_class and i < len(instances_sorted) - 1 :
			if num_cur_class < num_by_class:
				chosen_instances.append(instances_sorted[i])
				num_cur_class = num_cur_class + 1
			i = i + 1




	# first_class = instances_sorted[1][position_class]
	# i = 0
	# while instances_sorted[i][position_class] == first_class :
	# 	if i < num_by_class:
	# 		chosen_instances.append(instances_sorted[i])
	# 	i = i + 1


	# num_first_class = i
	# second_class = instances_sorted[num_first_class+1][position_class]
	# while instances_sorted[i][position_class] == second_class and i < len(instances_sorted) - 1 :
	# 	if i < num_first_class + num_by_class:
	# 		chosen_instances.append(instances_sorted[i])
	# 	i = i + 1

	#shuffle chosen list
	for i in range(3):
		shuffle(chosen_instances)

	return chosen_instances

#**************************************************************



if __name__ == '__main__':

	if len(sys.argv) != 3 :
	        print ("***** Usage syntax Error!!!! *****\n")
	        print ("Usage:")
	        print ("python <script> <nameLearnFile> <num_by_class>")
	        sys.exit(1) # exit
	else:
	        pass

	nameErrorFile = str(sys.argv[1])
	num_by_class = int(sys.argv[2])

	#get instances and header in file
	header, instances, position_class = load_file(nameErrorFile)
	
	#randomize and chose "num_by_class" by class
	chosen_instances = randomizeAndChose(instances,position_class,num_by_class)

	#write in file
	split_name = re.split('/',nameErrorFile)
	split_name[-1] = "random_numClass" +  str(num_by_class) + "_" + split_name[-1]
	name_output_file = split_name[0]
	for string in split_name[1:] : name_output_file = name_output_file + "/" + string

	out_file = open(name_output_file, 'w')
	for head in header : out_file.write(head + '\n')
	for instance in chosen_instances : 
		for s in instance[:-1] :
			out_file.write(s + ',')
		out_file.write(instance[-1] + '\n')
	
	out_file.close()
