#!/bin/bash

#ENTREE
# - Un fichier "videos_to_encode.txt" qui contient les videos à encoder
# - Un template cfg de base dans chaque dossier de video
# - l'executable à utiliser "orig" ou "ML"

# SORTIE 
# - Un dossier qui contient les logs pour les 4 QPs
# dans datas/MachineLearning/log/

if [ "$#" -ne 3 ]; then
 echo "WRONG NUMBER OF PARAMETERS"
 echo "./launch_encodings.sh <videos_to_encode> <n_frames> <exec_to_use>"
 exit
fi

cd "/home/tamestoy/Documents/These/JEM_Codec/HM-16.6-JEM-7.0-dev"

videos_to_encode=$1
n_frames=$2
exec_to_use=$3

all_QPs="22 27 32 37"
# all_QPs="32 37"

#ml_dir="/home/tamestoy/Documents/These/JEM_Codec/HM-16.6-JEM-7.0-dev/datas/MachineLearning/models/opencv_01_02_03_Traffic_BBPass_Cac"
ml_dir="/home/tamestoy/Documents/These/JEM_Codec/HM-16.6-JEM-7.0-dev/datas/MachineLearning/models/sklearn_0All_AllTrain"

#Recuperer dossier qui contient videos
read -r video_folder < $videos_to_encode
if [[ "$video_folder" != "Folder: "* ]]; then
	echo "FIRST LINE MUST BE \"Folder: <name of videos directory>\" "
	exit
fi
video_folder=${video_folder#"Folder: "}
echo $video_folder
DATE=`date '+%d_%m-%H_%M'`


#Boucle sur les videos contenus dans le fichier $videos_to_encode
while IFS='' read -r name_video || [[ -n "$name_video" ]]; do
  if [[ "$name_video" != Video* ]]; then
  	echo "SKIP THIS LINE "
  	echo
  	continue
  fi
  name_video=${name_video#"Video: "}
  echo $name_video
  path_video=`find $video_folder -name $name_video`

  path_cfg=${path_video%".yuv"}
  orig_cfg_file=$path_cfg".cfg"

  if [[ $exec_to_use == "orig" ]]; then
  	output_dir=datas/MachineLearning/log/${name_video%".yuv"}-orig-Fr${n_frames}-$DATE
  elif [[ $exec_to_use == "ML" ]]; then
  	output_dir=datas/MachineLearning/log/${name_video%".yuv"}-Fr${n_frames}-$DATE
	elif [[ $exec_to_use == "Stats" ]]; then
  	output_dir=datas/MachineLearning/log/${name_video%".yuv"}-Stats-Fr${n_frames}-$DATE
  else
  	echo "WRONG EXECUTABLE : ORIG or ML or Stats"
  	exit
  fi
	mkdir -p $output_dir

	#boucle sur 4 QPs
	for QP in $all_QPs; do
    	echo "QP for encoding: "$QP
    	
		temporary_cfg="temp_cfg_"$DATE".cfg"
  	cp $orig_cfg_file $temporary_cfg
    echo "InputFile                     : "$path_video >> $temporary_cfg
    echo "FramesToBeEncoded             : "$n_frames >> $temporary_cfg
    echo "QP                            : "$QP >> $temporary_cfg
	
		if [[ $exec_to_use == "orig" ]]; then
			./bin/TAppEncoderStatic_orig -c $temporary_cfg > $output_dir/sortie_encode_QP${QP}
		elif [[ $exec_to_use == "ML" ]]; then
    	echo "MachineLearningDir            : "$ml_dir >> $temporary_cfg
			./bin/TAppEncoderStatic_ML -c $temporary_cfg -out $output_dir> $output_dir/sortie_encode_QP${QP}
			sleep 2
		elif [[ $exec_to_use == "Stats" ]]; then
			./bin/TAppEncoderStatic_Stats -c $temporary_cfg -out $output_dir> $output_dir/sortie_encode_QP${QP}
		fi

		cp $temporary_cfg $output_dir/"configuration.cfg"			
		rm $temporary_cfg
	done
    
done < $videos_to_encode
