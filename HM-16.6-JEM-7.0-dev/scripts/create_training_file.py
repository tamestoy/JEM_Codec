import numpy as np
import cv2
from PIL import Image
import sys
from struct import *
import array
import matplotlib.pyplot as plt
import math

QP_offsets = [1,1,4,5,6];


# ************************************************************
def getFeaturesCU(predsBloc4x4,arraySplitData,pelX,pelY,widthCU,heightCU):
	
	mean_pixmeans = 0
	meanHor = 0
	meanVer = 0
	gradx = 0
	grady = 0
	nbblocks = 0
	for j in range(pelX,pelX+widthCU,sizeBlock):
		for k in range(pelY,pelY+heightCU,sizeBlock):
			idX=j//sizeBlock
			idY=k//sizeBlock
			if(idX<len(predsBloc4x4) and idY<len(predsBloc4x4[0])):
				nbblocks = nbblocks + 1 
				mean_pixmeans=mean_pixmeans+1.0*predsBloc4x4[idX][idY]["mean"]
				gradx=gradx+1.0*predsBloc4x4[idX][idY]["gradx"]
				grady=grady+1.0*predsBloc4x4[idX][idY]["grady"]
				meanHor=meanHor+ 1.0 * predsBloc4x4[idX][idY]["hor"]
				meanVer=meanVer+ 1.0 * predsBloc4x4[idX][idY]["ver"]

	mean_pixmeans=mean_pixmeans/nbblocks
	gradx=gradx/nbblocks
	grady=grady/nbblocks
	meanHor=meanHor/nbblocks
	meanVer=meanVer/nbblocks

	varPix=0
	energy=0
	varHor=0
	varVer=0
	maxEcartHor = 0
	maxEcartVer = 0
	for j in range(pelX,pelX+widthCU,sizeBlock):
		for k in range(pelY,pelY+heightCU,sizeBlock):
			idX=j//sizeBlock
			idY=k//sizeBlock
			if(idX<len(predsBloc4x4) and idY<len(predsBloc4x4[0])):
				# diffMean=abs(predsBloc4x4[idX][idY][2]-mean_pixmeans)
				diffMean=abs(predsBloc4x4[idX][idY]['mean']+predsBloc4x4[idX][idY]["var"]-mean_pixmeans)
				diffMean=(diffMean + abs(predsBloc4x4[idX][idY]['mean']-predsBloc4x4[idX][idY]["var"]-mean_pixmeans))/2.0
				varPix=varPix+diffMean

				energy = energy + abs(predsBloc4x4[idX][idY]["hor"]-meanHor) * predsBloc4x4[idX][idY]["gradx"]
				energy = energy + abs(predsBloc4x4[idX][idY]["ver"]-meanVer) * predsBloc4x4[idX][idY]["grady"]

				varHor = varHor + 1.0 * abs(predsBloc4x4[idX][idY]["hor"]-meanHor)
				varVer = varVer + 1.0 * abs(predsBloc4x4[idX][idY]["ver"]-meanVer)

				maxEcartHor = max(maxEcartHor,abs(predsBloc4x4[idX][idY]["hor"]-meanHor))
				maxEcartVer = max(maxEcartVer,abs(predsBloc4x4[idX][idY]["ver"]-meanVer))


	varPix = 1.0 * varPix / nbblocks
	energy = 1.0 * energy / nbblocks
	varHor = 1.0 * varHor / nbblocks
	varVer = 1.0 * varVer / nbblocks

	# logAir=calcAirNeighbors(predsBloc4x4,arraySplitData,pelX,pelY,widthCU,heightCU)

	return {'meanPix':mean_pixmeans, 'varPix':varPix, 'meanHor':meanHor, 'meanVer':meanVer, 'gradx':gradx, 'grady':grady,
	'energy':energy,'varHor':varHor,'varVer':varVer, 'maxEcartHor' : maxEcartHor, 'maxEcartVer' : maxEcartVer}
	# 'energy':energy,'varHor':varHor,'varVer':varVer, 'maxEcartHor' : maxEcartHor, 'maxEcartVer' : maxEcartVer, 'logAir':logAir}

# ************************************************************


# ************************************************************
def calcAirNeighbors(predsBloc4x4,arraySplitData,pelX,pelY,widthCU,heightCU):

	sizeCTU=128
	ctuIdX = pelX//sizeBlock
	ctuIdY = pelY//sizeBlock
	logAireMoy=0
	nbCuAdj=0
	for i in range(0,widthCU):
		# print "\n", ctuIdX ,ctuIdY+i,len(arraySplitData[ctuIdX-1])
		if(ctuIdX > 0 and ctuIdY+i<len(arraySplitData[ctuIdX-1])):
			cuGauche = arraySplitData[ctuIdX-1][ctuIdY+i]
			aire=math.log(cuGauche[0]*cuGauche[1],2)
			# print "Gauche", aire
			logAireMoy=logAireMoy+aire
			nbCuAdj=nbCuAdj+1

	for i in range(0,heightCU):
		# print ctuIdY ,ctuIdX+i,len(arraySplitData)
		if(ctuIdY > 0 and ctuIdX+i<len(arraySplitData)):
			cuHaut = arraySplitData[ctuIdX+i][ctuIdY-1]
			aire=math.log(cuHaut[0]*cuHaut[1],2)
			# print "haut", aire
			logAireMoy=logAireMoy+aire
			nbCuAdj=nbCuAdj+1

	logAireCu = math.log(widthCU*heightCU,2)
	if(nbCuAdj>0):		
		logAireMoy = logAireMoy*1.0/nbCuAdj
		# print("logAireCu logAireMoy ", logAireCu, logAireMoy)
	else:
		logAireMoy = 0 

	return logAireMoy
# ************************************************************


# ************************************************************
def getSplitInfos(nameDecoupeFile,width,height,sizeBlock=4):
	
	DecoupeFile = open(nameDecoupeFile)

	for i in range(1): DecoupeFile.next() # skip first line

	W = width // sizeBlock
	H = height // sizeBlock
	arraySplitData = [0] * W
	for i in range(W):
		arraySplitData[i] = [0] * H
		for j in range(H):
			arraySplitData[i][j] = [0] * 2

	for line in DecoupeFile:
		donnees = map(int,line.split())
		pelX = donnees[0]
		pelY = donnees[1]
		widthCU = donnees[2]
		heightCU = donnees[3]
		for j in range(pelX,pelX+widthCU,sizeBlock):
			for k in range(pelY,pelY+heightCU,sizeBlock):
				idX=j//sizeBlock
				idY=k//sizeBlock
				if idX < W and idY < H:
					# print idX, idY, widthCU, heightCU
					arraySplitData[idX][idY][0] = widthCU
					arraySplitData[idX][idY][1] = heightCU

	DecoupeFile.close()
	return arraySplitData
# ************************************************************


# ************************************************************
def getPredInfos(namePredFile,width,height,sizeBlock=4):
	
	predFile = open(namePredFile)

	for i in range(1): predFile.next() # skip first line

	W = width // sizeBlock
	H = height // sizeBlock

	predsBloc4x4 = [0] * W
	for i in range(W):
		predsBloc4x4[i] = [0] * H
		for j in range(H):
			predsBloc4x4[i][j] = {}

	for line in predFile:
		donnees = map(int,line.split())
		# print(donnees)
		# print(donnees[0]//4,donnees[1]//4)
		idX=donnees[0]//sizeBlock
		idY=donnees[1]//sizeBlock

		#vecteur mvt INTER horizontal
		predsBloc4x4[idX][idY]["hor"] = donnees[2]
		#vecteur mvt INTER vertical
		predsBloc4x4[idX][idY]["ver"] = donnees[3]
		#mode pred INTRA
		predsBloc4x4[idX][idY]["intra"] = donnees[4]
		#meanenne pixels bloc
		predsBloc4x4[idX][idY]['mean'] = donnees[5]
		#grad X
		predsBloc4x4[idX][idY]["gradx"] = donnees[6]
		#grad Y
		predsBloc4x4[idX][idY]["grady"] = donnees[7]
		#Variance pixels
		predsBloc4x4[idX][idY]["var"] = donnees[8]

		# blocsParcourus4x4[donnees[0]//sizeBlock][donnees[1]//sizeBlock] = 0

	predFile.close()
	return predsBloc4x4
# ************************************************************


# *************************************************************
def getDimensionVideo(pathFolder):
	predFile = open(pathFolder + '/block_info_1')

	for line in predFile:
		donnees = line.split()
	
	donnees = map(int,donnees)
	widthVideo = donnees[0]+4
	heightVideo = donnees[1]+4
	return widthVideo,heightVideo

# *************************************************************

# *************************************************************
def getQP(pathFolder):
	# predFile = open(pathFolder + '/QP')

	# for line in predFile:
	# 	donnees = line.split()
	
	# nomDossier = donnees[0]
	# #../datas/learning_data/JVET_SDRA_BQTerrace_1920x1080_60_22_JEM_RA
	# QP = nomDossier[-9:-7]
	donnees = pathFolder.split('_')
	QP = donnees[-6]

	if (QP not in ['22','27','30','32','37']):
		print "WRONG QP !! CHECK DIRECTORY NAME"
		sys.exit(1)

	return QP

# *************************************************************



# *************************************************************
def getTemporality(num_frame):

	divider = 16
	temporality = 0
	while divider >= 1 :
		if num_frame%divider == 0:
			break
		divider = divider / 2
	 	temporality = temporality + 1

	return temporality

# *************************************************************


# ************************************************************
def indexInsert(energyList,energie):
	
	i=0
	length = len(energyList)
	if(length==0):
		return i

	while(energie>energyList[i]):
		# print ("index ",i, len(energyList), energie, energyList[i])
		i = i+1+length/100
		if(i >= length):
			return i

	return i
# ************************************************************


# ************************************************************
def fillAttributes(trainingFile,choixAttributs):
	
	trainingFile.write("@RELATION splitCU\n")

	if(choixAttributs == "test1All" or choixAttributs == "test0All" 
		or choixAttributs == "test0BT" or choixAttributs == "test1BT"):


		trainingFile.write("@ATTRIBUTE QP REAL\n")
		# trainingFile.write("@ATTRIBUTE resolution REAL\n")
		trainingFile.write("@ATTRIBUTE varPix REAL\n")
		trainingFile.write("@ATTRIBUTE gradx REAL\n")
		trainingFile.write("@ATTRIBUTE grady REAL\n")
		trainingFile.write("@ATTRIBUTE gradx/grady REAL\n")
		trainingFile.write("@ATTRIBUTE varMv REAL\n")
		trainingFile.write("@ATTRIBUTE maxEcartMV REAL\n")

		trainingFile.write("@ATTRIBUTE varPix0 REAL\n")
		trainingFile.write("@ATTRIBUTE varMv0 REAL\n")
		trainingFile.write("@ATTRIBUTE maxEcartMv0 REAL\n")

		trainingFile.write("@ATTRIBUTE varPix1 REAL\n")
		trainingFile.write("@ATTRIBUTE varMv1 REAL\n")
		trainingFile.write("@ATTRIBUTE maxEcartMv1 REAL\n")

		trainingFile.write("@ATTRIBUTE varPix2 REAL\n")
		trainingFile.write("@ATTRIBUTE varMv2 REAL\n")
		trainingFile.write("@ATTRIBUTE maxEcartMv2 REAL\n")

		trainingFile.write("@ATTRIBUTE varPix3 REAL\n")
		trainingFile.write("@ATTRIBUTE varMv3 REAL\n")
		trainingFile.write("@ATTRIBUTE maxEcartMv3 REAL\n")
		
		trainingFile.write("@ATTRIBUTE horIncMeanPix REAL\n")
		trainingFile.write("@ATTRIBUTE horIncMeanMv REAL\n")
		trainingFile.write("@ATTRIBUTE horIncVarPix REAL\n")
		trainingFile.write("@ATTRIBUTE horIncVarMv REAL\n")
		trainingFile.write("@ATTRIBUTE verIncMeanPix REAL\n")
		trainingFile.write("@ATTRIBUTE verIncMeanMv REAL\n")
		trainingFile.write("@ATTRIBUTE verIncVarPix REAL\n")
		trainingFile.write("@ATTRIBUTE verIncVarMv REAL\n")

		trainingFile.write("@ATTRIBUTE ratioEnergyQt REAL\n")					
		trainingFile.write("@ATTRIBUTE ratio_varPix REAL\n")					
		trainingFile.write("@ATTRIBUTE ratio_varMv REAL\n")					

		# trainingFile.write("@ATTRIBUTE meanLowerVarPix REAL\n")
		# trainingFile.write("@ATTRIBUTE varLowerMeanPix REAL\n")
		# trainingFile.write("@ATTRIBUTE varLowerMeanMV REAL\n")
		# trainingFile.write("@ATTRIBUTE meanLowerVarMV REAL\n")
		# trainingFile.write("@ATTRIBUTE varLowerVarMV REAL\n")

		trainingFile.write("@ATTRIBUTE split {0,1}\n")



	elif choixAttributs == "testNosplitQt":
		trainingFile.write("@ATTRIBUTE QP REAL\n")
		# trainingFile.write("@ATTRIBUTE temporality REAL\n")

		trainingFile.write("@ATTRIBUTE varPix REAL\n")
		trainingFile.write("@ATTRIBUTE varLowerMeanPix REAL\n")
		trainingFile.write("@ATTRIBUTE meanLowerVarPix REAL\n")

		trainingFile.write("@ATTRIBUTE ratioEnergyQt REAL\n")					
	
		trainingFile.write("@ATTRIBUTE varMv REAL\n")
		trainingFile.write("@ATTRIBUTE maxEcartMV REAL\n")
		trainingFile.write("@ATTRIBUTE varLowerMeanMV REAL\n")
		trainingFile.write("@ATTRIBUTE meanLowerVarMV REAL\n")
		trainingFile.write("@ATTRIBUTE varLowerVarMV REAL\n")

		trainingFile.write("@ATTRIBUTE split {0,1}\n")


	elif(choixAttributs == "testNosplitHor"):

		trainingFile.write("@ATTRIBUTE QP REAL\n")
		# trainingFile.write("@ATTRIBUTE temporality REAL\n")
		trainingFile.write("@ATTRIBUTE ratioSize REAL\n")

		trainingFile.write("@ATTRIBUTE gradx/grady REAL\n")
		trainingFile.write("@ATTRIBUTE varPix REAL\n")
		trainingFile.write("@ATTRIBUTE varMv REAL\n")
		trainingFile.write("@ATTRIBUTE maxEcartMV REAL\n")
		# trainingFile.write("@ATTRIBUTE logAir REAL\n")

		# trainingFile.write("@ATTRIBUTE varPixTop REAL\n")
		# trainingFile.write("@ATTRIBUTE varMvTop REAL\n")
		# trainingFile.write("@ATTRIBUTE maxEcartMvTop REAL\n")

		# trainingFile.write("@ATTRIBUTE varPixLow REAL\n")
		# trainingFile.write("@ATTRIBUTE varMvLow REAL\n")
		# trainingFile.write("@ATTRIBUTE maxEcartMvLow REAL\n")

		trainingFile.write("@ATTRIBUTE ratio_varPix REAL\n")
		trainingFile.write("@ATTRIBUTE ratio_minVarPix REAL\n")
		trainingFile.write("@ATTRIBUTE ratio_varMv REAL\n")
		trainingFile.write("@ATTRIBUTE ratio_minVarMv REAL\n")
		trainingFile.write("@ATTRIBUTE ratio_ecartMax REAL\n")

		trainingFile.write("@ATTRIBUTE ratioEnergyHor REAL\n")	

		trainingFile.write("@ATTRIBUTE split {0,2}\n")


	elif(choixAttributs == "testNosplitVer"):
		trainingFile.write("@ATTRIBUTE QP REAL\n")
		# trainingFile.write("@ATTRIBUTE temporality REAL\n")
		trainingFile.write("@ATTRIBUTE ratioSize REAL\n")

		trainingFile.write("@ATTRIBUTE gradx/grady REAL\n")
		trainingFile.write("@ATTRIBUTE varPix REAL\n")
		trainingFile.write("@ATTRIBUTE varMv REAL\n")
		trainingFile.write("@ATTRIBUTE maxEcartMV REAL\n")

		trainingFile.write("@ATTRIBUTE ratio_varPix REAL\n")
		trainingFile.write("@ATTRIBUTE ratio_minVarPix REAL\n")
		trainingFile.write("@ATTRIBUTE ratio_varMv REAL\n")
		trainingFile.write("@ATTRIBUTE ratio_minVarMv REAL\n")
		trainingFile.write("@ATTRIBUTE ratio_ecartMax REAL\n")

		trainingFile.write("@ATTRIBUTE ratioEnergyVer REAL\n")	

		trainingFile.write("@ATTRIBUTE split {0,3}\n")


	elif(choixAttributs == "testWhichBT"):
		# trainingFile.write("@ATTRIBUTE aireCU REAL\n")
		trainingFile.write("@ATTRIBUTE ratioSize REAL\n")
		# trainingFile.write("@ATTRIBUTE sizeImage REAL\n")
		trainingFile.write("@ATTRIBUTE gradx/grady REAL\n")

		# trainingFile.write("@ATTRIBUTE varPixBtHor REAL\n")
		# trainingFile.write("@ATTRIBUTE varMvBtHor REAL\n")
		# trainingFile.write("@ATTRIBUTE ecartMaxBtHor REAL\n")

		# trainingFile.write("@ATTRIBUTE varPixBtVer REAL\n")
		# trainingFile.write("@ATTRIBUTE varMvBtVer REAL\n")
		# trainingFile.write("@ATTRIBUTE ecartMaxBtVer REAL\n")

		# trainingFile.write("@ATTRIBUTE ratio_Energy REAL\n")


		trainingFile.write("@ATTRIBUTE ratio_varPix REAL\n")
		trainingFile.write("@ATTRIBUTE ratio_minVarPix REAL\n")
		trainingFile.write("@ATTRIBUTE ratio_varMv REAL\n")
		trainingFile.write("@ATTRIBUTE ratio_ecartMax REAL\n")
		trainingFile.write("@ATTRIBUTE ratio_minVarMv REAL\n")
		trainingFile.write("@ATTRIBUTE ratio_Energy REAL\n")
		
		# trainingFile.write("@ATTRIBUTE varPixTop REAL\n")
		# trainingFile.write("@ATTRIBUTE varMvTop REAL\n")
		# trainingFile.write("@ATTRIBUTE maxEcartMvTop REAL\n")

		# trainingFile.write("@ATTRIBUTE varPixLow REAL\n")
		# trainingFile.write("@ATTRIBUTE varMvLow REAL\n")
		# trainingFile.write("@ATTRIBUTE maxEcartMvLow REAL\n")

		# trainingFile.write("@ATTRIBUTE varPixLeft REAL\n")
		# trainingFile.write("@ATTRIBUTE varMvLeft REAL\n")
		# trainingFile.write("@ATTRIBUTE maxEcartMvLeft REAL\n")

		# trainingFile.write("@ATTRIBUTE varPixRight REAL\n")
		# trainingFile.write("@ATTRIBUTE varMvRight REAL\n")
		# trainingFile.write("@ATTRIBUTE maxEcartMvRight REAL\n")

		# trainingFile.write("@ATTRIBUTE EnergyHor REAL\n")
		# trainingFile.write("@ATTRIBUTE EnergyVer REAL\n")

		trainingFile.write("@ATTRIBUTE split {2,3}\n")


	elif(choixAttributs == "testQtHor"):
		trainingFile.write("@ATTRIBUTE aireCU REAL\n")
		trainingFile.write("@ATTRIBUTE gradx/grady REAL\n")

		trainingFile.write("@ATTRIBUTE ratio_varPix REAL\n")
		trainingFile.write("@ATTRIBUTE ratio_varMv REAL\n")
		trainingFile.write("@ATTRIBUTE ratio_Energy REAL\n")

		trainingFile.write("@ATTRIBUTE split {1,2}\n")


	elif(choixAttributs == "testQtVer"):
		trainingFile.write("@ATTRIBUTE aireCU REAL\n")
		trainingFile.write("@ATTRIBUTE gradx/grady REAL\n")

		trainingFile.write("@ATTRIBUTE ratio_varPix REAL\n")
		trainingFile.write("@ATTRIBUTE ratio_varMv REAL\n")
		trainingFile.write("@ATTRIBUTE ratio_Energy REAL\n")

		trainingFile.write("@ATTRIBUTE split {1,3}\n")

	elif(choixAttributs == "testQtHorVer"):
		trainingFile.write("@ATTRIBUTE aireCU REAL\n")
		trainingFile.write("@ATTRIBUTE ratioSize REAL\n")
		trainingFile.write("@ATTRIBUTE gradx/grady REAL\n")

		trainingFile.write("@ATTRIBUTE ratio_varPix_HorVer REAL\n")
		trainingFile.write("@ATTRIBUTE ratio_varMv_HorVer REAL\n")
		trainingFile.write("@ATTRIBUTE ratio_Energy_HorVer REAL\n")

		trainingFile.write("@ATTRIBUTE ratio_varPix_QTVer REAL\n")
		trainingFile.write("@ATTRIBUTE ratio_varMv_QTVer REAL\n")
		trainingFile.write("@ATTRIBUTE ratio_Energy_QTVer REAL\n")

		trainingFile.write("@ATTRIBUTE split {1,2,3}\n")

	else:
		trainingFile.close()
		print "MAUVAIS CHOIX D'ATTRIBUTS"
		sys.exit(1)

	trainingFile.write("@DATA\n")

# ************************************************************

#************************************************************
def calcFeaturesQTvsAll(predsBloc4x4,arraySplitData,pelX,pelY,widthCU,heightCU,QP,temporality):


# # trainingFile.write("@ATTRIBUTE meanLowerVarPix REAL\n")
# # trainingFile.write("@ATTRIBUTE varLowerMeanPix REAL\n")
# # trainingFile.write("@ATTRIBUTE varLowerMeanMV REAL\n")
# # trainingFile.write("@ATTRIBUTE meanLowerVarMV REAL\n")
# # trainingFile.write("@ATTRIBUTE varLowerVarMV REAL\n")

# trainingFile.write("@ATTRIBUTE split {0,1}\n")

# trainingFile.write("@ATTRIBUTE QP REAL\n")
	choiceFeatures = []
	featuresCU = getFeaturesCU(predsBloc4x4,arraySplitData,pelX,pelY,widthCU,heightCU)
	choiceFeatures.append(int(QP)+QP_offsets[temporality]) 

	# w_image = len(predsBloc4x4)*4
	# choiceFeatures.append(w_image) 
	# choiceFeatures.append(temporality)

	#***************************************************************
	#PARTIE CU entiere
	#*************************************************************** 
	# trainingFile.write("@ATTRIBUTE varPix REAL\n")
	# trainingFile.write("@ATTRIBUTE gradx REAL\n")
	# trainingFile.write("@ATTRIBUTE grady REAL\n")
	# trainingFile.write("@ATTRIBUTE gradx/grady REAL\n")
	# trainingFile.write("@ATTRIBUTE varMv REAL\n")
	# trainingFile.write("@ATTRIBUTE maxEcartMV REAL\n")
	choiceFeatures.append(featuresCU['varPix'])
	choiceFeatures.append(featuresCU['gradx'])
	choiceFeatures.append(featuresCU['grady'])
	choiceFeatures.append(featuresCU['gradx']/max(0.01,featuresCU['grady']))
	choiceFeatures.append(featuresCU['varHor']+featuresCU['varVer'])
	choiceFeatures.append(featuresCU['maxEcartHor']+featuresCU['maxEcartVer'])

	#***************************************************************
	#SUR LES 4 SOUS-CUs
	#***************************************************************  
	# trainingFile.write("@ATTRIBUTE varPix0 REAL\n")
	# trainingFile.write("@ATTRIBUTE varMv0 REAL\n")
	# trainingFile.write("@ATTRIBUTE maxEcartMv0 REAL\n")

	# trainingFile.write("@ATTRIBUTE varPix1 REAL\n")
	# trainingFile.write("@ATTRIBUTE varMv1 REAL\n")
	# trainingFile.write("@ATTRIBUTE maxEcartMv1 REAL\n")

	# trainingFile.write("@ATTRIBUTE varPix2 REAL\n")
	# trainingFile.write("@ATTRIBUTE varMv2 REAL\n")
	# trainingFile.write("@ATTRIBUTE maxEcartMv2 REAL\n")

	# trainingFile.write("@ATTRIBUTE varPix3 REAL\n")
	# trainingFile.write("@ATTRIBUTE varMv3 REAL\n")
	# trainingFile.write("@ATTRIBUTE maxEcartMv3 REAL\n")
	meanVarPix = 0
	energyQt = 0
	meanVarMv = 0
	featuresSubCU = [[0,0],[0,0]]
	for i in range(0,2):
		for j in range(0,2): 
			pelXsubCu = pelX + i * widthCU // 2
			pelYsubCu = pelY + j * heightCU // 2
			featuresSubCU[i][j] = getFeaturesCU(predsBloc4x4,arraySplitData,pelXsubCu,pelYsubCu,widthCU//2,heightCU//2)
			meanVarPix = meanVarPix +  0.25*featuresSubCU[i][j]['varPix'] 
			energyQt = energyQt + featuresSubCU[i][j]['energy'] / 4.0
			meanVarMv = meanVarMv + 0.25*featuresSubCU[i][j]['varHor'] + 0.25*featuresSubCU[i][j]['varVer']

			choiceFeatures.append(featuresSubCU[i][j]['varPix'])
			choiceFeatures.append(1.0*featuresSubCU[i][j]['varHor'] + 1.0*featuresSubCU[i][j]['varVer'])
			choiceFeatures.append(1.0*featuresSubCU[i][j]['maxEcartHor'] + 1.0 * featuresSubCU[i][j]['maxEcartVer'])


	#***************************************************************
	#PARTIE HOR DIRECTIONAL INCONSISTENCY
	#***************************************************************
	# trainingFile.write("@ATTRIBUTE horIncMeanPix REAL\n")
	# trainingFile.write("@ATTRIBUTE horIncMeanMv REAL\n")
	# trainingFile.write("@ATTRIBUTE horIncVarPix REAL\n")
	# trainingFile.write("@ATTRIBUTE horIncVarMv REAL\n")
	horInconsistencyMeanPix = abs(featuresSubCU[0][0]['meanPix'] - featuresSubCU[0][1]['meanPix']) + abs(featuresSubCU[1][0]['meanPix'] - featuresSubCU[1][1]['meanPix'])
	horInconsistencyMeanMv = abs(featuresSubCU[0][0]['meanVer'] - featuresSubCU[0][1]['meanVer']) + abs(featuresSubCU[1][0]['meanVer'] - featuresSubCU[1][1]['meanVer'])
	horInconsistencyMeanMv = horInconsistencyMeanMv + abs(featuresSubCU[0][0]['meanHor'] - featuresSubCU[0][1]['meanHor']) + abs(featuresSubCU[1][0]['meanHor'] - featuresSubCU[1][1]['meanHor'])

	horInconsistencyVarPix = abs(featuresSubCU[0][0]['varPix'] - featuresSubCU[0][1]['varPix']) + abs(featuresSubCU[1][0]['varPix'] - featuresSubCU[1][1]['varPix'])
	horInconsistencyVarMv = abs(featuresSubCU[0][0]['varVer'] - featuresSubCU[0][1]['varVer']) + abs(featuresSubCU[1][0]['varVer'] - featuresSubCU[1][1]['varVer'])
	horInconsistencyVarMv = horInconsistencyVarMv + abs(featuresSubCU[0][0]['varHor'] - featuresSubCU[0][1]['varHor']) + abs(featuresSubCU[1][0]['varHor'] - featuresSubCU[1][1]['varHor'])
	
	choiceFeatures.append(horInconsistencyMeanPix)
	choiceFeatures.append(horInconsistencyMeanMv)
	choiceFeatures.append(horInconsistencyVarPix)
	choiceFeatures.append(horInconsistencyVarMv)

	#***************************************************************
	#PARTIE Ver DIRECTIONAL INCONSISTENCY
	#***************************************************************
	# trainingFile.write("@ATTRIBUTE verIncMeanPix REAL\n")
	# trainingFile.write("@ATTRIBUTE verIncMeanMv REAL\n")
	# trainingFile.write("@ATTRIBUTE verIncVarPix REAL\n")
	# trainingFile.write("@ATTRIBUTE verIncVarMv REAL\n")
	verInconsistencyMeanPix = abs(featuresSubCU[0][0]['meanPix'] - featuresSubCU[1][0]['meanPix']) + abs(featuresSubCU[0][1]['meanPix'] - featuresSubCU[1][1]['meanPix'])
	verInconsistencyMeanMv = abs(featuresSubCU[0][0]['meanVer'] - featuresSubCU[1][0]['meanVer']) + abs(featuresSubCU[0][1]['meanVer'] - featuresSubCU[1][1]['meanVer'])
	verInconsistencyMeanMv = verInconsistencyMeanMv + abs(featuresSubCU[0][0]['meanHor'] - featuresSubCU[1][0]['meanHor']) + abs(featuresSubCU[0][1]['meanHor'] - featuresSubCU[1][1]['meanHor'])

	verInconsistencyVarPix = abs(featuresSubCU[0][0]['varPix'] - featuresSubCU[1][0]['varPix']) + abs(featuresSubCU[0][1]['varPix'] - featuresSubCU[1][1]['varPix'])
	verInconsistencyVarMv = abs(featuresSubCU[0][0]['varVer'] - featuresSubCU[1][0]['varVer']) + abs(featuresSubCU[0][1]['varVer'] - featuresSubCU[1][1]['varVer'])
	verInconsistencyVarMv = verInconsistencyVarMv + abs(featuresSubCU[0][0]['varHor'] - featuresSubCU[1][0]['varHor']) + abs(featuresSubCU[0][1]['varHor'] - featuresSubCU[1][1]['varHor'])

	choiceFeatures.append(verInconsistencyMeanPix)
	choiceFeatures.append(verInconsistencyMeanMv)
	choiceFeatures.append(verInconsistencyVarPix)
	choiceFeatures.append(verInconsistencyVarMv)


	#***************************************************************
	#PARTIE RATIOS
	#***************************************************************
	# trainingFile.write("@ATTRIBUTE ratio_varPix REAL\n")					
	# trainingFile.write("@ATTRIBUTE ratio_varMv REAL\n")					
	# trainingFile.write("@ATTRIBUTE ratioEnergyQt REAL\n")					

	energyCu = featuresCU['energy']
	choiceFeatures.append(featuresCU['varPix']/max(0.1,meanVarPix))
	choiceFeatures.append((featuresCU['varHor']+featuresCU['varVer'])/max(0.1,meanVarMv))
	choiceFeatures.append(min(10.0,energyCu/max(0.1,energyQt)))


	return choiceFeatures

#**************************************************************


#************************************************************
def calcFeaturesNosplitQT(predsBloc4x4,arraySplitData,pelX,pelY,widthCU,heightCU,QP,temporality):

# trainingFile.write("@ATTRIBUTE QP REAL\n")
# # trainingFile.write("@ATTRIBUTE temporality REAL\n")

# trainingFile.write("@ATTRIBUTE varPix REAL\n")
# trainingFile.write("@ATTRIBUTE varLowerMeanPix REAL\n")
# trainingFile.write("@ATTRIBUTE meanLowerVarPix REAL\n")

# trainingFile.write("@ATTRIBUTE ratioEnergyQt REAL\n")         
# # trainingFile.write("@ATTRIBUTE varMv REAL\n")
# trainingFile.write("@ATTRIBUTE maxEcartMV REAL\n")
# # trainingFile.write("@ATTRIBUTE varLowerMeanMV REAL\n")
# trainingFile.write("@ATTRIBUTE meanLowerVarMV REAL\n")
# # trainingFile.write("@ATTRIBUTE varLowerVarMV REAL\n")

# trainingFile.write("@ATTRIBUTE split {0,1}\n")

  choiceFeatures = []
  featuresCU = getFeaturesCU(predsBloc4x4,arraySplitData,pelX,pelY,widthCU,heightCU)
  choiceFeatures.append(int(QP)+QP_offsets[temporality]) 
  # choiceFeatures.append(temporality)
  

  #***************************************************************
  #PARTIE Pixels
  #***************************************************************
  choiceFeatures.append(featuresCU['varPix'])

  # Data on Sub CUs 
  varMeanPix = 0
  meanVarPix = 0
  energyQt = 0
  
  varMeanHor = 0
  varMeanVer = 0
  meanVarMv = 0
  featuresSubCU = [[0,0],[0,0]]
  for i in range(0,2):
    for j in range(0,2): 
      pelXsubCu = pelX + i * widthCU // 2
      pelYsubCu = pelY + j * heightCU // 2
      featuresSubCU[i][j] = getFeaturesCU(predsBloc4x4,arraySplitData,pelXsubCu,pelYsubCu,widthCU//2,heightCU//2)
      varMeanPix = varMeanPix +  0.25*abs(featuresSubCU[i][j]['meanPix'] -featuresCU['meanPix'])
      meanVarPix = meanVarPix +  0.25*featuresSubCU[i][j]['varPix'] 
      energyQt = energyQt + featuresSubCU[i][j]['energy'] / 4.0

      varMeanHor =  varMeanHor + 0.25*abs(featuresSubCU[i][j]['meanHor']-featuresCU['meanHor'])
      varMeanVer =  varMeanVer + 0.25*abs(featuresSubCU[i][j]['meanVer']-featuresCU['meanVer'])
      meanVarMv = meanVarMv + 0.25*featuresSubCU[i][j]['varHor'] + 0.25*featuresSubCU[i][j]['varVer']
  
  varVarPix = 0
  varVarMv = 0
  for i in range(0,2):
    for j in range(0,2): 
      varVarPix = varVarPix + 0.25*abs(featuresSubCU[i][j]['varPix'] - meanVarPix)
      varVarMv = varVarMv + 0.25*abs(featuresSubCU[i][j]['varHor'] - varMeanHor) + 0.25*abs(featuresSubCU[i][j]['varVer'] - varMeanVer)

  choiceFeatures.append(varMeanPix)
  choiceFeatures.append(meanVarPix)


  #***************************************************************
  #PARTIE MV blocks
  #***************************************************************
  energyCu = featuresCU['energy']
  choiceFeatures.append(min(10.0,energyCu/max(0.1,energyQt)))

  choiceFeatures.append(featuresCU['varHor']+featuresCU['varVer'])
  choiceFeatures.append(featuresCU['maxEcartHor']+featuresCU['maxEcartVer'])

  choiceFeatures.append(varMeanHor+varMeanVer)
  choiceFeatures.append(meanVarMv)
  choiceFeatures.append(varVarMv)


  return choiceFeatures

#**************************************************************




#************************************************************
def calcFeaturesNosplitHor(predsBloc4x4,arraySplitData,pelX,pelY,widthCU,heightCU,QP,temporality):
# trainingFile.write("@ATTRIBUTE QP REAL\n")
# # trainingFile.write("@ATTRIBUTE temporality REAL\n")
# trainingFile.write("@ATTRIBUTE ratioSize REAL\n")

# trainingFile.write("@ATTRIBUTE gradx/grady REAL\n")
# trainingFile.write("@ATTRIBUTE varPix REAL\n")
# trainingFile.write("@ATTRIBUTE varMv REAL\n")
# trainingFile.write("@ATTRIBUTE maxEcartMV REAL\n")
# trainingFile.write("@ATTRIBUTE logAir REAL\n")

# trainingFile.write("@ATTRIBUTE ratio_varPix REAL\n")
# trainingFile.write("@ATTRIBUTE ratio_minVarPix REAL\n")
# trainingFile.write("@ATTRIBUTE ratio_varMv REAL\n")
# trainingFile.write("@ATTRIBUTE ratio_minVarMv REAL\n")
# trainingFile.write("@ATTRIBUTE ratio_ecartMax REAL\n")

# trainingFile.write("@ATTRIBUTE ratioEnergyHor REAL\n")	

#trainingFile.write("@ATTRIBUTE split 	{0,2}\n")

	choiceFeatures = []
	featuresCU = getFeaturesCU(predsBloc4x4,arraySplitData,pelX,pelY,widthCU,heightCU)
	choiceFeatures.append(int(QP)+QP_offsets[temporality]) 
	# choiceFeatures.append(temporality) 
	choiceFeatures.append(math.log(widthCU*1.0/heightCU,2))
	

	#***************************************************************
	#PARTIE Pixels
	#***************************************************************
	if (featuresCU['gradx']*featuresCU['grady']!=0.0):
		choiceFeatures.append(featuresCU['gradx']/featuresCU['grady'])
	else:
		choiceFeatures.append(0.0)

	choiceFeatures.append(featuresCU['varPix'])
	choiceFeatures.append(featuresCU['varHor'] + featuresCU['varVer'])
	choiceFeatures.append(featuresCU['maxEcartHor']+featuresCU['maxEcartVer'])
	# choiceFeatures.append(featuresCU['logAir'])


	#HORIZONTAL
	BThorVarPix = 0
	BThorMinVarPix = 100000000000000000
	BThorEnergy = 0
	BThorVarMv = 0
	BThorEcartMV = 0
	BThorMinVarMv = 100000000000000000
	for i in range(0,2):
		pelXsubCu = pelX 
		pelYsubCu = pelY + i * heightCU // 2
		featuresSubCU = getFeaturesCU(predsBloc4x4,arraySplitData,pelXsubCu,pelYsubCu,widthCU,heightCU//2)
		BThorVarPix = BThorVarPix + 0.5*featuresSubCU['varPix']
		BThorMinVarPix = min(BThorMinVarPix, featuresSubCU['varPix'])
		BThorEnergy = BThorEnergy + featuresSubCU['energy'] / 2.0 

		BThorVarMv = BThorVarMv + 0.5*featuresSubCU['varHor'] + 0.5*featuresSubCU['varVer']
		BThorEcartMV = BThorEcartMV + 0.5 *featuresSubCU['maxEcartHor'] + 0.5  * featuresSubCU['maxEcartVer']
		BThorMinVarMv = min(BThorMinVarMv, featuresSubCU['varHor'] + featuresSubCU['varVer'])


		# choiceFeatures.append(featuresSubCU['varPix'])
		# choiceFeatures.append(1.0*featuresSubCU['varHor'] + 1.0*featuresSubCU['varVer'])
		# choiceFeatures.append(1.0*featuresSubCU['maxEcartHor'] + 1.0 * featuresSubCU['maxEcartVer'])

	# choiceFeatures.append(BThorVarPix)
	choiceFeatures.append(featuresCU['varPix']/max(0.1,BThorVarPix))
	choiceFeatures.append(featuresCU['varPix']/max(0.1,BThorMinVarPix))

	#***************************************************************
	#PARTIE MV blocks
	#***************************************************************
	# choiceFeatures.append(BThorVarMv)
	choiceFeatures.append((featuresCU['varHor'] + featuresCU['varVer'])/max(0.1,BThorVarMv))
	choiceFeatures.append((featuresCU['varHor'] + featuresCU['varVer'])/max(0.1,BThorMinVarMv))
	choiceFeatures.append((featuresCU['maxEcartHor']+featuresCU['maxEcartVer'])/max(0.1,BThorEcartMV))

	energyCu = featuresCU['energy']
	choiceFeatures.append(min(10.0,energyCu/max(0.1,BThorEnergy)))

	return choiceFeatures

#**************************************************************




#************************************************************
def calcFeaturesNosplitVer(predsBloc4x4,arraySplitData,pelX,pelY,widthCU,heightCU,QP,temporality):
# trainingFile.write("@ATTRIBUTE QP REAL\n")
# # trainingFile.write("@ATTRIBUTE temporality REAL\n")
# trainingFile.write("@ATTRIBUTE ratioSize REAL\n")

# trainingFile.write("@ATTRIBUTE gradx/grady REAL\n")
# trainingFile.write("@ATTRIBUTE varPix REAL\n")
# trainingFile.write("@ATTRIBUTE varMv REAL\n")
# trainingFile.write("@ATTRIBUTE maxEcartMV REAL\n")

# trainingFile.write("@ATTRIBUTE ratio_varPix REAL\n")
# trainingFile.write("@ATTRIBUTE ratio_minVarPix REAL\n")
# trainingFile.write("@ATTRIBUTE ratio_varMv REAL\n")
# trainingFile.write("@ATTRIBUTE ratio_minVarMv REAL\n")
# trainingFile.write("@ATTRIBUTE ratio_ecartMax REAL\n")

# trainingFile.write("@ATTRIBUTE ratioEnergyHor REAL\n")	

#trainingFile.write("@ATTRIBUTE split 	{0,2}\n")

	choiceFeatures = []
	featuresCU = getFeaturesCU(predsBloc4x4,arraySplitData,pelX,pelY,widthCU,heightCU)
	choiceFeatures.append(int(QP)+QP_offsets[temporality]) 
	# choiceFeatures.append(temporality) 
	choiceFeatures.append(math.log(widthCU*1.0/heightCU,2))
	

	#***************************************************************
	#PARTIE Pixels
	#***************************************************************
	if (featuresCU['gradx']*featuresCU['grady']!=0.0):
		choiceFeatures.append(featuresCU['gradx']/featuresCU['grady'])
	else:
		choiceFeatures.append(0.0)

	choiceFeatures.append(featuresCU['varPix'])
	choiceFeatures.append(featuresCU['varHor'] + featuresCU['varVer'])
	choiceFeatures.append(featuresCU['maxEcartHor']+featuresCU['maxEcartVer'])


	#VERTICAL
	BTVerVarPix = 0
	BTVerMinVarPix = 100000000000000000  
	BTverEnergy = 0

	BTVerVarMv = 0
	BTVerEcartMV = 0
	BTVerMinVarMv = 100000000000000000
	for i in range(0,2):
		pelXsubCu = pelX + i * widthCU // 2
		pelYsubCu = pelY 
		featuresSubCU = getFeaturesCU(predsBloc4x4,arraySplitData,pelXsubCu,pelYsubCu,widthCU//2,heightCU)
		BTVerVarPix = BTVerVarPix + 0.5*featuresSubCU['varPix']
		BTVerMinVarPix = min(BTVerMinVarPix, featuresSubCU['varPix'])
		BTverEnergy = BTverEnergy + featuresSubCU['energy'] / 2.0

		BTVerVarMv = BTVerVarMv + 0.5*featuresSubCU['varHor'] + 0.5*featuresSubCU['varVer']
		BTVerEcartMV = BTVerEcartMV + 0.5 *featuresSubCU['maxEcartHor'] + 0.5  * featuresSubCU['maxEcartVer']
		BTVerMinVarMv = min(BTVerMinVarMv, featuresSubCU['varHor'] + featuresSubCU['varVer'])
		# choiceFeatures.append(featuresCU['varPix'])
		# choiceFeatures.append(1.0*featuresCU['varHor'] + 1.0*featuresCU['varVer'])
		# choiceFeatures.append(1.0*featuresCU['maxEcartHor'] + 1.0 * featuresCU['maxEcartVer'])

	# choiceFeatures.append(BTVerVarPix)
	choiceFeatures.append(featuresCU['varPix']/max(0.1,BTVerVarPix))
	choiceFeatures.append(featuresCU['varPix']/max(0.1,BTVerMinVarPix))
	# choiceFeatures.append(BTVerVarMv)
	choiceFeatures.append((featuresCU['varHor'] + featuresCU['varVer'])/max(0.1,BTVerVarMv))
	choiceFeatures.append((featuresCU['varHor'] + featuresCU['varVer'])/max(0.1,BTVerMinVarMv))
	choiceFeatures.append((featuresCU['maxEcartHor']+featuresCU['maxEcartVer'])/max(0.1,BTVerEcartMV))

	#***************************************************************
	#PARTIE MV blocks
	#***************************************************************
	energyCu = featuresCU['energy']
	choiceFeatures.append(min(10.0,energyCu/max(0.1,BTverEnergy)))


	return choiceFeatures

#**************************************************************



#**************************************************************
def calcFeaturesWhichBTSplit(predsBloc4x4,arraySplitData,pelX,pelY,widthCU,heightCU):
# trainingFile.write("@ATTRIBUTE ratioSize REAL\n")
# trainingFile.write("@ATTRIBUTE gradx/grady REAL\n")
# trainingFile.write("@ATTRIBUTE ratio_varMv REAL\n")
# trainingFile.write("@ATTRIBUTE ratio_varPix REAL\n")
# trainingFile.write("@ATTRIBUTE ratio_Energy REAL\n")

	choiceFeatures = []
	featuresCU = getFeaturesCU(predsBloc4x4,arraySplitData,pelX,pelY,widthCU,heightCU)
	# choiceFeatures.append(widthCU*heightCU)
	choiceFeatures.append(math.log(widthCU*1.0/heightCU,2))
	if (featuresCU['grady']!=0.0):
		choiceFeatures.append(featuresCU['gradx']/featuresCU['grady'])
	else:
		choiceFeatures.append(featuresCU['gradx'])

	#HORIZONTAL
	BThorVarPix = 0
	BThorMinVarPix = 100000000000000000
	BThorEnergy = 0
	BThorVarMv = 0
	BThorEcartMV = 0
	BThorMinVarMv = 100000000000000000
	for i in range(0,2):
		pelXsubCu = pelX 
		pelYsubCu = pelY + i * heightCU // 2
		featuresSubCU = getFeaturesCU(predsBloc4x4,arraySplitData,pelXsubCu,pelYsubCu,widthCU,heightCU//2)
		BThorVarPix = BThorVarPix + featuresSubCU['varPix']
		BThorMinVarPix = min(BThorMinVarPix, featuresSubCU['varPix'])
		BThorEnergy = BThorEnergy + featuresSubCU['energy'] / 2.0

		BThorVarMv = BThorVarMv + 1.0*featuresSubCU['varHor'] + 1.0*featuresSubCU['varVer']
		BThorEcartMV = BThorEcartMV + 1.0*featuresSubCU['maxEcartHor'] + 1.0 * featuresSubCU['maxEcartVer']
		BThorMinVarMv = min(BThorMinVarMv, 1.0*featuresSubCU['varHor'] + 1.0*featuresSubCU['varVer'])
		# choiceFeatures.append(featuresSubCU['varPix'])
		# choiceFeatures.append(1.0*featuresSubCU['varHor'] + 1.0*featuresSubCU['varVer'])
		# choiceFeatures.append(1.0*featuresSubCU['maxEcartHor'] + 1.0 * featuresSubCU['maxEcartVer'])
	# choiceFeatures.append(BThorVarPix)
	# choiceFeatures.append(BThorVarMv)
	# choiceFeatures.append(BThorEcartMV)


	# VERTICAL
	BTverVarPix = 0
	BTverMinVarPix = 100000000000000000
	BTverEnergy = 0
	BTverVarMv = 0
	BTverEcartMV = 0
	BTverMinVarMv = 100000000000000000
	for i in range(0,2):
		pelXsubCu = pelX + i * widthCU // 2
		pelYsubCu = pelY 
		featuresSubCU = getFeaturesCU(predsBloc4x4,arraySplitData,pelXsubCu,pelYsubCu,widthCU//2,heightCU)
		BTverVarPix = BTverVarPix + featuresSubCU['varPix']
		BTverMinVarPix = min(BTverMinVarPix, featuresSubCU['varPix'])
		BTverEnergy = BTverEnergy + featuresSubCU['energy'] / 2.0

		BTverVarMv = BTverVarMv + 1.0*featuresSubCU['varHor'] + 1.0*featuresSubCU['varVer']
		BTverEcartMV = BTverEcartMV + 1.0*featuresSubCU['maxEcartHor'] + 1.0 * featuresSubCU['maxEcartVer']
		BTverMinVarMv = min(BTverMinVarMv, 1.0*featuresSubCU['varHor'] + 1.0*featuresSubCU['varVer'])
		# choiceFeatures.append(featuresSubCU['varPix'])
		# choiceFeatures.append(1.0*featuresSubCU['varHor'] + 1.0*featuresSubCU['varVer'])
		# choiceFeatures.append(1.0*featuresSubCU['maxEcartHor'] + 1.0 * featuresSubCU['maxEcartVer'])

	# choiceFeatures.append(BTverVarPix)
	# choiceFeatures.append(BTverVarMv)
	# choiceFeatures.append(BTverEcartMV)
	choiceFeatures.append(BThorVarPix/max(0.1,BTverVarPix))
	choiceFeatures.append(BThorMinVarPix/max(0.1,BTverMinVarPix))
	choiceFeatures.append(BThorVarMv/max(0.1,BTverVarMv))
	choiceFeatures.append(BThorEcartMV/max(0.1,BTverEcartMV))
	choiceFeatures.append(BThorMinVarMv/max(0.1,BTverMinVarMv))

	choiceFeatures.append(min(10.0,BThorEnergy/max(0.1,BTverEnergy)))
	return choiceFeatures

#**************************************************************



#**************************************************************
def calcFeaturesQTHor(predsBloc4x4 ,pelX,pelY,widthCU,heightCU):
# trainingFile.write("@ATTRIBUTE aireCU REAL\n")
# trainingFile.write("@ATTRIBUTE gradx/grady REAL\n")

# trainingFile.write("@ATTRIBUTE ratio_varPix REAL\n")
# trainingFile.write("@ATTRIBUTE ratio_varMv REAL\n")
# trainingFile.write("@ATTRIBUTE ratio_Energy REAL\n")

# trainingFile.write("@ATTRIBUTE split 	{1,2}\n")

	choiceFeatures = []
	featuresCU = getFeaturesCU(predsBloc4x4,arraySplitData,pelX,pelY,widthCU,heightCU)
	choiceFeatures.append(widthCU*heightCU)
	if (featuresCU['grady']!=0.0):
		choiceFeatures.append(featuresCU['gradx']/featuresCU['grady'])
	else:
		choiceFeatures.append(featuresCU['gradx'])

	#HORIZONTAL
	BThorVarMv = 0
	BThorVarPix = 0
	for i in range(0,2):
		pelXsubCu = pelX 
		pelYsubCu = pelY + i * heightCU // 2
		featuresCU = getFeaturesCU(predsBloc4x4,arraySplitData,pelXsubCu,pelYsubCu,widthCU,heightCU//2)
		BThorVarMv = BThorVarMv + 1.0*featuresCU['varHor'] + 1.0*featuresCU['varVer']
		BThorVarPix = BThorVarPix + featuresCU['varPix']

	CostHor = calcEnergyHor(predsBloc4x4 ,pelX,pelY,widthCU,heightCU)

	# QT split
	qtVarMv = 0
	qtVarPix = 0
	CostQt = 0
	for i in range(0,2):
		for j in range(0,2): 
			pelXsubCu = pelX + i * widthCU // 2
			pelYsubCu = pelY + j * heightCU // 2

			featuresCU = getFeaturesCU(predsBloc4x4,arraySplitData,pelXsubCu,pelYsubCu,widthCU//2,heightCU//2)
			qtVarMv = qtVarMv + 0.5*featuresCU['varHor'] + 0.5*featuresCU['varVer']
			qtVarPix = qtVarPix + 0.5*featuresCU['varPix'] 
	
	CostQt = calcEnergyQT(predsBloc4x4 ,pelX,pelY,widthCU,heightCU)
	

	choiceFeatures.append(qtVarMv/max(0.1,BThorVarMv))
	choiceFeatures.append(qtVarPix/max(0.1,BThorVarPix))
	choiceFeatures.append(min(10.0,CostQt/max(0.1,CostHor)))
	return choiceFeatures

#**************************************************************


#**************************************************************
def calcFeaturesQTVer(predsBloc4x4 ,pelX,pelY,widthCU,heightCU):
# trainingFile.write("@ATTRIBUTE aireCU REAL\n")
# trainingFile.write("@ATTRIBUTE gradx/grady REAL\n")

# trainingFile.write("@ATTRIBUTE ratio_varPix REAL\n")
# trainingFile.write("@ATTRIBUTE ratio_varMv REAL\n")
# trainingFile.write("@ATTRIBUTE ratio_Energy REAL\n")

# trainingFile.write("@ATTRIBUTE split 	{1,3}\n")

	choiceFeatures = []
	featuresCU = getFeaturesCU(predsBloc4x4,arraySplitData,pelX,pelY,widthCU,heightCU)
	choiceFeatures.append(widthCU*heightCU)
	if (featuresCU['grady']!=0.0):
		choiceFeatures.append(featuresCU['gradx']/featuresCU['grady'])
	else:
		choiceFeatures.append(featuresCU['gradx'])

	# VERTICAL
	BTverVarMv = 0
	BTverVarPix = 0
	for i in range(0,2):
		pelXsubCu = pelX + i * widthCU // 2
		pelYsubCu = pelY 
		featuresCU = getFeaturesCU(predsBloc4x4,arraySplitData,pelXsubCu,pelYsubCu,widthCU//2,heightCU)
		BTverVarMv = BTverVarMv + 1.0*featuresCU['varHor'] + 1.0*featuresCU['varVer']
		BTverVarPix = BTverVarPix + featuresCU['varPix']
	
	CostVer = calcEnergyVer(predsBloc4x4 ,pelX,pelY,widthCU,heightCU)



	# QT split
	qtVarMv = 0
	qtVarPix = 0
	CostQt = 0
	for i in range(0,2):
		for j in range(0,2): 
			pelXsubCu = pelX + i * widthCU // 2
			pelYsubCu = pelY + j * heightCU // 2

			featuresCU = getFeaturesCU(predsBloc4x4,arraySplitData,pelXsubCu,pelYsubCu,widthCU//2,heightCU//2)
			qtVarMv = qtVarMv + 0.5*featuresCU['varHor'] + 0.5*featuresCU['varVer']
			qtVarPix = qtVarPix + 0.5*featuresCU['varPix'] 
	
	CostQt = calcEnergyQT(predsBloc4x4 ,pelX,pelY,widthCU,heightCU)
	

	choiceFeatures.append(qtVarMv/max(0.1,BTverVarMv))
	choiceFeatures.append(qtVarPix/max(0.1,BTverVarPix))
	choiceFeatures.append(min(10.0,CostQt/max(0.1,CostVer)))
	return choiceFeatures

#**************************************************************



#**************************************************************
def calcFeaturesQTVerHor(predsBloc4x4,arraySplitData,pelX,pelY,widthCU,heightCU):
# trainingFile.write("@ATTRIBUTE aireCU REAL\n")
# trainingFile.write("@ATTRIBUTE ratioSize REAL\n")
# trainingFile.write("@ATTRIBUTE gradx/grady REAL\n")

# trainingFile.write("@ATTRIBUTE ratio_varPix_VerHor REAL\n")
# trainingFile.write("@ATTRIBUTE ratio_varMv_VerHor REAL\n")
# trainingFile.write("@ATTRIBUTE ratio_Energy_VerHor REAL\n")

# trainingFile.write("@ATTRIBUTE ratio_varPix_QTVer REAL\n")
# trainingFile.write("@ATTRIBUTE ratio_varMv_QThor REAL\n")
# trainingFile.write("@ATTRIBUTE ratio_Energy_QThor REAL\n")

# trainingFile.write("@ATTRIBUTE split 	{1,2,3}\n")

	choiceFeatures = []
	featuresCU = getFeaturesCU(predsBloc4x4,arraySplitData,pelX,pelY,widthCU,heightCU)
	choiceFeatures.append(widthCU*heightCU)
	choiceFeatures.append(math.log(widthCU*1.0/heightCU,2))
	if (featuresCU['grady']!=0.0):
		choiceFeatures.append(featuresCU['gradx']/featuresCU['grady'])
	else:
		choiceFeatures.append(featuresCU['gradx'])

	#HORIZONTAL
	BThorVarMv = 0
	BThorVarPix = 0
	for i in range(0,2):
		pelXsubCu = pelX 
		pelYsubCu = pelY + i * heightCU // 2
		featuresCU = getFeaturesCU(predsBloc4x4,arraySplitData,pelXsubCu,pelYsubCu,widthCU,heightCU//2)
		BThorVarMv = BThorVarMv + 1.0*featuresCU['varHor'] + 1.0*featuresCU['varVer']
		BThorVarPix = BThorVarPix + featuresCU['varPix']

	# VERTICAL
	BTverVarMv = 0
	BTverVarPix = 0
	for i in range(0,2):
		pelXsubCu = pelX + i * widthCU // 2
		pelYsubCu = pelY 
		featuresCU = getFeaturesCU(predsBloc4x4,arraySplitData,pelXsubCu,pelYsubCu,widthCU//2,heightCU)
		BTverVarMv = BTverVarMv + 1.0*featuresCU['varHor'] + 1.0*featuresCU['varVer']
		BTverVarPix = BTverVarPix + featuresCU['varPix']

	choiceFeatures.append(BThorVarMv/max(0.1,BTverVarMv))
	choiceFeatures.append(BThorVarPix/max(0.1,BTverVarPix))

	CostHor = calcEnergyHor(predsBloc4x4 ,pelX,pelY,widthCU,heightCU)
	CostVer = calcEnergyVer(predsBloc4x4 ,pelX,pelY,widthCU,heightCU)
	choiceFeatures.append(min(10.0,CostHor/max(0.1,CostVer)))

	# QT split
	qtVarMv = 0
	qtVarPix = 0
	CostQt = 0
	if widthCU == heightCU:
		for i in range(0,2):
			for j in range(0,2): 
				pelXsubCu = pelX + i * widthCU // 2
				pelYsubCu = pelY + j * heightCU // 2

				featuresCU = getFeaturesCU(predsBloc4x4,arraySplitData,pelXsubCu,pelYsubCu,widthCU//2,heightCU//2)
				qtVarMv = qtVarMv + 0.5*featuresCU['varHor'] + 0.5*featuresCU['varVer']
				qtVarPix = qtVarPix + 0.5*featuresCU['varPix'] 

		CostQt = calcEnergyQT(predsBloc4x4 ,pelX,pelY,widthCU,heightCU)
	

	choiceFeatures.append(qtVarMv/max(0.1,BTverVarMv))
	choiceFeatures.append(qtVarPix/max(0.1,BTverVarPix))
	choiceFeatures.append(min(10.0,CostQt/max(0.1,CostVer)))
	return choiceFeatures

#**************************************************************


#**************************************************************
def calcWeight(costNS,costQt,costHor,costVer,choixAttributs):

	#CALCUL DU POIDS DE L'ERREUR
	weightError = 0.0
	error = 0.0
	truecost  = 0.0

	max_double = 1000000000000.0
	if costNS == 0.0  : costNS = max_double
	if costQt == 0.0  : costQt = max_double
	if costHor == 0.0 : costHor = max_double
	if costVer == 0.0 : costVer = max_double

	if choixAttributs == "testNosplitHor":
		maxCostSplit = max(costNS,costHor)
		if maxCostSplit < max_double - 1 and costNS != costHor:
			# print costNS,costHor
			# weightError = 1.0 + math.log(100.0*abs(costNS-costHor)/max(costNS,costHor),10)
			weightError = math.sqrt(100.0*abs(costNS-costHor)/min(costNS,costHor))
			error = abs(costNS-costHor)
			truecost  = min(costNS,costHor)

	elif choixAttributs == "test0BT":
		costBT=min(costHor,costVer)
		maxCostSplit = max(costNS,costBT)
		if maxCostSplit < max_double - 1 and costNS != costBT:
			weightError = math.sqrt(100.0*abs(costNS-costBT)/min(costNS,costBT))
			error = abs(costNS-costBT)
			truecost  = min(costNS,costBT)

	elif choixAttributs == "test1BT":
		costBT=min(costHor,costVer)
		maxCostSplit = max(costQt,costBT)
		if maxCostSplit < max_double - 1 and costQt != costBT:
			weightError = math.sqrt(100.0*abs(costQt-costBT)/min(costQt,costBT))
			error = abs(costQt-costBT)
			truecost  = min(costQt,costBT)

	elif choixAttributs == "test1All":
		costNotQt=min(costNS,costHor,costVer)
		if costQt ==  max_double :
			costQt = min(costHor,costVer)

		if costQt < max_double - 1 and costNotQt < max_double - 1 and costQt != costNotQt:
			weightError = math.sqrt(100.0*abs(costQt-costNotQt)/min(costQt,costNotQt))
			error = abs(costQt-costNotQt)
			truecost  = min(costQt,costNotQt)

	elif choixAttributs == "test0All":
		costSplit=min(costQt,costHor,costVer)
		if max(costNS,costSplit) < max_double - 1 :
			# weightError = math.sqrt(100.0*abs(costNS-costSplit)/min(costNS,costSplit))
			weightError = 100.0*abs(costNS-costSplit)/min(costNS,costSplit)
			error = abs(costNS-costSplit)
			truecost  = min(costNS,costSplit)

	return weightError, error, truecost
# ************************************************************



# ************************************************************
def createTrainingFile(QP):
	#LOOP ON ALL FRAMES
	numCategoryQP = 1
	numCategoryCu = 7
	if oneClassbyQP == True: 
		numCategoryQP = 4

	stepQP = (44-22)/numCategoryQP
	trainingFile = [""]*numCategoryCu
	for i in range(0,numCategoryCu):
		trainingFile[i] = [""]*numCategoryQP
		
	numTreatedBySplit = [0]*numCategoryCu
	for i in range(0,numCategoryCu):
		numTreatedBySplit[i] = [0]*4
		for j in range(0,numCategoryQP):
			categoryQP = 22 + j * stepQP
			# size = 128 / (2**i)
			size = i
			

			if oneClassbyQP == True:
				nameFile = nameTrainFile + "_s" + str(size) + "_qp" + str(categoryQP) + ".arff"
			else:
				nameFile = nameTrainFile + "_s" + str(size) + ".arff"

			trainingFile[i][j] = open(pathFolder + "/" + nameFile,"w") 
			if outFormat == "arff":
				fillAttributes(trainingFile[i][j],choixAttributs)


	for count in range(firstFrame,lastFrame+1):

		#avoid intra frames
		if count%16 == 0:
			continue

		#Calc QP of specific frame (for random access)
		temporality = getTemporality(count)
		qpFrame = int(QP)+QP_offsets[temporality]
		categoryQP = min(numCategoryQP-1,(qpFrame-22)/stepQP)
		# print count, qpFrame, categoryQP

		# open input/output files
		s = str(count)
		namePredFile = pathFolder + '/block_info_' + s
		predsBloc4x4 = getPredInfos(namePredFile,width,height,sizeBlock)

		# splitFile = pathFolder + '/choice_split_frame_' + s
		splitFile = pathFolder + '/cu_info_' + s
		inputfile = open(splitFile)
		for i in range(1): inputfile.next() # skip first line

		splitFile = pathFolder + '/choice_split_frame_' + s
		arraySplitData = getSplitInfos(splitFile,width,height,sizeBlock)

		listCuData = []
		for line in inputfile:
			donneesCU = map(float,line.split())
			donneesCU = map(int,donneesCU)
			listCuData.append(donneesCU)
			pelX = donneesCU[0]
			pelY = donneesCU[1]
			widthCU = donneesCU[2]
			heightCU = donneesCU[3]


			if (widthCU  > sizeBlock and heightCU > sizeBlock and widthCU*heightCU >= 16*16 and pelX+widthCU<width and pelY+heightCU<height):
				# print("\n")
				# categoryCU =  int(math.log((128*128)/(widthCU*heightCU),2))/2
				categoryCU =  int(math.log((128*128)/(widthCU*heightCU),2))
				# print widthCU,heightCU
				# print categoryCU

				splitType = donneesCU[8]
				# splitType = donneesCU[4]

		
				#CALCUL DU POIDS DE L'ERREUR
				costNS = donneesCU[4]
				costQt = donneesCU[5]
				costHor = donneesCU[6]
				costVer = donneesCU[7]
				weightError, error, truecost = calcWeight(costNS,costQt,costHor,costVer,choixAttributs)
				if weightError < 0.001 :
					continue

				#*************************************************************************
				#PREMIERS CLASSIFIEURS
				if choixAttributs == "testNosplitQt":
					# print "\n",pelX,pelY,widthCU,heightCU
					# if widthCU==heightCU :
					# if widthCU==heightCU and max(costHor,costVer)>0.001:
					# if widthCU==heightCU and max(costHor,costVer)>0.001 and splitType != 0:
					# if max(costHor,costVer)>0.001 and splitType != 1:

					if widthCU==heightCU and (splitType ==0 or splitType == 1):
						if splitType == 0 and numTreatedBySplit[categoryCU][0] > numTreatedBySplit[categoryCU][1] + 500:
							continue
						elif splitType != 0 and numTreatedBySplit[categoryCU][1] > numTreatedBySplit[categoryCU][0]  + 500:
							continue
						numTreatedBySplit[categoryCU][splitType] =  numTreatedBySplit[categoryCU][splitType] + 1

						features = calcFeaturesNosplitQT(predsBloc4x4,arraySplitData ,pelX,pelY,widthCU,heightCU,QP,temporality)
						for i in range(0,len(features)):
							trainingFile[categoryCU][categoryQP].write(str(features[i])+ ",")
						
						if useWeights:
							trainingFile[categoryCU][categoryQP].write(str(splitType) + ",{" + str(weightError) + "}\n")
						else:
							trainingFile[categoryCU][categoryQP].write(str(splitType) + "\n")


				elif choixAttributs == "testNosplitHor":
					# print "\n",pelX,pelY,widthCU,heightCU
					if splitType ==0 or splitType == 2:

						if splitType == 0 and numTreatedBySplit[categoryCU][0] > numTreatedBySplit[categoryCU][2]+ 500:
							continue
						elif splitType != 0 and numTreatedBySplit[categoryCU][2] > numTreatedBySplit[categoryCU][0] + 500:
							continue
						numTreatedBySplit[categoryCU][splitType] =  numTreatedBySplit[categoryCU][splitType] + 1

						features = calcFeaturesNosplitHor(predsBloc4x4,arraySplitData,pelX,pelY,widthCU,heightCU,QP,temporality)
						for i in range(0,len(features)):
							trainingFile[categoryCU][categoryQP].write(str(features[i])+ ",")
						
						if useWeights:
							trainingFile[categoryCU][categoryQP].write(str(splitType) + ",{" + str(weightError) + "}\n")
						else:
							trainingFile[categoryCU][categoryQP].write(str(splitType) + "\n")
					

				elif choixAttributs == "testNosplitVer":
					# print "\n",pelX,pelY,widthCU,heightCU
					if splitType ==0 or splitType == 3:

						if splitType == 0 and numTreatedBySplit[categoryCU][0] > numTreatedBySplit[categoryCU][3]+ 500:
							continue
						elif splitType != 0 and numTreatedBySplit[categoryCU][3] > numTreatedBySplit[categoryCU][0] + 500:
							continue
						numTreatedBySplit[categoryCU][splitType] =  numTreatedBySplit[categoryCU][splitType] + 1

						features = calcFeaturesNosplitVer(predsBloc4x4,arraySplitData,pelX,pelY,widthCU,heightCU,QP,temporality)
						for i in range(0,len(features)):
							trainingFile[categoryCU][categoryQP].write(str(features[i])+ ",")
						trainingFile[categoryCU][categoryQP].write(str(splitType) + "\n")
						# trainingFile[categoryCU][categoryQP].write(str(splitType) + ",{" + str(weightError) + "}\n")
				


				#*************************************************************************
				#NOUVEAU CLASSIFIEURS
				elif choixAttributs == "test1All":

					if widthCU==heightCU :
						if (splitType != 1): 
							splitType = 0

						if splitType == 0 and numTreatedBySplit[categoryCU][0] > numTreatedBySplit[categoryCU][1] + 500:
							continue
						elif splitType != 0 and numTreatedBySplit[categoryCU][1] > numTreatedBySplit[categoryCU][0]  + 500:
							continue
						numTreatedBySplit[categoryCU][splitType] =  numTreatedBySplit[categoryCU][splitType] + 1

						features = calcFeaturesQTvsAll(predsBloc4x4,arraySplitData,pelX,pelY,widthCU,heightCU,QP,temporality)
						for i in range(0,len(features)):
							trainingFile[categoryCU][categoryQP].write(str(features[i])+ ",")

						if useWeights:
							trainingFile[categoryCU][categoryQP].write(str(splitType) + ",{" + str(weightError) + "}\n")
						else:
							trainingFile[categoryCU][categoryQP].write(str(splitType) + "\n")


				elif choixAttributs == "test0All" or choixAttributs == "test0BT":
					# if widthCU==heightCU and (splitType == 0 or costQt > 0.001):
					# if choixAttributs == "test0All" and not(widthCU==heightCU and (splitType == 0 or costQt > 0.001)):
					# 	continue

					if (splitType != 0): 
						splitType = 1

					if splitType == 0 and numTreatedBySplit[categoryCU][0] > numTreatedBySplit[categoryCU][1] + 500:
						continue
					elif splitType != 0 and numTreatedBySplit[categoryCU][1] > numTreatedBySplit[categoryCU][0]  + 500:
						continue
					numTreatedBySplit[categoryCU][splitType] =  numTreatedBySplit[categoryCU][splitType] + 1

					features = calcFeaturesQTvsAll(predsBloc4x4,arraySplitData,pelX,pelY,widthCU,heightCU,QP,temporality)
					for i in range(0,len(features)):
						trainingFile[categoryCU][categoryQP].write(str(features[i])+ ",")

					if useWeights:
						trainingFile[categoryCU][categoryQP].write(str(splitType) + ",{" + str(weightError) + "}\n")
					else:
						trainingFile[categoryCU][categoryQP].write(str(splitType) + "\n")


				elif choixAttributs == "test1BT":

					if widthCU==heightCU and splitType > 0:
						if (splitType > 1) : 
							splitType = 0

						if splitType == 0 and numTreatedBySplit[categoryCU][0] > numTreatedBySplit[categoryCU][1] + 20:
							continue
						elif splitType != 0 and numTreatedBySplit[categoryCU][1] > numTreatedBySplit[categoryCU][0]  + 20:
							continue
						numTreatedBySplit[categoryCU][splitType] =  numTreatedBySplit[categoryCU][splitType] + 1

						features = calcFeaturesNosplitQT(predsBloc4x4,arraySplitData,pelX,pelY,widthCU,heightCU,QP,temporality)
						for i in range(0,len(features)):
							trainingFile[categoryCU][categoryQP].write(str(features[i])+ ",")

						if useWeights:
							trainingFile[categoryCU][categoryQP].write(str(splitType) + ",{" + str(weightError) + "}\n")
						else:
							trainingFile[categoryCU][categoryQP].write(str(splitType) + "\n")


				elif choixAttributs == "testWhichBT":

					if splitType == 2 or splitType == 3:

						if splitType == 2 and numTreatedBySplit[categoryCU][2] > numTreatedBySplit[categoryCU][3] + 500:
							continue
						elif splitType == 3 and numTreatedBySplit[categoryCU][3] > numTreatedBySplit[categoryCU][2] + 500:
							continue
						numTreatedBySplit[categoryCU][splitType] =  numTreatedBySplit[categoryCU][splitType] + 1

						features = calcFeaturesWhichBTSplit(predsBloc4x4,arraySplitData,pelX,pelY,widthCU,heightCU)

						for i in range(0,len(features)):
							trainingFile[categoryCU][categoryQP].write(str(features[i])+ ",")
						trainingFile[categoryCU][categoryQP].write(str(splitType) + "\n")



				#*************************************************************************
				#INUTILES CLASSIFIEURS
				elif choixAttributs == "testQtHor":

					if (splitType == 1 or splitType == 2) and widthCU==heightCU:

							if splitType == 1 and numTreatedBySplit[categoryCU][splitType][categoryCU] > numTreatedBySplit[categoryCU][2][categoryCU] + 500:
								continue
							elif splitType == 2 and numTreatedBySplit[categoryCU][splitType][categoryCU] > numTreatedBySplit[categoryCU][1][categoryCU] + 500:
								continue
							numTreatedBySplit[categoryCU][splitType][categoryCU] =  numTreatedBySplit[categoryCU][splitType][categoryCU] + 1
							
							features = calcFeaturesQTHor(predsBloc4x4 ,pelX,pelY,widthCU,heightCU)
							for i in range(0,len(features)):
								trainingFile[i].write(str(features[i])+ ",")
							trainingFile[i].write(str(splitType) + "\n")


				elif choixAttributs == "testQtVer":

					if (splitType == 1 or splitType == 3) and widthCU==heightCU:
						if splitType == 1 and numTreatedBySplit[categoryCU][splitType][categoryCU] > numTreatedBySplit[categoryCU][3][categoryCU] + 500:
							continue
						elif splitType == 3 and numTreatedBySplit[categoryCU][splitType][categoryCU] > numTreatedBySplit[categoryCU][1][categoryCU] + 500:
							continue
						numTreatedBySplit[categoryCU][splitType][categoryCU] =  numTreatedBySplit[categoryCU][splitType][categoryCU] + 1
						
						features = calcFeaturesQTVer(predsBloc4x4 ,pelX,pelY,widthCU,heightCU)
						for i in range(0,len(features)):
							trainingFile.write(str(features[i])+ ",")
						trainingFile.write(str(splitType) + "\n")



				elif choixAttributs == "testQtHorVer":

					if splitType != 0:

						if splitType == 1 and numTreatedBySplit[categoryCU][splitType] > min(numTreatedBySplit[categoryCU][2], numTreatedBySplit[categoryCU][3]) + 100:
							numTreatedBySplit[categoryCU][splitType]=numTreatedBySplit[categoryCU][splitType]-1
							continue
						elif splitType == 2 and numTreatedBySplit[categoryCU][splitType] > min(numTreatedBySplit[categoryCU][1], numTreatedBySplit[categoryCU][3]) + 100:
							numTreatedBySplit[categoryCU][splitType]=numTreatedBySplit[categoryCU][splitType]-1
							continue
						elif splitType == 3 and numTreatedBySplit[categoryCU][splitType] > min(numTreatedBySplit[categoryCU][1], numTreatedBySplit[categoryCU][2]) + 100:
							numTreatedBySplit[categoryCU][splitType]=numTreatedBySplit[categoryCU][splitType]-1
							continue
						
						features = calcFeaturesQTVerHor(predsBloc4x4 ,pelX,pelY,widthCU,heightCU)

						for i in range(0,len(features)):
							trainingFile.write(str(features[i])+ ",")
						trainingFile.write(str(splitType) + "\n")
				#*************************************************************************


	 	print ("Image done", count)
	
	for i in range (0,numCategoryCu):
		for j in range (0,numCategoryQP):
			trainingFile[i][j].close()				
# *************************************************************



#Block size for MV
sizeBlock = 4

if __name__ == '__main__':


	if len(sys.argv) != 7 :
	        print ("***** Usage syntax Error!!!! *****\n")
	        print ("Usage:")
	        print ("python <script> <folder data> <outFormat> <firstFrame> <lastFrame> <classifier> <one classif by QP?>")
	        sys.exit(1) # exit
	else:
	        pass

	pathFolder = str(sys.argv[1])
	outFormat = str(sys.argv[2])
	firstFrame = int(sys.argv[3])
	lastFrame = int(sys.argv[4])
	classifier = str(sys.argv[5])
	oneClassbyQP = str(sys.argv[6])
	

	if(outFormat != "arff" and outFormat != "data"):
		print "WRONG OUTPUT FORMAT FOR TRAINING DATA"
		sys.exit(1) # exit


	if(str(sys.argv[6]) == "yes"):
		oneClassbyQP = True
	elif (str(sys.argv[6]) == "no"):
		oneClassbyQP = False
	else:
		print "WRONG PARAMETER"
		print "<one classif by QP?> : must be yes or no"
		sys.exit(1) # exit
	# useWeights = False
	useWeights = True

		
	#LOAD ORIGINAL QP AND QP OFFSETS
	QP = getQP(pathFolder)
	width, height = getDimensionVideo(pathFolder)
	print "width, height", width,height
	print "QP",QP

	choixAttributs = ""

	#******************************
	if(classifier == "01"):
		nameTrainFile = "training_01"
		choixAttributs = "testNosplitQt"
		createTrainingFile(QP)

	elif(classifier == "02"):
		nameTrainFile = "training_02"
		choixAttributs = "testNosplitHor"
		createTrainingFile(QP)

	elif(classifier == "03"):
		nameTrainFile = "training_03"
		choixAttributs = "testNosplitVer"
		createTrainingFile(QP)



	#******************************
	elif(classifier == "1All"):
		nameTrainFile = "training_1All"
		choixAttributs = "test1All"
		createTrainingFile(QP)
	
	elif(classifier == "0All"):
		nameTrainFile = "training_0All"
		choixAttributs = "test0All"
		createTrainingFile(QP)

	elif(classifier == "0BT"):
		nameTrainFile = "training_0BT"
		choixAttributs = "test0BT"
		createTrainingFile(QP)

	elif(classifier == "1BT"):
		nameTrainFile = "training_1BT"
		choixAttributs = "test1BT"
		createTrainingFile(QP)



	#******************************
	#USELESS
	elif(classifier == "12"):
		nameTrainFile = "training_12"
		choixAttributs = "testQtHor"
		createTrainingFile(QP)

	elif(classifier == "13"):
		nameTrainFile = "training_13"
		choixAttributs = "testQtVer"
		createTrainingFile(QP)

	elif(classifier == "23"):
		nameTrainFile = "training_23"
		choixAttributs = "testWhichBT"
		createTrainingFile(QP)
	
	elif(classifier == "123"):
		choixAttributs = "testQtHorVer"
		createTrainingFile(QP)
	
	# elif(classifier == "all"):
	# 	choixAttributs = "testNosplitQt"
	# 	createTrainingFile(QP)

	# 	choixAttributs = "testQtHor"
	# 	createTrainingFile(QP)

	# 	choixAttributs = "testQtVer"
	# 	createTrainingFile(QP)

	# 	choixAttributs = "testWhichBT"
	# 	createTrainingFile(QP)
	else:
		print "\n\nWRONG CHOICE FOR CLASSIFIER (values : 01 12 13 23 all)"
		sys.exit(1)




