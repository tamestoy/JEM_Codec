import os
import sys
from bjontegaard_metric import *


# ************************************************************
def indexInsert(qpList,qp):
	
	i=0
	length = len(qpList)
	if(length==0):
		return i

	while(qp>qpList[i]):
		# print ("index ",i, len(energyList), energie, energyList[i])
		i = i+1
		if(i >= length):
			return i
	return i
# ************************************************************
	

# ************************************************************
def getEncodingData(directory):
	
	rate = []
	distortion = []
	time = []
	qpList = []

	for filename in os.listdir(directory):
		if filename.startswith("sortie_") : 
			filePath = os.path.join(directory, filename)
			file = open(filePath)
			# print(filePath)

			for line in file:
				donnees = line.split()
				
				if(len(donnees)!=0):
					if(donnees[0]=="QP" and donnees[1]==":"):
						index = indexInsert(qpList,int(donnees[2]))
						qpList.insert(index,int(donnees[2]))
						# print(donnees)

					if(donnees[0]=="Total" and donnees[1]=="Time:"):
						time.insert(index,float(donnees[2]))
						# print(donnees)

					if(donnees[0]=="SUMMARY"):
						file.next() 
						line = file.next() 
						donnees = line.split()
						# print donnees
						rate.insert(index,float(donnees[2]))
						distortion.insert(index,float(donnees[6]))

	return [rate,distortion,time]
# ************************************************************


if len(sys.argv) != 3:
	print ("***** Usage syntax Error!!!! *****\n")
	print ("Usage:")
	print ("python <script> <directory orig> <directory chalenger>")
	sys.exit(1) # exit
else:
	pass

dirOrig = str(sys.argv[1])
dirChalenger = str(sys.argv[2])

print 'ORIG'
RdDataOrig = getEncodingData(dirOrig)
Rorig = np.array(RdDataOrig[0])
PSNRorig = np.array(RdDataOrig[1])
timeOrig = np.mean(RdDataOrig[2])
print Rorig
print PSNRorig
print RdDataOrig[2]
print timeOrig,"s"

print '\nCHALENGER'
RdDataChalenger = getEncodingData(dirChalenger)
RChalenger = np.array(RdDataChalenger[0])
PSNRChalenger = np.array(RdDataChalenger[1])
timeChalenger = np.mean(RdDataChalenger[2])
print RChalenger
print PSNRChalenger
print RdDataChalenger[2]
print timeChalenger,"s"

# # ORIG
# rate = [142185.84 , 66576.96 , 27287.52 , 13188.84]
# psnr = [41.6869, 39.5116, 37.6614, 35.8143]
# Rorig = np.array(rate)
# PSNRorig = np.array(psnr)


# # CHALENGER
# rate = [142484.52,  66968.16 , 27593.76 , 13361.64]
# psnr = [41.6869, 39.5116, 37.6614, 35.8143]
# # psnr =[41.5285, 39.2547, 37.2548, 35.3092]
# RChalenger = np.array(rate)
# PSNRChalenger = np.array(psnr)

print '\nBD-PSNR: ', BD_PSNR(Rorig, PSNRorig, RChalenger, PSNRChalenger)
print 'BD-RATE: ', BD_RATE(Rorig, PSNRorig, RChalenger, PSNRChalenger)
print 'Delta Time: ', 100*(timeChalenger-timeOrig)/timeOrig, "%"