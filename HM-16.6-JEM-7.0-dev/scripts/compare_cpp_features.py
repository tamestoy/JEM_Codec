from create_training_file import *



# ************************************************************
def compareFeatures():
	#LOOP ON ALL FRAMES
	totDiff = 0
	totCu = 0
	for count in range(firstFrame,lastFrame+1):

		#avoid intra frames
		if count%16 == 0:
			continue

		# open input/output files
		s = str(count)
		namePredFile = pathFolder + '/block_info_' + s
		predsBloc4x4 = getPredInfos(namePredFile,width,height,sizeBlock)

		nameCppFeatures = pathFolder + '/features_cu_' + s
		fileCppFeatures = open(nameCppFeatures)

		for line in fileCppFeatures:
			featuresCU = map(float,line.split())

			pelX = int(featuresCU[0])
			pelY = int(featuresCU[1])
			widthCU = int(featuresCU[2])
			heightCU = int(featuresCU[3])
			
			#meanPixCu,meanGx,meanGy,meanHor,meanVer,varPixCu,energy,varHor,varVer,maxEcartHor,maxEcartVer
			# listDico = ['meanPix', 'meanGradx', 'meanGrady', 'meanHor', 'meanVer', 'varPix',  
			# 						'energy','varHor','varVer', 'maxEcartHor' , 'maxEcartVer' ]

			# cppFeaturesCu = {'meanPix':777, 'varPix':777, 'meanHor':777, 'meanVer':777, 'meanGradx':777, 'meanGrady':777,
			# 									'energy':777,'varHor':777,'varVer':777, 'maxEcartHor' : 777, 'maxEcartVer' : 777}

			# i=4
			# for word in listDico:
			# 	cppFeaturesCu[word] = featuresCU[i]
			# 	# cppFeaturesCu["meanGx"] = featuresCU[5]
			# 	# cppFeaturesCu["meanGy"] = featuresCU[6]
			# 	# cppFeaturesCu["meanHor"] = featuresCU[7]
			# 	# cppFeaturesCu["meanVer"] = featuresCU[8]
			# 	# cppFeaturesCu["varPixCu"] = featuresCU[9]
			# 	# cppFeaturesCu["energy"] = featuresCU[9]
			# 	# cppFeaturesCu["varHor"] = featuresCU[10]
			# 	# cppFeaturesCu["varVer"] = featuresCU[11]
			# 	# cppFeaturesCu["maxEcartHor"] = featuresCU[12]
			# 	# cppFeaturesCu["maxEcartVer"] = featuresCU[13]
			# 	i = i + 1


			# pythonFeaturesCu = getFeaturesCU(predsBloc4x4,pelX,pelY,widthCU,heightCU)
			# same = 1
			# for word in listDico:
			# 	if abs(cppFeaturesCu[word] - pythonFeaturesCu[word]) / max(0.1, 1.0*abs(pythonFeaturesCu[word])) > 1.0 :
			# 		if same == 1:
			# 			print "\n"
			# 		print word
			# 		same = 0



			temporality = getTemporality(count)
			# pythonFeaturesCu = calcFeaturesNosplitHor(predsBloc4x4 ,pelX,pelY,widthCU,heightCU,temporality)
			pythonFeaturesCu = calcFeaturesNosplitVer(predsBloc4x4 ,pelX,pelY,widthCU,heightCU,temporality)
			# pythonFeaturesCu = calcFeaturesNosplitQT(predsBloc4x4 ,pelX,pelY,widthCU,heightCU,temporality)
			i=4
			cppFeaturesCu = []
			for i in range(4,len(featuresCU)):
				cppFeaturesCu.append(featuresCU[i])
				i = i + 1

			same = 1
			for i in range(1,len(cppFeaturesCu)):
				if abs(cppFeaturesCu[i] - pythonFeaturesCu[i]) / max(0.1, 1.0*abs(pythonFeaturesCu[i])) > 0.01 :
					if same == 1:
						print "\n"
					print i
					same = 0

			if same == 0 :
				print "cpp", cppFeaturesCu 
				print "py ", pythonFeaturesCu
				totDiff = totDiff + 1 

			totCu = totCu + 1

	print ("Pourcentage d'erreur " , 100.0 * totDiff/totCu)


if __name__ == '__main__':


	if len(sys.argv) != 4 :
	        print ("***** Usage syntax Error!!!! *****\n")
	        print ("Usage:")
	        print ("python <script> <folder data> <firstFrame> <lastFrame>")
	        sys.exit(1) # exit
	else:
	        pass

	pathFolder = str(sys.argv[1])
	firstFrame = int(sys.argv[2])
	lastFrame = int(sys.argv[3])

	width, height = getDimensionVideo(pathFolder)

	compareFeatures()