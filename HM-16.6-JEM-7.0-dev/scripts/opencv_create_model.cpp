#include "opencv2/core.hpp"
#include "opencv2/ml.hpp"

#include <cstdio>
#include <vector>
#include <iostream>
#include <fstream>
#include <sys/types.h>
#include <dirent.h>
#include <sstream>
#include <iterator>

using namespace std;
using namespace cv;
using namespace cv::ml;

static void help()
{
    printf("\nThe sample demonstrates how to train Random Trees classifier\n"
    "\n"
    "======================================================\n");
    printf("\nThis is letter recognition sample.\n"
            "The usage: letter_recog [-data=<path to training.data>] \\\n"
            "  [-save=<output XML file for the classifier>] \\\n"
            "  [-load=<XML file with the pre-trained classifier>] \\\n"
            "  default Random Trees\n" );
}

// This function reads data and responses from the file <filename>
static bool
read_num_class_data( const string& filename,
                     Mat* _data, Mat* _responses )
{
    const int M = 1024;
    char buf[M+2];

    int i;
    vector<int> responses;

    _data->release();
    _responses->release();

    FILE* f = fopen( filename.c_str(), "rt" );
    if( !f )
    {
        cout << "Could not read the database " << filename << endl;
        return false;
    }

    //Skip first lines
    for(int i=0;i<20;i++)
        fgets( buf, M, f );

    //Count number of features
    string line(buf);
    int n_features = count(line.begin(), line.end(), ',');
    printf("\nNUMBER OF FEATURES %i\n", n_features);
    Mat el_ptr(1, n_features, CV_32F);

    for(;;)
    {
        char* ptr;
        if( !fgets( buf, M, f ) || !strchr( buf, ',' ) )
            break;

        //Read Data
        ptr = buf;
        for( i = 0; i < n_features; i++ )
        {
            int n = 0;
            sscanf( ptr, "%f%n", &el_ptr.at<float>(i), &n );
            ptr += n + 1;
        }
        if( i != n_features ){
            printf("MAUVAISE LECTURE RESPONSE %i != %i\n", i, n_features);
            break;
        }
        _data->push_back(el_ptr);

        //Read Response
        int resp;
        int n = 0;
        sscanf( ptr, "%i%n", &resp, &n );
        responses.push_back(resp);
    }

    fclose(f);
    Mat(responses).copyTo(*_responses);

    cout << "The database " << filename << " is loaded.\n";

    return true;
}

template<typename T>
static Ptr<T> load_classifier(const string& filename_to_load)
{
    // load classifier from the specified file
    Ptr<T> model = StatModel::load<T>( filename_to_load );
    if( model.empty() )
        cout << "Could not read the classifier " << filename_to_load << endl;
    else
        cout << "The classifier " << filename_to_load << " is loaded.\n";

    return model;
}

static Ptr<TrainData>
prepare_train_data(const Mat& data, const Mat& responses, int ntrain_samples)
{
    Mat sample_idx = Mat::zeros( 1, data.rows, CV_8U );
    Mat train_samples = sample_idx.colRange(0, ntrain_samples);
    train_samples.setTo(Scalar::all(1));

    int nvars = data.cols;
    Mat var_type( nvars + 1, 1, CV_8U );
    var_type.setTo(Scalar::all(VAR_ORDERED));
    var_type.at<uchar>(nvars) = VAR_CATEGORICAL;

    return TrainData::create(data, ROW_SAMPLE, responses,
                             noArray(), sample_idx, noArray(), var_type);
}


inline TermCriteria TC(int iters, double eps)
{
    return TermCriteria(TermCriteria::MAX_ITER + (eps > 0 ? TermCriteria::EPS : 0), iters, eps);
}


// static void test_and_save_classifier(const Ptr<RTrees>& model,
static void test_and_save_classifier(const Ptr<StatModel>& model,
                                     const Mat& data, const Mat& responses,
                                     int ntrain_samples, int rdelta,
                                     const string& filename_to_save)
{
    int i, nsamples_all = data.rows;
    double train_hr = 0, test_hr = 0;

    // compute prediction error on train and test data
    for( i = 0; i < nsamples_all; i++ )
    {
        Mat sample = data.row(i);
        float r = model->predict( sample );
        r = std::abs(r + rdelta - responses.at<int>(i)) <= FLT_EPSILON ? 1.f : 0.f;
        if( i < ntrain_samples )
            train_hr += r;
        else
            test_hr += r;

        Mat output;
		    // model->getVotes( sample , output, 0);
		    for(int i=0; i<output.rows;i++){
		    	for(int j=0; j<output.cols;j++){
				    printf("%i ", output.at<int>(i,j));
				  }
				  printf("\n");
		    }
		    printf("\n");

		    // printf("%i %i\n", output.at<char>(1,0), output.at<char>(1,1));
		    // printf("%i %i\n\n", output.at<char>(2,0), output.at<char>(2,1));
		    // cout << "Number of trees: " << model->getRoots().size() << endl;
    }

    test_hr /= nsamples_all - ntrain_samples;
    train_hr = ntrain_samples > 0 ? train_hr/ntrain_samples : 1.;
    printf( "Recognition rate: train = %.1f%%, test = %.1f%%\n",
            train_hr*100., test_hr*100. );


    if( !filename_to_save.empty() )
    {
        model->save( filename_to_save );
    }
}


static bool
build_rtrees_classifier( const string& data_filename,
                         const string& filename_to_save,
                         const string& filename_to_load )
{
    Mat data;
    Mat responses;
    bool ok = read_num_class_data( data_filename, &data, &responses );
    if( !ok )
        return ok;

    Ptr<RTrees> model;

    int nsamples_all = data.rows;
    int nfeatures = data.cols;
    printf("nsamples_all nfeatures %i %i\n",nsamples_all, nfeatures );
    int ntrain_samples = (int)(nsamples_all*0.66);

    // Create or load Random Trees classifier
    if( !filename_to_load.empty() )
    {
        model = load_classifier<RTrees>(filename_to_load);
        if( model.empty() )
            return false;
        ntrain_samples = 0;
    }
    else
    {
        // create classifier by using <data> and <responses>
        cout << "Training the classifier ...\n";
//        Params( int maxDepth, int minSampleCount,
//                   double regressionAccuracy, bool useSurrogates,
//                   int maxCategories, const Mat& priors,
//                   bool calcVarImportance, int nactiveVars,
//                   TermCriteria termCrit );
        Ptr<TrainData> tdata = prepare_train_data(data, responses, ntrain_samples);
        model = RTrees::create();
        model->setMaxDepth(20);
        model->setMinSampleCount(10);
        model->setRegressionAccuracy(0);
        model->setUseSurrogates(false);
        model->setMaxCategories(20);
        model->setPriors(Mat());
        model->setCalculateVarImportance(true);
        model->setActiveVarCount(4);
        model->setTermCriteria(TC(15,0.01f));
        model->train(tdata);
        cout << endl;
    }

    test_and_save_classifier(model, data, responses, ntrain_samples, 0, filename_to_save);

    cout << "Number of trees: " << model->getRoots().size() << endl;

    // Print variable importance
    Mat var_importance = model->getVarImportance();
    if( !var_importance.empty() )
    {
        double rt_imp_sum = sum( var_importance )[0];
        printf("var#\timportance (in %%):\n");
        int i, n = (int)var_importance.total();
        for( i = 0; i < n; i++ )
            printf( "%-2d\t%-4.1f\n", i, 100.f*var_importance.at<float>(i)/rt_imp_sum);
    }

    return true;
}


void read_directory(const std::string& name, vector<std::string>& v)
{
    DIR* dirp = opendir(name.c_str());
    struct dirent * dp;
    while ((dp = readdir(dirp)) != NULL) {
        std::cout<<dp->d_name<<std::endl;
        v.push_back(dp->d_name);
    }
    closedir(dirp);
}


int main( int argc, char *argv[] )
{
    // string filename_to_save = "";
    // string filename_to_load = "";
    // string data_filename;
    // int method = 0;

    // cv::CommandLineParser parser(argc, argv, "{data|../data/letter-recognition.data|}{save||}{load||}");
    // data_filename = parser.get<string>("data");
    // if (parser.has("save"))
    //     filename_to_save = parser.get<string>("save");
    // if (parser.has("load"))
    //     filename_to_load = parser.get<string>("load");


    // help();

    // if( (method == 0 ? build_rtrees_classifier( data_filename, filename_to_save, filename_to_load ) : -1) < 0)


	  std::string scriptFolder = "/home/tamestoy/Documents/These/JEM_Codec/HM-16.6-JEM-7.0-dev/scripts/";
	  std::string fileStr = scriptFolder + "which_cu.txt";
   	ofstream fileCurCuData(fileStr.c_str(), ios::out );
	  fileCurCuData << "ddddBasketballPassdddd"<<endl;
	  fileCurCuData << "/home/tamestoy/Documents/These/JEM_Codec/HM-16.6-JEM-7.0-dev/datas/MachineLearning/models/sklearn_0All_AllTrain"<<endl;
	  fileCurCuData << 16 << " " << 64 << " "<< 64 << " "<< 64 << " "<< 64 << " "<< 22 << " " << " " << 416 << " " << 240 <<endl;

		std::string params = fileStr;
		std::string script = "probas_split_probas_single_cu.py ";
		std::string command = "python ";
		command += script;
		command += params;
		system(command.c_str());

		Py_SetProgramName("");  /* optional but recommended */
	  Py_Initialize();
	  PyRun_SimpleString("from time import time,ctime\n"
	                     "print 'Today is',ctime(time())\n");
	  Py_Finalize();

		fileCurCuData.close();


		std::ifstream file(scriptFolder + "python_kept_splits.txt");
		std::string   line;

		std::getline(file, line);
    std::istringstream buf(line);
    std::istream_iterator<std::string> beg(buf), end;

    std::vector<std::string> tokens(beg, end); // done!
    for(auto& s: tokens)
        std::cout << '"' << s << '"' << '\n';
    return 0;
}
