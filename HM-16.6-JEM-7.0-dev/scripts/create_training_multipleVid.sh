#!/bin/bash

function which_step {
	
	video_folder=$1
	
	if [[ $video_folder = *"416"* ]]; then
	  step_frame=$step_base
  elif [[ $video_folder = *"832"* ]]; then
	  step_frame=`expr $step_base \\* 3`
  elif [[ $video_folder = *"1280"* ]]; then
	  step_frame=`expr $step_base \\* 7`
	elif [[ $video_folder = *"1920"* ]]; then
	  step_frame=`expr $step_base \\* 17`
	elif [[ $video_folder = *"2560"* ]]; then
		#because Traffic only got 300 frames
	  step_frame=`expr $step_base \\* 17`
	  # step_frame=`expr $step_base \\* 31`
	elif [[ $video_folder = *"3840"* ]]; then
		#because Campfire only got 300 frames
	  step_frame=`expr $step_base \\* 53`
	  # step_frame=`expr $step_base \\* 63`
	else
		echo "CAREFULL MAYBE WRONG VIDEO RESOLUTION"
		exit
	fi
}


echo "num parameters "$#
# if [ "$#" -ne 4 ] && [ "$#" -ne 5 ]; then
if [ "$#" -ne 5 ] ;  then
    echo "Illegal number of parameters"
    echo
    echo "USE ./create_training_multipleVid.sh <base_video_folder> <classifier> <step_frame> <num by class> <many_qp?>"
    exit
fi

base_folder=$1
classifier=$2
step_base=$3
num_by_class=$4
end_frame=600
# end_frame=300

many_qp=$5
if [ $many_qp != "yes" ] && [ $many_qp != "no" ]; then
	echo "Last parameter : many_qp "
	echo "Value must be : yes or no"
	exit
fi

for qp in 22 27 32 37;do
	# for size in 128 64 32 16;do
	for size in `seq 0 6` ;do

		if [ $many_qp = "yes" ] ; then
			qp_in_filename="_qp"${qp}
		fi
		training_file_global=$base_folder"/training_class"${classifier}"_size"${size}${qp_in_filename}"_step"${step_base}".arff"
		rm $training_file_global
		touch $training_file_global 

	done
done


numVid=0
frame_number="1"
while [ $frame_number -lt $end_frame ] ;do
	block_files=`find $base_folder -name block_info_${frame_number}`
	echo 
	echo "**********" 

	for bf in $block_files; do
		echo 
		video_folder=${bf%block_info_$frame_number}

		which_step ${video_folder}
		# [ "$#" == 0 ] || [ "$#" -gt 1 ]
		if [ "$step_frame" != 1 ] && [ $(($frame_number % $step_frame)) != 1 ]; then
			continue;
		fi
		echo "FRAME NUMBER "$frame_number 
		echo "STEP FRAME "$step_frame
		echo "modulo "$(($frame_number % $step_frame)) 

		training_format="data"
		if [ $numVid = "0" ];then
			training_format="arff"
			echo "training_format ---> "$training_format
		fi

		echo "python create_training_file.py $video_folder $training_format $frame_number $frame_number $classifier"
		python create_training_file.py $video_folder $training_format $frame_number $frame_number $classifier $many_qp

		if [ $many_qp = "yes" ] ; then
			for qp in 22 27 32 37;do
				qp_in_filename="_qp"${qp}
				# for size in 128 64 32 16;do
				for size in `seq 0 6` ;do
					training_file_global=$base_folder"/training_class"${classifier}"_size"${size}${qp_in_filename}"_step"${step_base}".arff"
					cat "$video_folder/training_${classifier}_s${size}${qp_in_filename}.arff" >> $training_file_global
				done
			done
		
		else

			# for size in 128 64 32 16;do
			for size in `seq 0 6` ;do
				training_file_global=$base_folder"/training_class"${classifier}"_size"${size}"_step"${step_base}".arff"
				cat "$video_folder/training_${classifier}_s${size}.arff" >> $training_file_global
			done
		fi

		numVid=`expr $numVid + 1`
	done

	frame_number=`expr $frame_number + $step_base`
done


for qp in 22 27 32 37;do
	# for size in 128 64 32 16;do
	for size in `seq 0 6` ;do
		echo
		echo "randomize training file -> ${size} ${qp}..."

		if [ $many_qp = "yes" ] ; then
			qp_in_filename="_qp"${qp}
		fi
		training_file_global=$base_folder"/training_class"${classifier}"_size"${size}${qp_in_filename}"_step"${step_base}".arff"

		echo "python randomize_training_file.py $training_file_global ${num_by_class}"
		python randomize_training_file.py $training_file_global ${num_by_class}
		echo
	done
done

#
#if [ "$#" -ne 5 ];then
#	exit
#else
#	for size in 128 64 32 16;do
#		echo
#		echo "create model size ${size}..."
#		model_folder=$5
#		mkdir -p ${model_folder}
#
#		training_file_global=$base_folder"/training_class"${classifier}"_size"${size}"_step"${step_base}".arff"
#		python sklearn_machine_learning.py --data $training_file_global --percent 98 --numtrees 15 --save ${model_folder}/test_${classifier}_${size}.model
#		echo
#	done
#fi

