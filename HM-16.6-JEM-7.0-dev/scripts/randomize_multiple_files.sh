#!/bin/bash
echo "num parameters "$#
if [ "$#" -ne 2 ]; then
    echo "Illegal number of parameters"
    echo
    echo "USE ./create_training_multipleVid.sh <training_files_folder> <num_by_class>"
    exit
fi
training_files_folder=$1
num_by_class=$2

files=`ls -1 $training_files_folder`
echo $files
for file in $files;do

	if [[ $file != "training_class"* ]]; then
		continue
	fi
	echo
	echo "randomize training file -> $file..."

	# echo "python randomize_training_file.py $training_files_folder/$file ${num_by_class}"
	python randomize_training_file.py $training_files_folder/$file ${num_by_class}
	echo
done