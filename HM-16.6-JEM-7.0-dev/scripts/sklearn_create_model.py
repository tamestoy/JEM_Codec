# Author: Alexandre Gramfort <alexandre.gramfort@inria.fr>
# License: BSD 3 clause

import matplotlib.pyplot as plt
import numpy as np
from time import time

from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn import datasets
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.model_selection import GridSearchCV


#*************************************************************
def load_base(fn):

		if fn[-4:] == "arff":
				a = np.loadtxt(fn, str, delimiter=',',skiprows=35)
		else:
				a = np.loadtxt(fn, str, delimiter=',',skiprows=0)
	 
		lastString = a[1,-1][0]
		if lastString == '{':
				samples = a[:,:-2]
				responses = a[:,-2]
		else:
				samples = a[:,:-1]
				responses = a[:,-1]

		samples = samples.astype(np.float32)
		responses = responses.astype(np.float32)

		weights = a[:,1]
		for i in range(0,np.size(weights)):
				lastString = a[i,-1]
				if lastString[0] == '{':
						weights[i] = float(lastString[1:-1])
				else:
						weights[i] = 1.0
		weights = weights.astype(np.float32)

		return samples, responses, weights
#*************************************************************


#*************************************************************
# Utility function to report best scores
def report(results, n_top=3):
		for i in range(1, n_top + 1):
				candidates = np.flatnonzero(results['rank_test_score'] == i)
				for candidate in candidates:
						print("Model with rank: {0}".format(i))
						print("Mean validation score: {0:.3f} (std: {1:.3f})".format(
									results['mean_test_score'][candidate],
									results['std_test_score'][candidate]))
						print("Parameters: {0}".format(results['params'][candidate]))
						print("")
#*************************************************************


if __name__ == '__main__':
		import getopt
		import sys
		import pickle

		args, dummy = getopt.getopt(sys.argv[1:], '', ['data=', 'load=', 'save=', 'percent=', 'numtrees='])
		args = dict(args)
		args.setdefault('--data', 'split_Cuentiere_sauvegarde.data')
		args.setdefault('--percent', '50')
		args.setdefault('--numtrees', '20')

		# print('loading data %s ...' % args['--data'])
		X,y,w = load_base(args['--data'])  


		#SEPARER DONNEES ENTRAINEMENT ET TEST
		n_features = X.shape[1]
		# print ("n_features", n_features)

		C = 1.0
		kernel = 1.0 * RBF(np.ones(n_features))  # for GPC

		percentTrain = float(args['--percent'])
		# print ("train on %i percent ", int(percentTrain))
		train_n = int(len(X)*percentTrain/100)
		# print ("train_n", train_n)

		xtest = X[train_n:]
		ytest = y[train_n:]
		wtest = w[train_n:]


		#SEARCH PARAMETERS FOR RANDOM FOREST
		searchParams = 0
		if searchParams == 1:
				clf = RandomForestClassifier(n_estimators=15)

				# use a full grid over all parameters
				param_grid = {"max_depth": [3, 10, None],
											"max_features": ['auto', 3, 4],
											"min_samples_split": [2, 3, 4],
											"min_samples_leaf": [1, 3, 5],
											"random_state": [0,1],
											"bootstrap": [True, False],
											"criterion": ["gini", "entropy"]}

				# run grid search
				grid_search = GridSearchCV(clf, param_grid=param_grid)
				start = time()
				grid_search.fit(X[:train_n], y[:train_n])

				print("GridSearchCV took %.2f seconds for %d candidate parameter settings."
							% (time() - start, len(grid_search.cv_results_['params'])))
				report(grid_search.cv_results_)



		# Create different classifiers. The logistic regression cannot do
		classifiers = {#'Linear SVC': SVC(kernel=kernel, C=C, probability=True,random_state=0),
							 # 'L2 logistic (Multinomial)': LogisticRegression(C=C, solver='lbfgs', multi_class='multinomial'),
							 # 'GPC': GaussianProcessClassifier(kernel),
						
						# 01
						# Parameters: {'bootstrap': False, 'min_samples_leaf': 1, 'min_samples_split': 2, 
						# 'criterion': 'entropy', 'max_features': 3, 'max_depth': None}
										# 'Random Forest 01' : RandomForestClassifier(bootstrap=False, class_weight=None, criterion='entropy',
										#     max_depth=None, max_features=3, max_leaf_nodes=None,
										#     min_impurity_decrease=0.0, min_impurity_split=None,
										#     min_samples_leaf=1, min_samples_split=2,
										#     min_weight_fraction_leaf=0.0, n_estimators=int(args['--numtrees']), n_jobs=1,
										#     oob_score=False, random_state=1, verbose=0, warm_start=False),

						# 23
						#parameters: {'bootstrap': False, 'min_samples_leaf': 3, 'min_samples_split': 2, 
						#'random_state': 1, 'criterion': 'gini', 'max_features': 'auto', 'max_depth': 10}
							 'Random Forest 23' : RandomForestClassifier(bootstrap=False, class_weight=None, criterion='entropy',
												max_depth=20, max_features='auto', max_leaf_nodes=None,
												min_impurity_decrease=0.0, min_impurity_split=None,
												min_samples_leaf=3, min_samples_split=2,
												min_weight_fraction_leaf=0.0, n_estimators=int(args['--numtrees']), n_jobs=1,
												oob_score=False, random_state=1, verbose=0, warm_start=False)
							 }

		n_classifiers = len(classifiers)
		#******************************************************
		if '--load' in args:
				fn = args['--load']
				print('loading model from %s ...' % fn)
				loaded_model = pickle.load(open(fn, 'rb'))
				
				xtest = X
				ytest = y
				wtest = w
				ypred = loaded_model.predict(xtest)

				good = 0
				goodWeight = 0
				totWeight = 0
				for i in range(0,len(ypred)):
						if ypred[i] == ytest[i]:
								good = good + 1
								goodWeight = 1.0*goodWeight + wtest[i]

						totWeight = 1.0*totWeight + wtest[i]

				print("normal_rate   : %f " % ( 100 * good*1.0/len(ypred) ) )
				print("weighted_rate : %f " % ( 100 * goodWeight*1.0/totWeight))


		#******************************************************    
		else:

				# for index, (name, classifier) in enumerate(classifiers.items()):
				# 	classifier.fit(X[:train_n], y[:train_n])
				# 	ypred = classifier.predict(xtest)

				# 	good = 0
				# 	goodWeight = 0
				# 	totWeight = 0
				# 	for i in range(0,len(ypred)):
				# 			if ypred[i] == ytest[i]:
				# 					good = good + 1
				# 					goodWeight = 1.0*goodWeight + wtest[i]

				# 			totWeight = 1.0*totWeight + wtest[i]

					# print("%f,%f \n\n" % ( 100 * good*1.0/len(ypred), 100 * goodWeight*1.0/totWeight ))

					# print("\nLearn without weights" )
					# print("normal_rate for %s : %f " % (name, 100 * good*1.0/len(ypred) ) )
					# print("weighted_rate                     : %f " % ( 100 * goodWeight*1.0/totWeight))

					
				for index, (name, classifier) in enumerate(classifiers.items()):
						startTime = time()
						classifier.fit(X[:train_n], y[:train_n], sample_weight=w[:train_n])
						ypred = classifier.predict(xtest)

						good = 0
						goodWeight = 0
						totWeight = 0
						for i in range(0,len(ypred)):
								if ypred[i] == ytest[i]:
										good = good + 1
										goodWeight = 1.0*goodWeight + wtest[i]

								totWeight = 1.0*totWeight + wtest[i]

						# print("Learn with weights" )
						print("%f,%f" % ( 100 * good*1.0/len(ypred), 100 * goodWeight*1.0/totWeight ) )
						# print("normal_rate for %s : %f " % (name, 100 * good*1.0/len(ypred) ) )
						# print("weighted_rate                     : %f " % ( 100 * goodWeight*1.0/totWeight))
						# print ("Time %f sec\n", time() - startTime)


		#******************************************************
		if '--save' in args:
				fn = args['--save']
				print('saving model to %s ...' % fn)
				pickle.dump(classifier, open(fn, 'wb'))
