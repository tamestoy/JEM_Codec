#!/bin/bash

echo "num parameters "$#
if [ "$#" -ne 3 ];  then
    echo "Illegal number of parameters"
    echo
    echo "USE ./sklearn_multiple_model.sh <base folder of random files> <opencv or sklearn ?> <many qp ?>"
    exit
fi

base_folder=$1
ml_library=$2
many_qp=$3
if [ $many_qp != "yes" ] && [ $many_qp != "no" ]; then
	echo "Last parameter : many_qp "
	echo "Value must be : yes or no"
	exit
fi

# for size in 128 64 32 16; do
for size in `seq 0 6` ;do
	echo
	echo
	echo "SIZE "$size
	if [ $many_qp = "no" ] ; then
		training_files=`find $base_folder -name "random*size${size}*arff"`
		for file in $training_files;do
			echo
			# echo "create model from file --> $file..."

			model_file=${file%"arff"}
			model_file=${model_file}"model"

			if [ $ml_library = "sklearn" ]; then  
				python sklearn_create_model.py --data $file --percent 98 --numtrees 20 --save ${model_file}
				# python sklearn_create_model.py --data $file --percent 66 --numtrees 20 
			else
				./opencv_create_model --data=$file --save=${model_file}
			fi
			echo
		done
	else
		for qp in 37 32 27 22; do
			training_files=`find $base_folder -name "random*size${size}*qp${qp}*arff"`
			for file in $training_files;do
				echo
				# echo "create model from file --> $file..."

				model_file=${file%"arff"}
				model_file=${model_file}"model"

				if [ $ml_library = "sklearn" ]; then  
					# python sklearn_create_model.py --data $file --percent 98 --numtrees 15 --save ${model_file}
					python sklearn_create_model.py --data $file --percent 66 --numtrees 15 
				else
					./opencv_create_model --data=$file --save=${model_file}
				fi
				echo
			done
		done
	fi
done
