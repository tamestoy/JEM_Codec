import numpy as np
import cv2
from PIL import Image
import sys
from struct import *
import array
import math

def getFrame(image_name,nframe,width,height):
	 
	y = array.array('B')
	u = array.array('B')
	v = array.array('B')
	
	f_y = open(image_name, "rb")
	f_uv = open(image_name, "rb")
	startY = nframe * width * height * 1.5
	startUV = startY + width*height
	f_y.seek(startY)
	f_uv.seek(startUV)
	 
	image_out = Image.new("RGB", (width, height))
	pix = image_out.load()
	 
	print "width=", width, "height=", height
	 
	for i in range(0, height/2):
	    for j in range(0, width/2):
	        u.append(ord(f_uv.read(1)));
	 
	for i in range(0, height/2):
	    for j in range(0, width/2):
	        v.append(ord(f_uv.read(1)));
	for i in range(0,height):
	    for j in range(0, width):
	        y.append(ord(f_y.read(1)));
	        #print "i=", i, "j=", j , (i*width), ((i*width) +j)
	        #pix[j, i] = y[(i*width) +j], y[(i*width) +j], y[(i*width) +j]
	        Y_val = y[(i*width)+j]
	        U_val = u[((i/2)*(width/2))+(j/2)]
	        V_val = v[((i/2)*(width/2))+(j/2)]
	        B = 1.164 * (Y_val-16) + 2.018 * (U_val - 128)
	        G = 1.164 * (Y_val-16) - 0.813 * (V_val - 128) - 0.391 * (U_val - 128)
	        R = 1.164 * (Y_val-16) + 1.596*(V_val - 128)
	        pix[j, i] = int(R), int(G), int(B)
	 
	######################################################
	# B = 1.164(Y - 16)                   + 2.018(U - 128)
	# G = 1.164(Y - 16) - 0.813(V - 128) - 0.391(U - 128)
	# R = 1.164(Y - 16) + 1.596(V - 128)
	######################################################
	 
	#image_out.save("out.bmp")
	# image_out.show()
	return np .array(image_out.getdata(), 
		np .uint8).reshape(image_out.size[1], image_out.size[0], 3)



######################################################
def getDimensionVideo(pathFolder):
	predFile = open(pathFolder + '/block_info_1')

	for line in predFile:
		donnees = line.split()
	
	donnees = map(int,donnees)
	widthVideo = donnees[0]+4
	heightVideo = donnees[1]+4
	return widthVideo,heightVideo

######################################################



if len(sys.argv) != 6:
        print "***** Usage syntax Error!!!! *****\n"
        print "Usage:"
        print "python <script> <pathtoYUV> <pathFolder> <nframes> <W> <H>"
        sys.exit(1) # exit
else:
        pass

image_name = str(sys.argv[1])
pathFolder = str(sys.argv[2])
nframes = int(sys.argv[3])
width = int(sys.argv[4])
height = int(sys.argv[5])

# width, height = getDimensionVideo(pathFolder)	
# width = 832
# height = 480
# 3840x2160
for count in range(1,nframes):
# for count in range(nframes,nframes+5):
	#avoid intra frames
	if count%16 == 0:
		continue

	s = str(count)
	# img = getFrame(image_name,count,width,height)
	# open input/output files
	# mvfile = open(pathFolder +'/block_info_' + s)
	# for i in range(1): mvfile.next() # skip first line
	# for line in mvfile:
	# 	donnees_block = map(int,line.split())
	# 	mvHor = donnees_block[2]
	# 	mvVer = donnees_block[3]
	# 	gradx = donnees_block[6]
	# 	grady = donnees_block[7]
	# 	var = donnees_block[8]
	# 	# norm = math.sqrt(mvHor*mvHor + mvVer*mvVer)
	# 	norm = 125 + (mvHor + mvVer)/6.0
	# 	cv2.rectangle(img,(donnees_block[0],donnees_block[1]),
	# 		(donnees_block[0]+4,donnees_block[1]+4),(0,abs(mvHor),abs(mvVer)),-1)
			# (donnees_block[0]+4,donnees_block[1]+4),(var,0,0),-1)
			# (donnees_block[0]+4,donnees_block[1]+4),(norm,norm,norm),-1)

	# cv2.imwrite('mv' + s + '.png',img)
	# mvfile.close()


	img_decoupe = getFrame(image_name,count,width,height)
	inputfile = open(pathFolder +'/choice_split_frame_' + s)
	for i in range(1): inputfile.next() # skip first line
	for line in inputfile:
		coordonnees_coins = map(int,line.split())
		aire = coordonnees_coins[2]*coordonnees_coins[3]
		# cv2.rectangle(img,(coordonnees_coins[0],coordonnees_coins[1]),
			# (coordonnees_coins[0]+coordonnees_coins[2],coordonnees_coins[1]+coordonnees_coins[3]),(255,0,0),2)
		c = (0,0,255)
		if len(coordonnees_coins) == 6:
			if coordonnees_coins[5] == 1:
				c = (0,255,0)

		epaisseur = min(4,max(1,width/400))
		cv2.rectangle(img_decoupe,(coordonnees_coins[0],coordonnees_coins[1]),
			(coordonnees_coins[0]+coordonnees_coins[2],coordonnees_coins[1]+coordonnees_coins[3]),c,epaisseur)

	inputfile.close()

	# cv2.imwrite(pathFolder +'/mv' + s + '.png',img)
	cv2.imwrite(pathFolder +'/split' + s + '.png',img_decoupe)
	# cv2.imwrite(pathFolder +'/grad' + s + '.png',img)

