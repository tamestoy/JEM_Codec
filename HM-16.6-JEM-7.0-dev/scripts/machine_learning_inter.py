import numpy as np
import pickle
import sys
import matplotlib.pyplot as plt
import math
from time import time
from create_training_file import *
import os


# ************************************************************
def getModelFiles(modelFolder):

	for filename in os.listdir(modelFolder):
		if filename.startswith("random_") and filename.endswith("model"): 
			# for i in range(0,4):
			for i in range(0,numCategoryCu):
				# size = 128 / (2**i)
				size = i
				for categoryQP in range(0,numCategoryQP):
					qp = 22 + categoryQP*stepQP

					# if ("1All" in filename) and ("size"+str(size) in filename) and ("qp"+str(qp) in filename):
					if ("1All" in filename) and ("size"+str(size) in filename):
						print('loading model 1All from %s ...' % filename)
						print('insert model -> %i %i' % (i, categoryQP))
						model_1All[i].insert(categoryQP,pickle.load(open(modelFolder+"/"+filename, 'rb')))
						break

					# if ("0All" in filename) and ("size"+str(size) in filename) and ("qp"+str(qp) in filename) :
					if ("0All" in filename) and ("size"+str(size) in filename)  :
						print('loading model 0All from %s ...' % filename)
						print('insert model -> %i %i' % (i, categoryQP))
						model_0All[i].insert(categoryQP,pickle.load(open(modelFolder+"/"+filename, 'rb')))
						break

# ************************************************************



# ************************************************************
def calcProbaSplits(predsBloc4x4,arraySplitData,pelX,pelY,widthCU,heightCU,QP,temporality):

	categoryCU =  int(math.log((128*128)/(widthCU*heightCU),2))/2

	proba = [0]*4
	n_tests = 0
	thresholds = [intervalle, 0.1, intervalle, intervalle]
	classif = [[0,0],[0,0],[0,0]]

	if (widthCU == heightCU):
		choiceFeatures = calcFeaturesNoclass_split(predsBloc4x4 ,pelX,pelY,widthCU,heightCU,QP,temporality)
		proba01 = model_1All[categoryCU][categoryQP].predict_proba([choiceFeatures])

		proba[0] = proba01[0][0]
		proba[1] = proba01[0][1]
		n_tests = n_tests + 1
		classif[0] = [proba01[0][0]>0.5,proba01[0][1]>=0.5]


	choiceFeatures = calcFeaturesNosplitHor(predsBloc4x4 ,pelX,pelY,widthCU,heightCU,QP,temporality)
	proba02 = model_02[categoryCU].predict_proba([choiceFeatures])
	choiceFeatures = calcFeaturesNosplitVer(predsBloc4x4 ,pelX,pelY,widthCU,heightCU,QP,temporality)
	proba03 = model_03[categoryCU].predict_proba([choiceFeatures])
	classif[1:2] = [[proba02[0][0]>0.5,proba02[0][1]>=0.5],[proba03[0][0]>0.5,proba03[0][1]>=0.5]]
	
	if proba[0] > thresholds[1] or widthCU != heightCU:
		n_tests = n_tests + 2

		proba[0] = (proba[0] + proba02[0][0] + proba03[0][0]) / n_tests
		proba[2] = proba02[0][1]
		proba[3] = proba03[0][1]

		if proba[0] > thresholds[0] or proba[1] > thresholds[1]:
			if (proba02[0][1] > proba03[0][1]):
				proba[3] = 0.0
			else:
				proba[2] = 0.0

	keepSplit = [0]*4
	for i in range(4):
		if proba[i] > thresholds[i]:
			keepSplit[i] = 1


	return keepSplit, classif

# ************************************************************



# ************************************************************
def statsChoicePartition():
	#LOOP ON ALL FRAMES
	nb_intervals = 10

	splitFoundList = []
	for i in range(0,4):
		splitFoundList.insert(i,[0]*4)

	nbType = [0]*4
	totExplore = [0]*4
	
	good_classif = [0]*numCategoryCu
	total_classif = [0]*numCategoryCu
	effective_error = [0]*numCategoryCu
	total_error = [0]*numCategoryCu
	effective_time = [0]*numCategoryCu
	total_time = [0]*numCategoryCu
	for i in range(0,numCategoryCu):
		good_classif.insert(i,[0]*(nb_intervals))
		total_classif.insert(i,[0]*(nb_intervals))
		effective_error.insert(i,[0]*(nb_intervals))
		total_error.insert(i,[0]*(nb_intervals))
		effective_time.insert(i,[0]*(nb_intervals))
		total_time.insert(i,[0]*(nb_intervals))

	array_errors = [[0,7,7,7],[50,0,12,12],[12,5,0,10],[12,5,12,0]]
	array_times = [[34,1199,185,185],  [19,0,37,37],  [10,403,66,66],  [7,0,19,19],  [3,44,14,14],  [2,0,9,9]]

	# for count in range(1,nframes):
	for count in range(160,160+nframes):
		
		#avoid intra frames
		if count%16 == 0:
			continue
		#Calc QP of specific frame (for random access)
		temporality = getTemporality(count)
		qpFrame = int(QP)+QP_offsets[temporality]
		categoryQP = min(numCategoryQP-1,(qpFrame-22)/stepQP)

		# open input/output files
		s = str(count)
		namePredFile = pathFolder + '/block_info_' + s
		predsBloc4x4 = getPredInfos(namePredFile,width,height,sizeBlock)

		nameSplitFile = pathFolder + '/choice_split_frame_' + s
		arraySplitData = getSplitInfos(nameSplitFile,width,height,sizeBlock)
		
		cuInfoFile = open(pathFolder + '/cu_info_' + s)
		for i in range(1): cuInfoFile.next() # skip first line

		listCuData = []
		startTime = time()
		for line in cuInfoFile:
			intraCU = map(float,line.split())
			intraCU = map(int,intraCU)
			listCuData.append(intraCU)
			pelX = intraCU[0]
			pelY = intraCU[1]
			widthCU = intraCU[2]
			heightCU = intraCU[3]

			#POIDS DE L'ERREUR
			costs_splits = intraCU[4:8]

			if (widthCU  > sizeBlock and heightCU > sizeBlock and widthCU*heightCU > 16*16 and pelX+widthCU<width and pelY+heightCU<height):
				
				# splitType = intraCU[4]
				splitType = intraCU[8]
				# categoryCU =  int(math.log((128*128)/(widthCU*heightCU),2))/2
				categoryCU =  int(math.log((128*128)/(widthCU*heightCU),2))

				#******************************************
				#TEMPORAIRE, TEST 1VSALL
				# weightError, error, truecost  = calcWeight(costNS,costQt,costHor,costVer,"test1All")
				# if weightError < 0.001 or widthCU!=heightCU :
				# 	continue

				# nbType[splitType] = nbType[splitType] + 1
				# class_split = splitType
				# if (class_split != 1): 
				# 	class_split = 0

				# choiceFeatures = calcFeaturesQTvsAll(predsBloc4x4,arraySplitData,pelX,pelY,widthCU,heightCU,QP,temporality)
				# proba1All = model_1All[categoryCU][categoryQP].predict_proba([choiceFeatures])
				# keepSplit = [proba1All[0][0]>0.5,   proba1All[0][1]>=0.5,   proba1All[0][0]>0.5,proba1All[0][0]>0.5]
				#******************************************


				#******************************************
				#TEMPORAIRE, TEST 0VSALL
				weightError, error, truecost  = calcWeight(costs_splits[0],costs_splits[1],costs_splits[2],costs_splits[3],"test0All")
				# if not(widthCU==heightCU and (splitType == 0 or costQt > 0.001)) :
				# 	continue

				nbType[splitType] = nbType[splitType] + 1
				class_split = splitType
				if (class_split != 0): 
					class_split = 1

				choiceFeatures = calcFeaturesQTvsAll(predsBloc4x4,arraySplitData,pelX,pelY,widthCU,heightCU,QP,temporality)
				proba0All = model_0All[categoryCU][categoryQP].predict_proba([choiceFeatures])
				

				#******************************************
				#STATISTIQUES DU CLASSIFIEUR

				#On garde tout si on est dans un intervalle de probas proche de 0.5
				for interval in range(0,nb_intervals):
					keepSplit = [proba0All[0][0]>=0.5- 0.05*interval,   proba0All[0][1] >= 0.5- 0.05*interval,   proba0All[0][1] >= 0.5- 0.05*interval,proba0All[0][1] >= 0.5- 0.05*interval]

					#calcul des temps economises
					for i in range(0,4):
						if costs_splits > 0.01:
							if keepSplit[i] == True:
								effective_time[categoryCU][interval] = effective_time[categoryCU][interval] + array_times[categoryCU][i]
							total_time[categoryCU][interval] = total_time[categoryCU][interval] + array_times[categoryCU][i]
						
					#Calcul des temps et erreurs realisees
					if (class_split == 0 and keepSplit[0]) or (class_split == 1 and keepSplit[1]):
						good_classif[categoryCU][interval] = good_classif[categoryCU][interval] + 1
					else : 
						effective_error[categoryCU][interval] = effective_error[categoryCU][interval] + error
					total_error[categoryCU][interval] = total_error[categoryCU][interval] + error
					total_classif[categoryCU][interval] = total_classif[categoryCU][interval] + 1

						# if splitType == 2 or splitType == 3:
						# 	weightError2, error2, truecost2  = calcWeight(costNS,costQt,costHor,costVer,"test1BT")
						# 	effective_error[class_split][i] = effective_error[class_split][i] + error2

				#******************************************

				# print("\nsplit %i" % splitType)
				# keepSplit,classif = calcProbaSplits(predsBloc4x4,arraySplitData,pelX,pelY,widthCU,heightCU,QP,temporality)
				# errors = []
				# if (keepSplit[splitType] == 1) :
				# 	splitFoundList[splitType][splitType] = splitFoundList[splitType][splitType] + 1
				# 	totExplore[splitType] = totExplore[splitType] + 1
				# 	for i in range(0,4):
				# 		if (i != splitType and keepSplit[i] == 1) :
				# 			totExplore[splitType] = totExplore[splitType] + 1
				# else:
				# 	for i in range(0,4):
				# 		if (i != splitType and keepSplit[i] == 1) :
				# 			splitFoundList[splitType][i] = splitFoundList[splitType][i] + 1
				# 			totExplore[splitType] = totExplore[splitType] + 1

		endTime = time()
		# print ("Image num", count)
		# print ("Time %f sec\n", endTime - startTime)
				
	# for splitType in range(0,4):
	# # for splitType in range(0,2):
	# 	plt.bar([0,1,2,3], splitFoundList[splitType], 0.8, color='g' )
	# 	plt.xlabel('Mon split ')
	# 	plt.ylabel('Nombre ')
	# 	plt.title("Vrai Split : " + str(splitType) + "\n Optimal :" +  str(nbType[splitType]) + ", Explores :" +  str(totExplore[splitType]) )
	# 	if (splitType != 3):
	# 	# if (splitType != 1):
	# 		plt.figure()

	# print "\nSPLITS PAR NOEUDS :", 1.0 *(totExplore[0]+totExplore[1]+totExplore[2]+totExplore[3])/(nbType[0]+nbType[1]+nbType[2]+nbType[3])
	
	for categoryCU in range(0,numCategoryCu):
		print"\n\n Size CU", categoryCU
		for interval in range(0,nb_intervals):
			print"\n Interval", interval*0.05
			print"PERCENT CLASSIF 0All : " , 100.0 * good_classif[categoryCU][interval] / total_classif[categoryCU][interval], "%  (",total_classif[categoryCU][interval],")"
			print"ERREUR PONDEREE MOY  : " , 100.0 * effective_error[categoryCU][interval]/total_error[categoryCU][interval],"%  "
			print"TEMPS MOY PARCOURU   : " , 100.0 * effective_time[categoryCU][interval]/total_time[categoryCU][interval],"%  "
	

	cuInfoFile.close()
# *************************************************************


if __name__ == '__main__':


	if len(sys.argv) != 4:
	        print ("***** Usage syntax Error!!!! *****\n")
	        print ("Usage:")
	        print ("python <script> <videoData Folder> <model Folder> <nframes>")
	        sys.exit(1) # exit
	else:
	        pass

	pathFolder = str(sys.argv[1])
	modelFolder = str(sys.argv[2])
	nframes = int(sys.argv[3])
	intervalle = 20

	QP = getQP(pathFolder)
	width, height = getDimensionVideo(pathFolder)
	print "Nframes", nframes
	print "width, height", width, height
	print "QP", QP

	#CHOSE HOW TO RANK A SPLIT
	sizeBlock = 4
	rankStyle=11

	#INTERVALLE DE CONFIANCE
	intervalle = 1.0 * intervalle / 100
	# print "Proba Min pour split : ", intervalle

	oneClassbyQP = False
	numCategoryQP = 1
	if oneClassbyQP == True: 
		numCategoryQP = 4
	stepQP = (44-22)/numCategoryQP

	numCategoryCu = 6
	model_1All = []
	model_0All = []
	for j in range(0,numCategoryCu):
		model_1All.append([])
		model_0All.append([])
	# model_02 = []
	# model_03 = []
	getModelFiles(modelFolder)

	statsChoicePartition()

	plt.show()	

