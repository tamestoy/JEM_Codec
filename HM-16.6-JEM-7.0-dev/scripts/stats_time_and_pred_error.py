import numpy as np
import pickle
import sys
import matplotlib
import matplotlib.pyplot as plt
import math
from path import path
from create_training_file import *



# ************************************************************
def indexInsert(energyList,energie):
	
	i=0
	length = len(energyList)
	if(length==0):
		return i

	while(energie>energyList[i]):
		# print ("index ",i, len(energyList), energie, energyList[i])
		i = i+1+length/100
		if(i >= length):
			return i

	return i
# ************************************************************



#************************************************************
def getAllCuInfoFiles(pathFolder,nframes):

	recursiveFiles = path(pathFolder).walkfiles()
	cuInfoFiles = []
	for f in recursiveFiles:
		for count in range(1,nframes):
		
			#avoid intra frames
			if count%16 == 0:
				continue
			
			s = str(count)
			if f.endswith('/cu_info_' + s):
				cuInfoFiles.append(str(f))

	return cuInfoFiles

#************************************************************



# ************************************************************
def statsErrorPartition():
	#LOOP ON ALL FRAMES
	numCatCu = 8 
	errorCost = []
	totSplits = []
	totTimes = []
	totRencontre = []
	avg_time = []
	for i in range(0,numCatCu):
		errorCost.append([]*4)
		totSplits.append([]*4) 
		totTimes.append([0]*4)
		totRencontre.append([0]*4)
		avg_time.append([0]*4)
		for j in range(0,4):
			errorCost[i].append([0]*4)
			totSplits[i].append([0]*4)

	#get all files in pathFolder containing CU erro infos
	cuInfoFiles = getAllCuInfoFiles(pathFolder,nframes)

	energieStyle = 0
	for errorFile in cuInfoFiles:

		#getQP to difficult to use

		# open input/output files
		inputfile = open(errorFile)
		for i in range(1): inputfile.next() # skip first line

		listCuData = []
		for line in inputfile:
			intraCU = map(float,line.split())
			pelX = intraCU[0]
			pelY = intraCU[1]
			widthCU = intraCU[2]
			heightCU = intraCU[3]

			splitType = int(intraCU[8])
			trueCosts = intraCU[4:8]

			trueTimes = [0,0,0,0]
			if len(intraCU) > 9:
				trueTimes = intraCU[9:13]
				# print trueTimes[0], trueTimes[1],trueTimes[2],trueTimes[3]
			# print splitType
			# print trueCosts

			gg = 8/numCatCu
			categoryCU =  min(numCatCu-1,int(math.log((128*128)/(widthCU*heightCU),2))/gg)

			for i in range(0,4):
				if (trueCosts[i] > 0.1 and trueCosts[splitType] > 0.1) :
					errorCost[categoryCU][splitType][i] = errorCost[categoryCU][splitType][i] + abs(trueCosts[i] - trueCosts[splitType])/trueCosts[splitType]
					totSplits[categoryCU][splitType][i] = totSplits[categoryCU][splitType][i] + 1

			for i in range(0,4):
				if trueTimes[i] > 0:
					totTimes[categoryCU][i] = totTimes[categoryCU][i] + trueTimes[i]
					totRencontre[categoryCU][i] = totRencontre[categoryCU][i] + 1

	 	# print ("file num", errorFile)

	#Faire la moyenne
	for splitType in range(0,4):
		for j in range(0,4):
			for categoryCU in range(0,numCatCu):
				errorCost[categoryCU][splitType][j] = 100.0 * errorCost[categoryCU][splitType][j] / max(totSplits[categoryCU][splitType][j],0.1)


	#Ecrire informations de temps
	for categoryCU in range(0,numCatCu):
		print "Size Cu", categoryCU
		for split in range(0,4):
			avg_time[categoryCU][split] = totTimes[categoryCU][split]/max(0.1,totRencontre[categoryCU][split])

	for categoryCU in range(0,numCatCu):
		print('%i   %i  %i   %i' %(avg_time[categoryCU][0],avg_time[categoryCU][1],avg_time[categoryCU][2],avg_time[categoryCU][3]))


	#Tracer les Histogrammes
	# x_labels = ['NoSplit', 'QT', 'Hor', 'Ver']
	# bluePatch = matplotlib.patches.Rectangle((0, 0), 0, 0, color = 'b')
	# redPatch = matplotlib.patches.Rectangle((0, 0), 0, 0, color = 'r')
	# greenPatch = matplotlib.patches.Rectangle((0, 0), 0, 0, color = 'g')
	# yellowPatch = matplotlib.patches.Rectangle((0, 0), 0, 0, color = 'y')
	# for splitType in range(0,4):
	# 	x = np.array([0,1,2,3])

	# 	plt.bar(x-0.3, errorCost[0][splitType], width=0.2, color='b' )
	# 	plt.bar(x-0.1, errorCost[1][splitType], width=0.2, color='r' )
	# 	plt.bar(x+0.1, errorCost[2][splitType], width=0.2, color='g' )
	# 	plt.bar(x+0.3, errorCost[3][splitType], width=0.2, color='y' )
		

	# 	plt.legend([bluePatch, redPatch, greenPatch, yellowPatch], ['>64x64', '>32x32','>16x16','<=16x16'], markerscale = 100, frameon = False, fontsize = 10)

	# 	plt.xlabel('Split Alternatif')
	# 	plt.xticks(x, x_labels, rotation='vertical')
	# 	plt.ylabel('Erreur RDO (%)')
	# 	plt.ylim(0, 150)
	# 	plt.title('Meilleur Split : ' + x_labels[splitType] )
	# 	if (splitType != 3):
	# 		plt.figure()
# *************************************************************


if __name__ == '__main__':
	if len(sys.argv) != 3:
	        print ("***** Usage syntax Error!!!! *****\n")
	        print ("Usage:")
	        print ("python <script> <pathFolder> <nframes> ")
	        sys.exit(1) # exit
	else:
	        pass

	pathFolder = str(sys.argv[1])
	nframes = int(sys.argv[2])

	statsErrorPartition()

	plt.show()	



