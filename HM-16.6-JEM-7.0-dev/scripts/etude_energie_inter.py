import numpy as np
import cv2
from PIL import Image
import sys
from struct import *
import array
import matplotlib.pyplot as plt
import math

def getFrame(image_name,nframe,width,height):
	 
	y = array.array('B')
	u = array.array('B')
	v = array.array('B')
	 
	f_y = open(image_name, "rb")
	f_uv = open(image_name, "rb")
	startY = nframe * width * height * 1.5
	startUV = startY + width*height
	f_y.seek(startY)
	f_uv.seek(startUV)
	 
	image_out = Image.new("RGB", (width, height))
	pix = image_out.load()
	 
	print ("width=", width, "height=", height)
	 
	for i in range(0, height/2):
	    for j in range(0, width/2):
	        u.append(ord(f_uv.read(1)));
	 
	for i in range(0, height/2):
	    for j in range(0, width/2):
	        v.append(ord(f_uv.read(1)));
	for i in range(0,height):
	    for j in range(0, width):
	        y.append(ord(f_y.read(1)));
	        #print "i=", i, "j=", j , (i*width), ((i*width) +j)
	        #pix[j, i] = y[(i*width) +j], y[(i*width) +j], y[(i*width) +j]
	        Y_val = y[(i*width)+j]
	        U_val = u[((i/2)*(width/2))+(j/2)]
	        V_val = v[((i/2)*(width/2))+(j/2)]
	        B = 1.164 * (Y_val-16) + 2.018 * (U_val - 128)
	        G = 1.164 * (Y_val-16) - 0.813 * (V_val - 128) - 0.391 * (U_val - 128)
	        R = 1.164 * (Y_val-16) + 1.596*(V_val - 128)
	        pix[j, i] = int(R), int(G), int(B)
	 
	######################################################
	# B = 1.164(Y - 16)                   + 2.018(U - 128)
	# G = 1.164(Y - 16) - 0.813(V - 128) - 0.391(U - 128)
	# R = 1.164(Y - 16) + 1.596(V - 128)
	######################################################
	 
	#image_out.save("out.bmp")
	# image_out.show()
	return np .array(image_out.getdata(), 
		np .uint8).reshape(image_out.size[1], image_out.size[0], 3)



# ************************************************************
def getFeaturesCU(predsBloc4x4,arraySplitData,pelX,pelY,widthCU,heightCU):
	
	moy_pixmeans = 0
	moyHor = 0
	moyVer = 0
	moy_gradx = 0
	moy_grady = 0
	nbblocks = 0
	for j in range(pelX,pelX+widthCU,sizeBlock):
		for k in range(pelY,pelY+heightCU,sizeBlock):
			idX=j//sizeBlock
			idY=k//sizeBlock
			if(idX<len(predsBloc4x4) and idY<len(predsBloc4x4[0])):
				nbblocks = nbblocks + 1 
				moy_pixmeans=moy_pixmeans+predsBloc4x4[idX][idY]["mean"]
				moy_gradx=moy_gradx+predsBloc4x4[idX][idY]["gradx"]
				moy_grady=moy_grady+predsBloc4x4[idX][idY]["grady"]
				moyHor=moyHor+ 1.0 * predsBloc4x4[idX][idY]["hor"]
				moyVer=moyVer+ 1.0 * predsBloc4x4[idX][idY]["ver"]

	moy_pixmeans=moy_pixmeans/nbblocks
	moy_gradx=moy_gradx/nbblocks
	moy_grady=moy_grady/nbblocks
	moyHor=moyHor/nbblocks
	moyVer=moyVer/nbblocks

	varPix=0
	varHor=0
	varVer=0
	for j in range(pelX,pelX+widthCU,sizeBlock):
		for k in range(pelY,pelY+heightCU,sizeBlock):
			idX=j//sizeBlock
			idY=k//sizeBlock
			if(idX<len(predsBloc4x4) and idY<len(predsBloc4x4[0])):
				# diffMean=abs(predsBloc4x4[idX][idY][2]-moy_pixmeans)
				diffMean=abs(predsBloc4x4[idX][idY]['mean']+predsBloc4x4[idX][idY]["var"]-moy_pixmeans)
				diffMean=(diffMean + abs(predsBloc4x4[idX][idY]['mean']-predsBloc4x4[idX][idY]["var"]-moy_pixmeans))/2.0
				varPix=varPix+diffMean

				varHor = varHor + 1.0 * abs(predsBloc4x4[idX][idY]["hor"]-moyHor)
				varVer = varVer + 1.0 * abs(predsBloc4x4[idX][idY]["ver"]-moyVer)

	varPix = 1.0 * varPix / nbblocks
	varHor = 1.0 * varHor / nbblocks
	varVer = 1.0 * varVer / nbblocks

	return {'moyPix':moy_pixmeans, 'varPix':varPix, 'moyHor':moyHor, 'moyVer':moyVer, 'moy_gradx':moy_gradx, 'moy_grady':moy_grady,'varHor':varHor,'varVer':varVer}

# ************************************************************


# ************************************************************
def getPredInfos(namePredFile,width,height,sizeBlock=4):
	
	predFile = open(namePredFile)

	for i in range(1): predFile.next() # skip first line

	W = width // sizeBlock
	H = height // sizeBlock
	predsBloc4x4 = [0] * W
	for i in range(W):
		predsBloc4x4[i] = [0] * H
		for j in range(H):
			predsBloc4x4[i][j] = {}

	for line in predFile:
		donnees = map(int,line.split())
		# print(donnees)
		# print(donnees[0]//4,donnees[1]//4)
		idX=donnees[0]//sizeBlock
		idY=donnees[1]//sizeBlock

		#vecteur mvt INTER horizontal
		predsBloc4x4[idX][idY]["hor"] = donnees[2]
		#vecteur mvt INTER vertical
		predsBloc4x4[idX][idY]["ver"] = donnees[3]
		#mode pred INTRA
		predsBloc4x4[idX][idY]["intra"] = donnees[4]
		#moyenne pixels bloc
		predsBloc4x4[idX][idY]['mean'] = donnees[5]
		#grad X
		predsBloc4x4[idX][idY]["gradx"] = donnees[6]
		#grad Y
		predsBloc4x4[idX][idY]["grady"] = donnees[7]
		#Variance pixels
		predsBloc4x4[idX][idY]["var"] = donnees[8]

		# blocsParcourus4x4[donnees[0]//sizeBlock][donnees[1]//sizeBlock] = 0

	predFile.close()
	return predsBloc4x4
# ************************************************************


# ************************************************************
def getSplitInfos(nameDecoupeFile,width,height,sizeBlock=4):
	
	DecoupeFile = open(nameDecoupeFile)

	for i in range(1): DecoupeFile.next() # skip first line

	W = width // sizeBlock
	H = height // sizeBlock
	arraySplitData = [0] * W
	for i in range(W):
		arraySplitData[i] = [0] * H
		for j in range(H):
			arraySplitData[i][j] = [0] * 2

	for line in DecoupeFile:
		donnees = map(int,line.split())
		pelX = donnees[0]
		pelY = donnees[1]
		widthCU = donnees[2]-donnees[0]+1
		heightCU = donnees[3]-donnees[1]+1
		for j in range(pelX,pelX+widthCU,sizeBlock):
			for k in range(pelY,pelY+heightCU,sizeBlock):
				idX=j//sizeBlock
				idY=k//sizeBlock
				# print idX, idY, W, H
				arraySplitData[idX][idY][0] = widthCU
				arraySplitData[idX][idY][1] = heightCU

	DecoupeFile.close()
	return arraySplitData
# ************************************************************



# ************************************************************
def indexInsert(energyList,energie):
	
	i=0
	length = len(energyList)
	if(length==0):
		return i

	while(energie>energyList[i]):
		# print ("index ",i, len(energyList), energie, energyList[i])
		i = i+1+length/100
		if(i >= length):
			return i

	return i
# ************************************************************


# ************************************************************
def lowfilter(ListToFilter, amortisseurMax=0):
	
	amortisseurMin = amortisseurMax
	if (amortisseurMax==0):
		maxList = 250
		amortisseurMax = min(200,len(ListToFilter) / maxList)
		amortisseurMin = amortisseurMax/3
	print("amortisseurMax ", amortisseurMax, len(ListToFilter))


	#appliquer le filtre passe bas
	amortisseur = amortisseurMax
	for i in range(1,len(ListToFilter)):
		diff = (ListToFilter[i]-ListToFilter[i-1])
		amortisseur = amortisseurMin + (amortisseurMax-amortisseurMin)*(len(ListToFilter)-i)/len(ListToFilter)
		if(amortisseur>1):
			ListToFilter[i] = ListToFilter[i-1]+diff/amortisseur


# ************************************************************


# ************************************************************
def variation(ListToFilter, pas=10):

	#appliquer le filtre passe bas
	variation = 0.0
	pas = 1
	for i in range(pas,len(ListToFilter),pas):
		diff = ListToFilter[i]-ListToFilter[i-pas]
		variation += diff

	variation = variation * pas * 1.0 / len(ListToFilter)
	return variation
# ************************************************************


# ************************************************************
def energieVSsizeCU(nbCategories):
	#LOOP ON ALL FRAMES
	for count in range(0,nframes):
	 
		# open input/output files
		s = str(count)
		namePredFile = pathFolder + '/block_info_' + s
		predsBloc4x4 = getPredInfos(namePredFile,width,height,sizeBlock)
		
		splitFile = pathFolder + '/decoupe_frame_' + s
		arraySplitData = getSplitInfos(splitFile,width,height,sizeBlock)

		# # img = getFrame(image_name,count,width,height)
		nameCUFile = pathFolder + '/cu_info_' + s
		inputfile = open(nameCUFile)
		for i in range(1): inputfile.next() # skip first line

		listCuData = []
		for line in inputfile:
			intraCU = map(float,line.split())
			intraCU = map(int,intraCU)
			listCuData.append(intraCU)
			pelX = intraCU[0]
			pelY = intraCU[1]
			widthCU = intraCU[2]
			heightCU = intraCU[3]

			if (widthCU * heightCU > sizeBlock * sizeBlock * 2):
			# if(widthCU == 16 and heightCU == 16):

				# print("\n")
				# print intraCU
				
				distortionCU = intraCU[4]
				rateCU = intraCU[5]
				costCU = intraCU[6]*1.0
				distortionSurfacique = distortionCU/(widthCU*heightCU)
				rateSurfacique = rateCU/(widthCU*heightCU)
				costSurfacique = costCU/(widthCU*heightCU)

				#COMPARER AIRES
				energie = getFeaturesCU(predsBloc4x4,arraySplitData,pelX,pelY,widthCU,heightCU,2)*1.0
				energieSurfacique = energie/(widthCU*heightCU)
				aireBase = 8*8
				categorie = min(nbCategories-1,int(math.log(widthCU*heightCU//aireBase,2)))
				index = indexInsert(energyList[categorie],energieSurfacique)
				energyList[categorie].insert(index,energieSurfacique)
				costCUList[categorie].insert(index,costSurfacique)

				# A = costSurfacique/80.0
				# A = min(0.9,A)
				# tau = energieSurfacique / (-math.log(1.0-A))
				# tauList[categorie].insert(index,tau)
				# tau=[4,5,6,7,7.5,8]
				# tauList[categorie].insert(index,8.0 + 75.0*(1-math.exp(-energieSurfacique/tau[categorie])))
				# tau=[2.2,3,3.5,4,4.5,7,]
				# tauList[categorie].insert(index,4.0 + 68.0*(1-math.exp(-energieSurfacique/tau[categorie])))

				print energieSurfacique, costSurfacique

		print ("Image num", count)
		print ("Nbre elts", len(energyList[0]))


	#COMPARER AIRES
	for i in range(nbCategories):
		# lowfilter(costCUList[i])
		plt.scatter(energyList[i],costCUList[i])
		# lowfilter(tauList[i])
		# plt.scatter(energyList[i],tauList[i])
		plt.xlabel('Energie CU type : ' + str(i))
		plt.ylabel('Cout CU')
		if (i != nbCategories-1):
			plt.figure()

# *************************************************************



# ************************************************************
def energieVSenergie(nbCategories):
	#LOOP ON ALL FRAMES
	for count in range(0,nframes):
	 
		# open input/output files
		s = str(count)
		namePredFile = pathFolder + '/block_info_' + s
		predsBloc4x4 = getPredInfos(namePredFile,width,height,sizeBlock)
		
		splitFile = pathFolder + '/decoupe_frame_' + s
		arraySplitData = getSplitInfos(splitFile,width,height,sizeBlock)

		# # img = getFrame(image_name,count,width,height)
		nameCUFile = pathFolder + '/cu_info_' + s
		inputfile = open(nameCUFile)
		for i in range(1): inputfile.next() # skip first line

		listCuData = []
		for line in inputfile:
			intraCU = map(float,line.split())
			intraCU = map(int,intraCU)
			listCuData.append(intraCU)
			pelX = intraCU[0]
			pelY = intraCU[1]
			widthCU = intraCU[2]
			heightCU = intraCU[3]

			# if (widthCU * heightCU > sizeBlock * sizeBlock * 4):
			if(widthCU == 16 and heightCU == 16):

				# print("\n")
				# print intraCU
				
				distortionCU = intraCU[4]
				rateCU = intraCU[5]
				costCU = intraCU[6]*1.0
				distortionSurfacique = distortionCU/(widthCU*heightCU)
				rateSurfacique = rateCU/(widthCU*heightCU)
				costSurfacique = costCU/(widthCU*heightCU)

				# if(widthCU*heightCU == 8*16):	
				# 	#COMPARER ENERGIES
				for i in range(nbCategories):
					energie = getFeaturesCU(predsBloc4x4,arraySplitData,pelX,pelY,widthCU,heightCU,i)*1.0
					# energieGrad = getFeaturesCU(predsBloc4x4,pelX,pelY,widthCU,heightCU,1)
					energieSurfacique = energie/(widthCU*heightCU)
					
					if energie != 0.0:
						index = indexInsert(energyList[i],energieSurfacique)
						energyList[i].insert(index,energieSurfacique)
						costCUList[i].insert(index,costSurfacique)

		print ("Image num", count)
		print ("Nbre elts", len(energyList[0]))

	#COMPARER ENERGIES
	for i in range(nbCategories):
		lowfilter(costCUList[i],5*nframes)
		ET = variation(costCUList[i],5*nframes)
		plt.scatter(energyList[i],costCUList[i])
		plt.xlabel('Energie CU type : ' + str(i))
		plt.ylabel('Cout CU')
		plt.title("Derivee : " + str(ET))
		if (i != nbCategories-1):
			plt.figure()
# *************************************************************


#*************************************************************
def calcCostCu(predsBloc4x4,arraySplitData,pelXsubCu,pelYsubCu,widthCU,heightCU):

	featuresCU = getFeaturesCU(predsBloc4x4,arraySplitData,pelXsubCu,pelYsubCu,widthCU,heightCU)
	qp    = 27;
	a     = 0.00388; 
	b     = 1.489; 
	beta  = 1.372; 
	p     = 0.4394; 
	q     = 0.1255; 

	lambdaRD = 460

	if (rankStyle == 0):
		energie = abs(featuresCU["varHor"]*featuresCU["moy_gradx"]) + abs(featuresCU["varVer"]*featuresCU["moy_grady"])
		energie = energie//(sizeBlock*sizeBlock)
		distortionApprox = a * (qp**b) * (energie**beta);
		rateApprox       = p * math.exp((-q)**qp) * energie;
		rdApprox         = (lambdaRD * rateApprox + distortionApprox) * widthCU//sizeBlock * heightCU//sizeBlock;
	
	elif (rankStyle == 1):
		energie = abs(featuresCU["varHor"]*featuresCU["moy_gradx"]) + abs(featuresCU["varVer"]*featuresCU["moy_grady"])
		energie = energie* widthCU//sizeBlock * heightCU//sizeBlock
		distortionApprox = a * (qp**b) * (energie**beta);
		rateApprox       = p * math.exp((-q)**qp) * energie;
		rdApprox         = (lambdaRD * rateApprox + distortionApprox);

	# print 	distortionApprox, rateApprox
	return rdApprox

# ************************************************************


#*************************************************************
def calcCostQT(predsBloc4x4,arraySplitData,pelX,pelY,widthCU,heightCU):

	# print 	"calcCostQT"
	subCuEnergie = [0] * 2
	for i in range(2):
		subCuEnergie[i] = [0] * 2
		for j in range(2):
			subCuEnergie[i][j] = [0] * 2

	totCost = 0
	for i in range(0,2):
		for j in range(0,2): 
			pelXsubCu = pelX + i * widthCU // 2
			pelYsubCu = pelY + j * heightCU // 2
			if (rankStyle >= 10):
				subCuEnergie[i][j] = getFeaturesCU(predsBloc4x4,arraySplitData,pelXsubCu,pelYsubCu,widthCU//2,heightCU//2)
			else:
				totCost = totCost + calcCostCu(predsBloc4x4,arraySplitData,pelXsubCu,pelYsubCu,widthCU//2,heightCU//2)

	if (rankStyle == 10):	
		totCost = totCost + abs(subCuEnergie[0][0]['moyHor'] - subCuEnergie[0][1]['moyHor']) + abs(subCuEnergie[0][0]['moyVer'] - subCuEnergie[0][1]['moyVer'])	
		totCost = totCost + abs(subCuEnergie[0][0]['moyHor'] - subCuEnergie[1][0]['moyHor']) + abs(subCuEnergie[0][0]['moyVer'] - subCuEnergie[1][0]['moyVer'])	
		totCost = totCost + abs(subCuEnergie[1][1]['moyHor'] - subCuEnergie[0][1]['moyHor']) + abs(subCuEnergie[1][1]['moyVer'] - subCuEnergie[0][1]['moyVer'])	
		totCost = totCost + abs(subCuEnergie[1][1]['moyHor'] - subCuEnergie[1][0]['moyHor']) + abs(subCuEnergie[1][1]['moyVer'] - subCuEnergie[1][0]['moyVer'])		
		totCost = -totCost/ 4.0
	elif (rankStyle == 11):	
		totCost = totCost + abs((subCuEnergie[0][0]['moy_gradx'] + subCuEnergie[0][1]['moy_gradx'])*(subCuEnergie[0][0]['moyHor'] - subCuEnergie[0][1]['moyHor'])) 
		totCost = totCost + abs((subCuEnergie[0][0]['moy_grady'] + subCuEnergie[0][1]['moy_grady'])*(subCuEnergie[0][0]['moyVer'] - subCuEnergie[0][1]['moyVer']))	

		totCost = totCost + abs((subCuEnergie[0][0]['moy_gradx'] + subCuEnergie[1][0]['moy_gradx'])*(subCuEnergie[0][0]['moyHor'] - subCuEnergie[1][0]['moyHor'])) 
		totCost = totCost + abs((subCuEnergie[0][0]['moy_grady'] + subCuEnergie[1][0]['moy_grady'])*(subCuEnergie[0][0]['moyVer'] - subCuEnergie[1][0]['moyVer']))
		
		totCost = totCost + abs((subCuEnergie[1][1]['moy_gradx'] + subCuEnergie[0][1]['moy_gradx'])*(subCuEnergie[1][1]['moyHor'] - subCuEnergie[0][1]['moyHor'])) 
		totCost = totCost + abs((subCuEnergie[1][1]['moy_grady'] + subCuEnergie[0][1]['moy_grady'])*(subCuEnergie[1][1]['moyVer'] - subCuEnergie[0][1]['moyVer']))
		
		totCost = totCost + abs((subCuEnergie[1][1]['moy_gradx'] + subCuEnergie[1][0]['moy_gradx'])*(subCuEnergie[1][1]['moyHor'] - subCuEnergie[1][0]['moyHor'])) 
		totCost = totCost + abs((subCuEnergie[1][1]['moy_grady'] + subCuEnergie[1][0]['moy_grady'])*(subCuEnergie[1][1]['moyVer'] - subCuEnergie[1][0]['moyVer']))

		totCost = -totCost / 4.0

	return totCost
# ************************************************************


#*************************************************************
def calcCostHor(predsBloc4x4,arraySplitData,pelX,pelY,widthCU,heightCU):
	# print 	"calcCostHor"

	subCuEnergie = [0] * 2
	for i in range(2):
		subCuEnergie[i] = [0] * 2

	totCost = 0
	for j in range(0,2):
		pelYsubCu = pelY + j * heightCU // 2
		if (rankStyle >= 10):
			subCuEnergie[j] = getFeaturesCU(predsBloc4x4,arraySplitData,pelX,pelYsubCu,widthCU,heightCU//2)
		else:
			totCost = totCost + calcCostCu(predsBloc4x4,arraySplitData,pelX,pelYsubCu,widthCU,heightCU//2)

	if (rankStyle == 10):
		totCost = totCost + abs(subCuEnergie[0]['moyHor'] - subCuEnergie[1]['moyHor']) + abs(subCuEnergie[0]['moyVer'] - subCuEnergie[1]['moyVer']) 	
		totCost = -totCost
	elif (rankStyle == 11):
		totCost = totCost + abs((subCuEnergie[0]['moy_gradx'] + subCuEnergie[1]['moy_gradx'])*(subCuEnergie[0]['moyHor'] - subCuEnergie[1]['moyHor'])) 
		totCost = totCost + abs((subCuEnergie[0]['moy_grady'] + subCuEnergie[1]['moy_grady'])*(subCuEnergie[0]['moyVer'] - subCuEnergie[1]['moyVer'])) 	
		totCost = -totCost

	return totCost
# ************************************************************


#*************************************************************
def calcCostVer(predsBloc4x4,arraySplitData,pelX,pelY,widthCU,heightCU):
	# print 	"calcCostVer"

	subCuEnergie = [0] * 2
	for i in range(2):
		subCuEnergie[i] = [0] * 2

	totCost = 0
	for j in range(0,2):
		pelXsubCu = pelX + j * widthCU // 2
		if (rankStyle >= 10):
			subCuEnergie[j] =  getFeaturesCU(predsBloc4x4,arraySplitData,pelXsubCu,pelY,widthCU//2,heightCU)
		else:
			totCost = totCost + calcCostCu(predsBloc4x4,arraySplitData,pelXsubCu,pelY,widthCU//2,heightCU)

	if (rankStyle == 10):
		totCost = totCost + abs(subCuEnergie[0]['moyHor'] - subCuEnergie[1]['moyHor']) + abs(subCuEnergie[0]['moyVer'] - subCuEnergie[1]['moyVer']) 
		totCost = -totCost
	elif (rankStyle == 11):
		totCost = totCost + abs((subCuEnergie[0]['moy_gradx'] + subCuEnergie[1]['moy_gradx'])*(subCuEnergie[0]['moyHor'] - subCuEnergie[1]['moyHor'])) 
		totCost = totCost + abs((subCuEnergie[0]['moy_grady'] + subCuEnergie[1]['moy_grady'])*(subCuEnergie[0]['moyVer'] - subCuEnergie[1]['moyVer'])) 	
		totCost = -totCost

	return totCost
# ************************************************************


# ************************************************************
def calcProbaSplits(pelX,pelY,widthCU,heightCU):

# ***********************	
	choiceFeatures = []
	featuresCU = getFeaturesCU(predsBloc4x4 ,pelX,pelY,widthCU,heightCU)
	choiceFeatures.append(widthCU*heightCU) 
	choiceFeatures.append(featuresCU['varHor']) 
	choiceFeatures.append(featuresCU['varVer']) 
	choiceFeatures.append(featuresCU['moyHor']) 
	choiceFeatures.append(featuresCU['moyVer']) 
	choiceFeatures.append(featuresCU['moy_gradx']) 
	choiceFeatures.append(featuresCU['moy_grady']) 

	proba01 = model_01.predict_proba(choiceFeatures)



# ***********************	
	choiceFeatures = []
	choiceFeatures.append(math.log(widthCU*1.0/heightCU,2)) 
	#HORIZONTAL
	for i in range(0,2):
		pelXsubCu = pelX 
		pelYsubCu = pelY + i * heightCU // 2
		featuresCU = getFeaturesCU(predsBloc4x4 ,pelXsubCu,pelYsubCu,widthCU,heightCU//2)
		choiceFeatures.append(featuresCU['varHor']) 
		choiceFeatures.append(featuresCU['varVer']) 
		choiceFeatures.append(featuresCU['moy_gradx']) 
		choiceFeatures.append(featuresCU['moy_grady']) 

	# VERTICAL
	for i in range(0,2):
		pelXsubCu = pelX + i * widthCU // 2
		pelYsubCu = pelY 
		featuresCU = getFeaturesCU(predsBloc4x4 ,pelXsubCu,pelYsubCu,widthCU//2,heightCU)
		choiceFeatures.append(featuresCU['varHor']) 
		choiceFeatures.append(featuresCU['varVer']) 
		choiceFeatures.append(featuresCU['moy_gradx']) 
		choiceFeatures.append(featuresCU['moy_grady']) 

	proba23 = model_23.predict_proba(choiceFeatures)


# ***********************	
	probas=[0]*4
	proba[0] = proba01[0][0]
	proba[2] = proba01[0][1]*proba23[0][0]
	proba[3] = proba01[0][1]*proba23[0][1]



# ***********************	
	if(widthCU==heightCU):
#elif choixAttributs == "testQtHor":
		choiceFeatures = []
		choiceFeatures.append(widthCU*heightCU) 
		#HORIZONTAL
		for i in range(0,2):
			pelXsubCu = pelX 
			pelYsubCu = pelY + i * heightCU // 2
			featuresCU = getFeaturesCU(predsBloc4x4 ,pelXsubCu,pelYsubCu,widthCU,heightCU//2)
			choiceFeatures.append(featuresCU['varHor']) 
			choiceFeatures.append(featuresCU['varVer']) 

		for i in range(0,2):
			for j in range(0,2): 
				pelXsubCu = pelX + i * widthCU // 2
				pelYsubCu = pelY + j * heightCU // 2

				featuresCU = getFeaturesCU(predsBloc4x4 ,pelXsubCu,pelYsubCu,widthCU//2,heightCU//2)
				choiceFeatures.append(featuresCU['varHor']) 
				choiceFeatures.append(featuresCU['varVer']) 
				choiceFeatures.append(featuresCU['moy_gradx']) 
				choiceFeatures.append(featuresCU['moy_grady']) 

		proba12 = model_12.predict_proba(choiceFeatures)



	#elif choixAttributs == "testQtVer":
		choiceFeatures = []
		choiceFeatures.append(widthCU*heightCU) 
		# VERTICAL
		for i in range(0,2):
			pelXsubCu = pelX + i * widthCU // 2
			pelYsubCu = pelY 
			featuresCU = getFeaturesCU(predsBloc4x4 ,pelXsubCu,pelYsubCu,widthCU//2,heightCU)
			choiceFeatures.append(featuresCU['varHor']) 
			choiceFeatures.append(featuresCU['varVer']) 

		for i in range(0,2):
			for j in range(0,2): 
				pelXsubCu = pelX + i * widthCU // 2
				pelYsubCu = pelY + j * heightCU // 2

				featuresCU = getFeaturesCU(predsBloc4x4 ,pelXsubCu,pelYsubCu,widthCU//2,heightCU//2)
				choiceFeatures.append(featuresCU['varHor']) 
				choiceFeatures.append(featuresCU['varVer']) 
				choiceFeatures.append(featuresCU['moy_gradx']) 
				choiceFeatures.append(featuresCU['moy_grady']) 
		
		proba13 = model_13.predict_proba(choiceFeatures)

	proba[2] = proba[2]*proba12[0][1]
	proba[3] = proba[3]*proba13[0][1]
	proba[1] = proba[2]*proba12[0][0] + proba[3]*proba13[0][0]

	return proba

# ************************************************************


# ************************************************************
def energieVSchoicePartition(nbCategories):
	#LOOP ON ALL FRAMES
	splitFoundList = []
	for i in range(0,4):
		splitFoundList.insert(i,[0]*4)

	nbType = [0]*4
	totExplore = [0]*4
	energieStyle = 0
	for count in range(1,nframes):

		# open input/output files
		s = str(count)
		namePredFile = pathFolder + '/block_info_' + s
		predsBloc4x4 = getPredInfos(namePredFile,width,height,sizeBlock)

		splitFile = pathFolder + '/decoupe_frame_' + s
		arraySplitData = getSplitInfos(splitFile,width,height,sizeBlock)

		splitFile = pathFolder + '/choice_split_frame_' + s
		inputfile = open(splitFile)
		for i in range(1): inputfile.next() # skip first line

		listCuData = []
		for line in inputfile:
			intraCU = map(float,line.split())
			intraCU = map(int,intraCU)
			listCuData.append(intraCU)
			pelX = intraCU[0]
			pelY = intraCU[1]
			widthCU = intraCU[2]
			heightCU = intraCU[3]

			Cost = [0]*4
			if (widthCU  > sizeBlock and heightCU > sizeBlock and widthCU*heightCU >= 16*8 and pelX+widthCU<width and pelY+heightCU<height):
				# print("\n")
				# print intraCU
				splitType = intraCU[4]

				if(rankStyle < 10):
					Cost[0] = calcCostCu(predsBloc4x4,arraySplitData,pelX,pelY,widthCU,heightCU)*1.0
				else:
					Cost[0] = 10000000000.0 
				
				if (heightCU == widthCU):
					Cost[1] = calcCostQT(predsBloc4x4,arraySplitData,pelX,pelY,widthCU,heightCU)*1.0
				else :
					Cost[1] = 100000000000.0

				Cost[2] = calcCostHor(predsBloc4x4,arraySplitData,pelX,pelY,widthCU,heightCU)*1.0
				Cost[3] = calcCostVer(predsBloc4x4,arraySplitData,pelX,pelY,widthCU,heightCU)*1.0

				splitMin = 0
				minCost = Cost[0]
				for i in range(1,4):
					if (Cost[i]<minCost) :
						splitMin = i
						minCost = Cost[i]

				# print Cost
				# print splitType, splitMin
				nbType[splitType] = nbType[splitType] + 1

				#INTERVALLE DE CONFIANCE
				intervalle = 0.01
				for i in range(0,4):
					if (Cost[i] <= minCost+intervalle*abs(minCost)/100.0) :
						splitFoundList[splitType][i] = splitFoundList[splitType][i] + 1
						totExplore[splitType] = totExplore[splitType] + 1

	 	print ("Image num", count)
		print ("Nbre elts QT", splitFoundList[1][1])
						
	for splitType in range(0,4):
		plt.bar([0,1,2,3], splitFoundList[splitType], 0.8, color='b' )
		plt.xlabel('Mon split ')
		plt.ylabel('Nombre ')
		plt.title("Vrai Split : " + str(splitType) + "\n Optimal :" +  str(nbType[splitType]) + ", Explores :" +  str(totExplore[splitType]) )
		if (splitType != 3):
			plt.figure()
# *************************************************************



if len(sys.argv) != 6:
        print ("***** Usage syntax Error!!!! *****\n")
        print ("Usage:")
        print ("python <script> <pathFolder> <nframes> <width> <height> <sizeBlock>")
        sys.exit(1) # exit
else:
        pass

pathFolder = str(sys.argv[1])
nframes = int(sys.argv[2])
width = int(sys.argv[3])
height = int(sys.argv[4])
sizeBlock = int(sys.argv[5])

#CHOSE HOW TO RANK A SPLIT
rankStyle=11

#CREATE LISTS
nbCategories = 3 
rateCUList = []
distortionCUList = []
costCUList = []
energyList = []
tauList = []
for i in range(0,nbCategories):
	rateCUList.insert(i,[])
	distortionCUList.insert(i,[])
	costCUList.insert(i,[])
	energyList.insert(i,[])
	tauList.insert(i,[])


fileModel="test_01.model"
print('loading model from %s ...' % fileModel)
model_01 = pickle.load(open(fileModel, 'rb'))

fileModel="test_12.model"
print('loading model from %s ...' % fileModel)
model_12 = pickle.load(open(fileModel, 'rb'))

fileModel="test_13.model"
print('loading model from %s ...' % fileModel)
model_13 = pickle.load(open(fileModel, 'rb'))

fileModel="test_23.model"
print('loading model from %s ...' % fileModel)
model_23 = pickle.load(open(fileModel, 'rb'))


# energieVSenergie(nbCategories)

# energieVSsizeCU(nbCategories)

energieVSchoicePartition(nbCategories)




# plt.xlabel('Energie CU / pixel')
# plt.ylabel('Cout CU / pixel')
plt.show()	



