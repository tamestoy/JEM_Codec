#!/bin/bash

function which_num_frames {

	folder=$1
	
	if [[ $folder = *"416"* ]]; then
	  num_frames=`expr $num_frames_4k \\* 16`
  elif [[ $folder = *"832"* ]]; then
	  num_frames=`expr $num_frames_4k \\* 10`
  elif [[ $folder = *"1280"* ]]; then
	  num_frames=`expr $num_frames_4k \\* 6`
	elif [[ $folder = *"1920"* ]]; then
	  num_frames=`expr $num_frames_4k \\* 4`
	elif [[ $folder = *"2560"* ]]; then
	  num_frames=`expr $num_frames_4k \\* 3`
	elif [[ $folder = *"3840"* ]]; then
	  num_frames=$num_frames_4k
	else
		echo "CAREFULL MAYBE WRONG VIDEO RESOLUTION"
		exit
	fi
}


echo "num parameters "$#
if [ "$#" -ne 3 ]; then
    echo "Illegal number of parameters"
    echo
    echo "USE ./machine_learning_multipleVid.sh <base_learning_folder> <models_folder> <num Frame 4K> "
    exit
fi

base_folder=$1
models_folder=$2
num_frames_4k=$3

for QP in 22 27 32 37; do
	# learning_folders=`find $base_folder -name block_info_16 | grep $QP"_JEM_RA" | grep -v "train_videos"`
	learning_folders=`find $base_folder -name block_info_16 | grep $QP"_JEM_RA" `
	echo $learning_folders
	for folder in $learning_folders; do
		echo 
		echo "valeur QP "$QP
		echo 
		folder=${folder%block_info_16}
		which_num_frames ${folder}

		echo "python machine_learning_inter.py  ${folder} ${models_folder} ${num_frames}"
		python machine_learning_inter.py  ${folder} ${models_folder} ${num_frames} 

	done
done

